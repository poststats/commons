package com.brianlong.util;

public class StringUtil {

	private static final StringUtil INSTANCE = new StringUtil();

	public static StringUtil getInstance() {
		return StringUtil.INSTANCE;
	}

	private StringUtil() {
	}

	public boolean isEmpty(String value) {
		return value == null || value.length() == 0;
	}

	public String toUppercaseWords(String value) {
		if (this.isEmpty(value)) return value;

		StringBuilder strbuilder = new StringBuilder(value);
		boolean newword = true;

		for (int i = 0; i < strbuilder.length(); i++) {
			char ch = strbuilder.charAt(i);
			if (Character.isWhitespace(ch)) {
				newword = true;
			} else if (newword) {
				strbuilder.setCharAt(i, Character.toUpperCase(ch));
				newword = false;
			}
		}

		return strbuilder.toString();
	}

	public String trim(String value) {
		if (value == null) return value;
		value = value.trim();
		return value.length() == 0 ? null : value;
	}

}
