package com.brianlong.util;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class DependencyNode<K, D> {

	private K key;
	private D data;
	private long value;
	private long totalParentValue = 0L;
	private long totalChildValue = 0L;
	private Set<K> parentKeys = new HashSet<K>();
	private Set<K> childKeys = new HashSet<K>();

	public DependencyNode(K key, D data, long value) {
		this.key = key;
		this.data = data;
		this.value = value;
	}

	public K getKey() {
		return this.key;
	}

	public D getData() {
		return this.data;
	}

	public long getValue() {
		return this.value;
	}

	public long getTotalParentValue() {
		return this.totalParentValue;
	}

	public long getTotalChildValue() {
		return this.totalChildValue;
	}

	void addParent(DependencyNode<K, D> depnode) {
		if (this.parentKeys.contains(depnode.key)) throw new IllegalStateException("This node is already associated with the specified parent node");

		this.parentKeys.add(depnode.key);
		this.totalParentValue += depnode.totalParentValue + depnode.value;

		depnode.childKeys.add(this.key);
		depnode.totalChildValue += this.totalChildValue + this.value;
	}

	void addChild(DependencyNode<K, D> depnode) {
		if (this.childKeys.contains(depnode.key)) throw new IllegalStateException("This node is already associated with the specified child node");

		this.childKeys.add(depnode.key);
		this.totalChildValue += depnode.totalChildValue + depnode.value;

		depnode.parentKeys.add(this.key);
		depnode.totalParentValue += this.totalParentValue + this.value;
	}

	boolean hasChildren() {
		return !this.childKeys.isEmpty();
	}

	Iterator<K> iterateChildren() {
		return this.childKeys.iterator();
	}

	Iterator<K> iterateParents() {
		return this.parentKeys.iterator();
	}

	void removeParent(DependencyNode<K, D> depnode) {
		if (!this.parentKeys.contains(depnode.key)) throw new IllegalStateException("This node is not associated with the specified parent node");

		this.parentKeys.remove(depnode.key);
		this.totalParentValue -= depnode.totalParentValue + depnode.value;
	}

	void removeChild(DependencyNode<K, D> depnode) {
		if (!this.childKeys.contains(depnode.key)) throw new IllegalStateException("This node is not associated with the specified child node");

		this.childKeys.remove(depnode.key);
		this.totalChildValue -= depnode.totalChildValue + depnode.value;
	}

}
