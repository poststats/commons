package com.brianlong.util;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/**
 * This class implements the quick sorting algorithm.
 *
 * A quick sort is a VERY non-human way of sorting. It is only efficient because
 * a computer does it. It can be VERY efficient on parallel processing systems.
 *
 * A quick sort does work and then "breaks it down". A merge sort "breaks it
 * down" and then does work.
 *
 * First, when there are 2 or more items, the quick sort will find a "pivot"
 * halfway in the set of items. Second, all items are then arranged with respect
 * to the pivot; all items to the left are less and all items to the right are
 * more. Third, the same procedure is then applied to the items on the left and
 * then the items on the right.
 *
 * This algorithm is the best choice if the items are not very sorted. It has
 * more varied performance results than merge sort. It has a best case scenario
 * better than the merge sort, but a worst case scenario worse than a merge
 * sort.
 *
 * @author Brian M Long
 */
public class QuickSorter implements Sorter {

	private boolean threaded;

	public QuickSorter(boolean threaded) {
		this.threaded = threaded;
	}

	/**
	 * @see Sorter#sort(java.util.List, boolean)
	 */
	public <C extends Comparable<C>> void sort(List<C> items, boolean ascending) {
		this.sort(items, new ComparableComparator<C>(), ascending);
	}

	/**
	 * @see Sorter#sortMapByField(java.util.List, java.lang.Object, boolean)
	 */
	public <K, C extends Comparable<C>, M extends Map<K, C>> void sortMapByField(List<M> items, K sortBy, boolean ascending) {
		this.sort(items, new SimpleMapComparator<K, C, M>(sortBy), ascending);
	}

	/**
	 * @see Sorter#sort(java.util.List, java.util.Comparator, boolean)
	 */
	@SuppressWarnings(value = "unchecked")
	public <O> void sort(List<O> items, Comparator<O> comparator, boolean ascending) {
		int size = items.size();
		if (size < 2) return;

		int pivot = size / 2;
		O pivotItem = items.remove(pivot);
		List<O> miditems = new LinkedList<O>();
		miditems.add(pivotItem);

		List<O> leftitems = items.subList(0, pivot);
		for (ListIterator<O> i = leftitems.listIterator(); i.hasNext();) {
			O item = i.next();
			int winner = comparator.compare(item, pivotItem);
			if (!ascending) winner = -winner;

			if (winner >= 0) {
				i.remove();
				miditems.add(item);
				pivot--;
			}
		}

		List<O> rightitems = items.subList(pivot, items.size());
		for (ListIterator<O> i = rightitems.listIterator(); i.hasNext();) {
			O item = i.next();
			int winner = comparator.compare(item, pivotItem);
			if (!ascending) winner = -winner;

			if (winner < 0) {
				i.remove();
				miditems.add(0, item);
				pivot++;
			}
		}

		items.addAll(items.size() - rightitems.size(), miditems);

		if (this.threaded) {
			// Java lists cannot be concurrently modified
			O[] leftobjs = (O[]) items.subList(0, pivot)
					.toArray();
			O[] rightobjs = (O[]) items.subList(pivot + 1, size)
					.toArray();
			O pivotobj = items.get(pivot);

			// use the array sorter
			this.sort(leftobjs, comparator, ascending);
			this.sort(rightobjs, comparator, ascending);

			// put them back into the list
			items.clear();
			for (int i = 0; i < leftobjs.length; i++)
				items.add(leftobjs[i]);
			items.add(pivotobj);
			for (int i = 0; i < rightobjs.length; i++)
				items.add(rightobjs[i]);
		} else {
			this.sort(items.subList(0, pivot), comparator, ascending);
			this.sort(items.subList(pivot + 1, size), comparator, ascending);
		}
	}

	/**
	 * @see Sorter#sortMapByField(java.util.List, java.lang.Object, boolean)
	 */
	public <K, O, M extends Map<K, O>> void sortMapByField(List<M> items, K sortBy, Comparator<O> comparator, boolean ascending) {
		this.sort(items, new MapComparator<K, O, M>(sortBy, comparator), ascending);
	}

	/**
	 * @see Sorter#sort(java.lang.Comparable[], boolean)
	 */
	public <C extends Comparable<C>> void sort(C[] items, boolean ascending) {
		this.sort(items, new ComparableComparator<C>(), ascending);
	}

	/**
	 * @see Sorter#sort(java.lang.Object[], java.util.Comparator, boolean)
	 */
	public <O> void sort(O[] items, Comparator<O> comparator, boolean ascending) {
		this.sort(items, 0, items.length, comparator, ascending);
	}

	private <O> void sort(O[] items, int start, int end, Comparator<O> comparator, boolean ascending) {
		int size = end - start;
		if (size < 2) return;

		int pivot = (start + end) / 2;
		for (int left = start, right = end - 1; left < pivot || pivot < right;) {
			int leftFound = -1;
			for (; left < pivot && leftFound < 0;) {
				int winner = comparator.compare(items[left], items[pivot]);
				if (!ascending) winner = -winner;
				if (winner >= 0) leftFound = left;
				else left++;
			}

			int rightFound = -1;
			for (; pivot < right && rightFound < 0;) {
				int winner = comparator.compare(items[pivot], items[right]);
				if (!ascending) winner = -winner;
				if (winner > 0) rightFound = right;
				else right--;
			}

			if (leftFound < 0 && rightFound < 0) {
				// it is sorted
			} else if (leftFound < 0) {
				O o = items[rightFound];
				for (int i = rightFound; i > pivot; i--)
					items[i] = items[i - 1];
				items[pivot++] = o;
				left++; // move left with pivot
			} else if (rightFound < 0) {
				O o = items[leftFound];
				for (int i = leftFound; i < pivot; i++)
					items[i] = items[i + 1];
				items[pivot--] = o;
				right--; // move right with pivot
			} else {
				O o = items[leftFound];
				items[leftFound] = items[rightFound];
				items[rightFound] = o;
				left++;
				right--;
			}
		}

		if (this.threaded) {
			Thread threadLeft = new Thread(new ThreadedArraySort<O>(this, items, start, pivot, comparator, ascending), "quick-sorter-" + start + "-" + pivot);
			Thread threadRight = new Thread(new ThreadedArraySort<O>(this, items, pivot + 1, end, comparator, ascending),
					"quick-sorter-" + (pivot + 1) + "-" + end);
			threadLeft.start();
			threadRight.start();

			try {
				threadLeft.join();
				threadRight.join();
			} catch (InterruptedException ie) {
				throw new RuntimeException(ie.getMessage());
			}
		} else {
			this.sort(items, start, pivot, comparator, ascending);
			this.sort(items, pivot + 1, end, comparator, ascending);
		}
	}

	/*
	 * private class ThreadedListSort<O> implements Runnable {
	 *
	 * private QuickSorter sorter; private List<O> items; private Comparator<O>
	 * comparator; private boolean ascending;
	 *
	 * public ThreadedListSort(QuickSorter sorter, List<O> items, Comparator<O>
	 * comparator, boolean ascending) { this.sorter = sorter; this.items = items;
	 * this.comparator = comparator; this.ascending = ascending; }
	 *
	 * public void run() { this.sorter.sort(this.items, this.comparator,
	 * this.ascending); }
	 *
	 * }
	 */
	private class ThreadedArraySort<O> implements Runnable {

		private QuickSorter sorter;
		private O[] items;
		private int start;
		private int end;
		private Comparator<O> comparator;
		private boolean ascending;

		public ThreadedArraySort(QuickSorter sorter, O[] items, int start, int end, Comparator<O> comparator, boolean ascending) {
			this.sorter = sorter;
			this.items = items;
			this.start = start;
			this.end = end;
			this.comparator = comparator;
			this.ascending = ascending;
		}

		public void run() {
			this.sorter.sort(this.items, start, end, this.comparator, this.ascending);
		}

	}

}
