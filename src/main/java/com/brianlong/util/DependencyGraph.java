package com.brianlong.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class DependencyGraph<K, D> {

	private Map<K, DependencyNode<K, D>> nodes = new HashMap<K, DependencyNode<K, D>>();
	private Set<K> noChildrenKeys = new HashSet<K>();

	public void add(DependencyNode<K, D> node, Set<K> childKeys, Set<K> parentKeys) {
		this.nodes.put(node.getKey(), node);

		for (K childKey : childKeys) {
			try {
				DependencyNode<K, D> childNode = this.nodes.get(childKey);
				if (childNode != null) node.addChild(childNode);
			} catch (IllegalStateException ise) {
				// suppress
			}
		}

		if (!node.hasChildren()) this.noChildrenKeys.add(node.getKey());

		for (K parentKey : parentKeys) {
			try {
				DependencyNode<K, D> parentNode = this.nodes.get(parentKey);
				if (parentNode != null) {
					node.addParent(parentNode);
					this.noChildrenKeys.remove(parentNode.getKey());
				}
			} catch (IllegalStateException ise) {
				// suppress
			}
		}
	}

	public DependencyNode<K, D> get(K key) {
		return this.nodes.get(key);
	}

	public DependencyNode<K, D> remove(K key) {
		DependencyNode<K, D> node = this.nodes.get(key);
		if (node.hasChildren()) throw new IllegalStateException("A node with dependencies may not be removed");

		this.nodes.remove(key);
		this.noChildrenKeys.remove(key);

		for (Iterator<K> i = node.iterateParents(); i.hasNext();) {
			K parentKey = i.next();
			DependencyNode<K, D> parentNode = this.nodes.get(parentKey);

			parentNode.removeChild(node);
			if (!parentNode.hasChildren()) this.noChildrenKeys.add(parentKey);
		}

		return node;
	}

	public boolean isEmpty() {
		return this.nodes.isEmpty();
	}

	public Iterator<K> iterateChildlessKeys() {
		return this.noChildrenKeys.iterator();
	}

}
