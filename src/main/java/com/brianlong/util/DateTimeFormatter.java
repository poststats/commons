package com.brianlong.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.TemporalAccessor;
import java.util.HashMap;
import java.util.Map;

/**
 * This class resolves two problems with SimpleDateFormat.
 *
 * SimpleDateFormat problem #1: --------------------------------------
 * Constructing a new SimpleDateFormat object is very expensive in terms of time
 * and processing power. This class caches all created SimpleDateFormat
 * instances using a hash map. The SimpleDateFormat class is not meant to be
 * used by multiple threads and is therefore not thread safe.
 *
 * SimpleDateFormat problem #2: -------------------------------------- The
 * SimpleDateFormat methods are not thread safe. This class synchronizes calls
 * to 'format' and 'parse' to alleviate the introduced issue.
 *
 *
 * There is always a cost to everything. This class may resolve the issues
 * presented above, but this class uses more memory and method calls are ever so
 * slightly slower. The memory footprint is extremely small as the JVM simply
 * holds a couple extra objects that it would not otherwise hold. Although
 * overall performance is better (not created new SimpleDateFormat instances),
 * the individual method calls, which are synchronized, are slightly slower to
 * allow for locking/unlocking and may actually be blocked for a short period of
 * time.
 *
 * @author Brian M Long
 */
public class DateTimeFormatter {

	private java.time.format.DateTimeFormatter dtf;

	private static final Map<String, java.time.format.DateTimeFormatter> DTFS = new HashMap<String, java.time.format.DateTimeFormatter>();

	public static final String FORMAT_SHORT_DATE = "MM/dd/yyyy";
	public static final String FORMAT_LONG_DATE = "MMMM d, yyyy";
	public static final String FORMAT_SQL_DATE = "yyyy-MM-dd";

	public static final String FORMAT_12HR_TIME = "h:mm a";
	public static final String FORMAT_24HR_TIME = "HH:mm";
	public static final String FORMAT_SQL_TIME = "HH:mm:ss";

	public static final String FORMAT_SQL_DATETIME = FORMAT_SQL_DATE + ' ' + FORMAT_SQL_TIME;

	/**
	 * This constructor creates a formatter using a format following the
	 * SimpleDateFormat format guidelines.
	 *
	 * This constructor creates one SimpleDateFormat instance for each unique
	 * format. The first call to this constructor for each unique format is several
	 * times more expensive than subsequent calls.
	 *
	 * @param format A SimpleDateFormat compatible format
	 */
	public DateTimeFormatter(String format) {
		synchronized (DTFS) {
			if (!DTFS.containsKey(format)) DTFS.put(format, java.time.format.DateTimeFormatter.ofPattern(format));
		}

		this.dtf = DTFS.get(format);
	}

	public String format(TemporalAccessor localDateOrTime) {
		return this.dtf.format(localDateOrTime);
	}

	public TemporalAccessor parse(String datestr) {
		return this.dtf.parse(datestr);
	}

	public LocalDate parseAsDate(String datestr) {
		return LocalDate.from(this.parse(datestr));
	}

	public LocalTime parseAsTime(String datestr) {
		return LocalTime.from(this.parse(datestr));
	}

	public LocalDateTime parseAsDateTime(String datestr) {
		return LocalDateTime.from(this.parse(datestr));
	}

}
