package com.brianlong.util;

import com.brianlong.DeveloperException;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKBReader;
import com.vividsolutions.jts.io.WKTReader;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * @author Brian M Long
 */
public class FlexMap extends HashMap<String, Object> implements Cloneable, Serializable {

	private static final long serialVersionUID = 1L;

	private Map<String, Object> cache = new HashMap<String, Object>();

	public FlexMap() {
	}

	public FlexMap(Map<String, Object> map) {
		super(map);
	}

	public Iterator<String> iterator() {
		Set<String> keys = new HashSet<String>(this.keySet());
		keys.addAll(this.cache.keySet());
		return keys.iterator();
	}

	public boolean containsKey(String field) {
		if (this.cache.containsKey(field)) return true;
		else return super.containsKey(field);
	}

	public Object get(String field) {
		if (this.cache.containsKey(field)) return this.cache.get(field);
		else return super.get(field);
	}

	public Object getUncached(String field) {
		return super.get(field);
	}

	/**
	 * This is required because JSP EL apparently uses reflection and looks for
	 * get(java.lang.Object)
	 */
	public Object get(Object key) {
		return this.get((String) key);
	}

	public Object put(String field, Object value) {
		this.cache.remove(field);
		return super.put(field, value);
	}

	public boolean isNotEmpty(String field) {
		Object obj = this.get(field);
		if (obj == null) {
			return false;
		} else if (obj instanceof String) {
			return ((String) obj).trim()
					.length() > 0;
		} else if (obj instanceof Object[]) {
			Object[] objs = (Object[]) obj;
			for (int i = 0; i < objs.length; i++) {
				if (objs[i] == null) {
				} else if (objs[i] instanceof String) {
					return ((String) objs[i]).trim()
							.length() > 0;
				} else {
					return true;
				}
			}

			return false;
		} else {
			return true;
		}
	}

	public <T> T get(String field, Class<T> clazz) {
		return this.get(field, clazz, null);
	}

	protected void cache(String field, Object value) {
		this.cache.put(field, value);
	}

	public String getString(String field) {
		return this.get(field, String.class);
	}

	public Boolean getBoolean(String field) {
		return this.get(field, Boolean.class);
	}

	public Byte getByte(String field) {
		return this.get(field, Byte.class);
	}

	public Short getShort(String field) {
		return this.get(field, Short.class);
	}

	public Integer getInteger(String field) {
		return this.get(field, Integer.class);
	}

	public Long getLong(String field) {
		return this.get(field, Long.class);
	}

	public Float getFloat(String field) {
		return this.get(field, Float.class);
	}

	public Double getDouble(String field) {
		return this.get(field, Double.class);
	}

	public BigInteger getBigInteger(String field) {
		return this.get(field, BigInteger.class);
	}

	public BigDecimal getBigDecimal(String field) {
		return this.get(field, BigDecimal.class);
	}

	@SuppressWarnings(value = "unchecked")
	private <T> T get(String field, Class<T> clazz, Method method) {
		Object obj = this.cache.get(field);
		if (obj != null && clazz.isInstance(obj)) return (T) obj;

		obj = this.getUncached(field);
		if (obj == null) return null;
		if (clazz.isInstance(obj)) return (T) obj;

		String str = null;
		if (obj.getClass()
				.isArray()) {
			Object[] objs = (Object[]) obj;
			str = (objs.length == 0 || objs[0] == null) ? null : String.valueOf(objs[0]);
		} else {
			str = obj.toString();
		}
		if (clazz != String.class && str.length() == 0) {
			this.cache.put(field, null);
			return null;
		}

		try {
			T instance = null;
			if (method != null) {
				instance = (T) method.invoke(new Object[] { str });
			} else {
				Constructor<T> constructor = clazz.getConstructor(new Class[] { String.class });
				instance = constructor.newInstance(new Object[] { str });
			}

			this.cache.put(field, instance);
			return instance;
		} catch (RuntimeException re) {
			throw re;
		} catch (Exception e) {
			throw new ClassCastException("The field, '" + field + "' with string value '" + str + "', could not be returned as '" + clazz + "'");
		}
	}

	public LocalDate getDate(String field) {
		Object obj = this.get(field);
		if (obj == null) return null;
		if (obj instanceof LocalDate) return (LocalDate) obj;

		LocalDate date = DateTimeParameterParser.getInstance()
				.parseAsDate(obj);
		this.cache.put(field, date);
		return date;
	}

	public LocalTime getTime(String field) {
		return this.getTime(field, false);
	}

	public LocalTime getTime(String field, boolean enableArrayParseSeconds) {
		Object obj = this.get(field);
		if (obj == null) return null;
		if (obj instanceof LocalTime) return (LocalTime) obj;

		LocalTime time = DateTimeParameterParser.getInstance()
				.parseAsTime(obj, enableArrayParseSeconds);
		this.cache.put(field, time);
		return time;
	}

	public LocalDateTime getDateTime(String field) {
		return this.getDateTime(field, false);
	}

	public LocalDateTime getDateTime(String field, boolean enableArrayParseSeconds) {
		Object obj = this.get(field);
		if (obj == null) return null;
		if (obj instanceof LocalDateTime) return (LocalDateTime) obj;

		LocalDateTime datetime = DateTimeParameterParser.getInstance()
				.parseAsDateTime(obj, enableArrayParseSeconds);
		this.cache.put(field, datetime);
		return datetime;
	}

	public LocalDateTime getDateTime(String field, String format) {
		Object obj = this.get(field);
		if (obj == null) return null;
		if (obj instanceof LocalDateTime) return (LocalDateTime) obj;

		try {
			Method parseMethod = DateTimeFormatter.class.getMethod("parse", new Class[] { String.class });
			Date date = this.get(field, Date.class, parseMethod);

			// FIXME is this the right timezone?
			LocalDateTime datetime = Instant.ofEpochMilli(date.getTime())
					.atZone(ZoneId.systemDefault())
					.toLocalDateTime();
			this.cache.put(field, datetime);
			return datetime;
		} catch (NoSuchMethodException nsme) {
			// this block should never be executed, but just in case ...
			throw new RuntimeException(nsme.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	public <T extends Geometry> T getGeometry(String field, Class<T> geometryType) throws ParseException {
		Object obj = this.get(field);
		if (obj == null) return null;
		if (geometryType.isInstance(obj)) return (T) obj;

		T geometry;
		if (obj instanceof byte[]) {
			geometry = (T) new WKBReader().read((byte[]) obj);
		} else if (obj instanceof CharSequence) {
			geometry = (T) new WKTReader().read(((CharSequence) obj).toString());
		} else {
			throw new DeveloperException("The gemoetry format '" + obj.getClass() + "' is not supported");
		}

		this.cache.put(field, geometry);
		return geometry;
	}

	@Override
	@SuppressWarnings(value = "unchecked")
	public boolean equals(Object obj) {
		Map<String, Object> map1 = null;
		Map<String, Object> map2 = null;

		if (obj instanceof FlexMap) {
			FlexMap ds = (FlexMap) obj;
			map1 = ds;
			map2 = ds.cache;
		} else if (obj instanceof Map) {
			map1 = (Map<String, Object>) obj;
		}

		if (map1 == null) return false;

		Set<String> keys = map1.keySet();
		if (map2 != null) keys.addAll(map2.keySet());
		for (Object key : keys) {
			boolean matched = false;

			Object value1 = map1.get(key);
			if (value1 != null) {
				matched = matched || value1.equals(this.get(key));
				matched = matched || value1.equals(this.cache.get(key));
			}

			Object value2 = map2.get(key);
			if (value2 != null) {
				matched = matched || value2.equals(this.get(key));
				matched = matched || value2.equals(this.cache.get(key));
			}

			if (!matched) return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		Set<String> keys = this.keySet();
		keys.addAll(this.cache.keySet());

		int power = 1;
		int totalHash = 0;
		for (Object key : keys) {
			totalHash += Math.pow(2, power) * key.hashCode();
			Object value = this.get(key);
			if (value != null) totalHash += Math.pow(2, power) * 10 * value.hashCode();

			power++;
		}

		return totalHash;
	}

	@Override
	@SuppressWarnings(value = "unchecked")
	public Object clone() {
		FlexMap map = (FlexMap) super.clone();
		map.cache = (Map<String, Object>) ((HashMap<String, Object>) this.cache).clone();
		return map;
	}

}
