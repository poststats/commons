package com.brianlong.util;

import java.lang.reflect.Method;
import java.util.Map;

/**
 * @author Brian M Long
 */
public final class JavaBeanUtils {

	private JavaBeanUtils() {
	}

	/**
	 * This method creates a new shallow copy of the specified source object. Unless
	 * the source object is a java.util.Map, it will be treated as a java bean. A
	 * shallow copy copies all the property object references from the source
	 * getters to the target setters. Because it is shallow, changes to the
	 * properties in one bean or map may be reflected in the other bean or map.
	 *
	 * @param source A source java bean using getters or map using get
	 * @return A java bean or map, depending on the source
	 */
	public static Object createShallowCopy(Object source) {
		try {
			Object target = source.getClass()
					.newInstance(); // execute bean's/map's default constructor
			JavaBeanUtils.doShallowCopy(source, target);
			return target;
		} catch (RuntimeException re) {
			throw re;
		} catch (Exception e) {
			throw new IllegalArgumentException("The source class must have a default constructor");
		}
	}

	/**
	 * This method creates a new deep copy of the specified source object. Unless
	 * the source object is a java.util.Map, it will be treated as a java bean. A
	 * deep copy recursively copies all the property objects from the source getters
	 * to the target setters. Because it is deep, changes to the properties in one
	 * bean or map will not be reflected in the other bean or map.
	 *
	 * @param source A source java bean using getters or map using get
	 * @return A java bean or map, depending on the source
	 */
	public static Object createDeepCopy(Object source) {
		return JavaBeanUtils.createDeepCopy(source, Byte.MAX_VALUE);
	}

	/**
	 * This method creates a new deep copy of the specified source object at the
	 * specified depth.
	 *
	 * @see com.brianlong.util.JavaBeanUtils#createDeepCopy(java.lang.Object)
	 * @param source A source java bean using getters or map using get
	 * @param depth  A number from 0 to java.lang.Integer.MAX_VALUE
	 * @return A java bean or map, depending on the source
	 */
	public static Object createDeepCopy(Object source, byte depth) {
		try {
			Object target = source.getClass()
					.newInstance(); // execute bean's/map's default constructor
			JavaBeanUtils.doDeepCopy(source, target, depth);
			return target;
		} catch (RuntimeException re) {
			throw re;
		} catch (Exception e) {
			throw new IllegalArgumentException("The source class must have a default constructor");
		}
	}

	/**
	 * This method does a shallow, least-denominator java bean comparison. It
	 * compares only all the fields that have getters in both beans. If the field's
	 * value is the same (same instance or equals()), then this method will return
	 * true.
	 *
	 * @param source A source java bean using getters or map using get
	 * @param target A source java bean using setters or map using put
	 * @return true if the java beans have the same getter values; false otherwise
	 */
	public static boolean doShallowBeanComparison(Object source, Object target) {
		if (source == null && target == null) return true;
		else if (source == null || target == null) return false;

		Class<?> sourceClass = source.getClass();
		Class<?> targetClass = target.getClass();

		Method[] methods = sourceClass.getMethods();
		for (int i = 0; i < methods.length; i++) {
			Method sourceMethod = methods[i];
			if (sourceMethod.getName()
					.startsWith("get") && sourceMethod.getParameterTypes().length == 0) {
				try {
					Method targetMethod = targetClass.getMethod(sourceMethod.getName(), new Class[0]);

					Object sourceValue = sourceMethod.invoke(source, new Object[0]);
					Object targetValue = targetMethod.invoke(target, new Object[0]);
					if (sourceValue == targetValue) {
						continue;
					} else if (sourceValue == null || targetValue == null) {
						return false;
					} else if (!sourceValue.equals(targetValue) && !targetValue.equals(sourceValue)) {
						return false;
					}
				} catch (RuntimeException re) {
					throw re;
				} catch (Exception e) {
					// suppress; skip property
				}
			}
		}

		return true;
	}

	/**
	 * This method performes a shallow copy. A shallow copy copies all the property
	 * object references from the source getters to the target setters. Because it
	 * is shallow, changes to the properties in one bean or map are reflected in the
	 * other bean or map.
	 *
	 * @param source A source java bean using getters or map using get
	 * @param target A target java bean using setters or map using put
	 */
	public static void doShallowCopy(Object source, Object target) {
		JavaBeanUtils.doCopy(source, target, (byte) 0);
	}

	/**
	 * This method performes a deep copy. A deep copy is the same as a shallow copy
	 * except Cloneable objects are cloned appropriately. Because it is deep,
	 * changes to the properties in one bean or map are NOT reflected in the other
	 * bean or map.
	 *
	 * @param source A source java bean using getters or map using get
	 * @param target A target java bean using setters or map using put
	 * @param depth  A number of levels deep...to copy
	 */
	public static void doDeepCopy(Object source, Object target, byte depth) {
		JavaBeanUtils.doCopy(source, target, depth);
	}

	@SuppressWarnings(value = "unchecked")
	private static void doCopy(Object source, Object target, byte depth) {
		if (source instanceof Map) JavaBeanUtils.doMapToBeanCopy((Map<String, Object>) source, target, depth);
		else if (target instanceof Map) JavaBeanUtils.doBeanToMapCopy(source, (Map<String, Object>) target, depth);
		else JavaBeanUtils.doBeanToBeanCopy(source, target, depth);
	}

	private static void doBeanToBeanCopy(Object source, Object target, int depth) {
		Class<?> sourceClass = source.getClass();
		Class<?> targetClass = target.getClass();

		Method[] methods = sourceClass.getMethods();
		for (int i = 0; i < methods.length; i++) {
			if (methods[i].getName()
					.startsWith("get") && methods[i].getParameterTypes().length == 0) {
				String setMethodName = "s" + methods[i].getName()
						.substring(1);
				Class<?> propertyClass = methods[i].getReturnType();

				try {
					Method setMethod = targetClass.getMethod(setMethodName, new Class[] { propertyClass });
					Object value = methods[i].invoke(source, new Object[0]);
					setMethod.invoke(target, new Object[] { JavaBeanUtils.doCopy(value, depth - 1) });
				} catch (NoSuchMethodException nsme) {
					// suppress; skip property
				} catch (RuntimeException re) {
					throw re;
				} catch (Exception e) {
					// suppress; skip property
				}
			}
		}
	}

	private static void doBeanToMapCopy(Object source, Map<String, Object> target, int depth) {
		Class<?> sourceClass = source.getClass();

		Method[] methods = sourceClass.getMethods();
		for (int i = 0; i < methods.length; i++) {
			if (methods[i].getName()
					.startsWith("get") && methods[i].getParameterTypes().length == 0) {
				char firstLetter = Character.toLowerCase(methods[i].getName()
						.charAt(3));
				String fieldName = firstLetter + methods[i].getName()
						.substring(4);

				try {
					Object sourceValue = methods[i].invoke(source, new Object[0]);
					target.put(fieldName, JavaBeanUtils.doCopy(sourceValue, depth - 1));
				} catch (RuntimeException re) {
					throw re;
				} catch (Exception e) {
					// suppress; skip method
				}
			}
		}
	}

	private static void doMapToBeanCopy(Map<String, Object> source, Object target, int depth) {
		Class<?> targetClass = target.getClass();

		for (String sourceFieldName : source.keySet()) {
			Object sourceValue = source.get(sourceFieldName);

			if (sourceValue == null) {
				// do what?
			} else {
				String methodName = "set" + Character.toUpperCase(sourceFieldName.charAt(0)) + sourceFieldName.substring(1);

				try {
					Method method = targetClass.getMethod(methodName, new Class[] { sourceValue.getClass() });
					method.invoke(target, new Object[] { JavaBeanUtils.doCopy(sourceValue, depth - 1) });
				} catch (NoSuchMethodException nsme) {
					// suppress; skip method
				} catch (RuntimeException re) {
					throw re;
				} catch (Exception e) {
					// suppress; skip method
				}
			}
		}
	}

	private static Object doCopy(Object value, int depth) throws CloneNotSupportedException {
		if (depth < 1) return value;

		if (value instanceof JavaBeanUtils) {
			return ((JavaBeanUtils) value).clone();
		} else if (value instanceof Cloneable) {
			try {
				Method cloneMethod = value.getClass()
						.getDeclaredMethod("clone", new Class[0]);
				return cloneMethod.invoke(value, new Object[0]);
			} catch (RuntimeException re) {
				throw re;
			} catch (Exception e) {
				throw new CloneNotSupportedException(e.getMessage());
			}
		} else {
			return value;
		}
	}

}
