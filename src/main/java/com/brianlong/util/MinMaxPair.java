package com.brianlong.util;

public class MinMaxPair<T extends Comparable<T>> {

	private T min;
	private T max;

	public MinMaxPair() {
	}

	public MinMaxPair(T min, T max) {
		this.min = min;
		this.max = max;
	}

	public T getMinimum() {
		return this.min;
	}

	public T getMaximum() {
		return this.max;
	}

	public MinMaxPair<T> revise(MinMaxPair<T> pair) {
		return this.revise(pair.getMinimum(), pair.getMaximum());
	}

	public MinMaxPair<T> revise(T min, T max) {
		return new MinMaxPair<T>((this.min == null || min.compareTo(this.min) > 0) ? min : this.min,
				(this.max == null || max.compareTo(this.max) < 0) ? max : this.max);
	}

	public boolean overlaps(MinMaxPair<T> pair) {
		return this.overlaps(pair.getMinimum(), pair.getMaximum());
	}

	public boolean overlaps(T min, T max) {
		return (this.min == null || max.compareTo(this.min) > 0) && (this.max == null || min.compareTo(this.max) < 0);
	}

	public boolean isValid() {
		return this.min == null || this.max == null || this.min.compareTo(this.max) <= 0;
	}

	@Override
	public String toString() {
		return this.min + "-" + this.max;
	}

}
