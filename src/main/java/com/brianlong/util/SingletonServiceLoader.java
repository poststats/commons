package com.brianlong.util;

import com.brianlong.DeveloperException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.ServiceLoader;
import javax.management.ServiceNotFoundException;

public class SingletonServiceLoader {

	private static final Map<Class<?>, ServiceLoader<?>> serviceLoaderMap = new HashMap<Class<?>, ServiceLoader<?>>(15);
	private static final Map<Class<?>, Object> serviceMap = new HashMap<Class<?>, Object>(15);

	@SuppressWarnings("unchecked")
	public static <T> ServiceLoader<T> load(Class<T> clazz) throws DeveloperException {
		if (!serviceLoaderMap.containsKey(clazz)) {
			ServiceLoader<T> services = ServiceLoader.load(clazz);
			if (services == null) throw new DeveloperException(new ServiceNotFoundException());

			serviceLoaderMap.put(clazz, services);
		}

		return (ServiceLoader<T>) serviceLoaderMap.get(clazz);
	}

	@SuppressWarnings("unchecked")
	public static <T> T loadOne(Class<T> clazz) throws DeveloperException {
		if (!serviceMap.containsKey(clazz)) {
			ServiceLoader<T> services = load(clazz);

			Iterator<T> i = services.iterator();
			if (!i.hasNext()) throw new DeveloperException(new ServiceNotFoundException());

			serviceMap.put(clazz, i.next());
		}

		return (T) serviceMap.get(clazz);
	}

}
