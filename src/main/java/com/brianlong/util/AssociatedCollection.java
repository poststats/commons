package com.brianlong.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * This class implements a doubly-linked web of associations. There are no
 * parents or children. A node may not be associated to anything or it may be
 * associated to everything. It can even be associated with itself.
 *
 * A node MUST implement 'equals' and 'hashCode'. This allows for the opposite
 * association in the reverse direction.
 *
 * Example #1: Nodes 'A' and 'B'. Associations 'better' and 'worse' 'A' is
 * better than 'B' | 'B' is worse than 'A' AssociatedCollection.add('A', 'B',
 * 'better', 'worse')
 *
 * Example #2: Nodes 'A' and 'B'. Association 'works with' 'A' works with 'B' |
 * 'B' works with 'A' AssociatedCollection.add('A', 'B', 'works with')
 *
 * @author Brian M Long
 */
public class AssociatedCollection<N, A> {

	private Map<N, Map<N, A>> tree = new HashMap<N, Map<N, A>>();

	public boolean add(N node) {
		if (this.tree.containsKey(node)) return false;

		this.tree.put(node, new HashMap<N, A>());
		return true;
	}

	public void add(N node1, N node2) {
		this.add(node1, node2, null);
	}

	public void add(N node1, N node2, A assoc) {
		this.add(node1, node2, assoc, assoc);
	}

	public void add(N node1, N node2, A assoc, A reverseAssoc) {
		this.add(node1);
		this.tree.get(node1)
				.put(node2, assoc);

		this.add(node2);
		this.tree.get(node2)
				.put(node1, reverseAssoc);
	}

	public void clear() {
		this.tree.clear();
	}

	public A getAssociation(N node1, N node2) {
		if (!this.tree.containsKey(node1) || !this.tree.containsKey(node2)) return null;
		return this.tree.get(node1)
				.get(node2);
	}

	public Iterator<N> iterator() {
		return this.tree.keySet()
				.iterator();
	}

	public Iterator<N> iterator(N node) {
		return this.tree.get(node)
				.keySet()
				.iterator();
	}

	public int size() {
		return this.tree.size();
	}

	public void remove(N node) {
		for (N onode : this.tree.get(node)
				.keySet())
			this.tree.get(onode)
					.remove(node);
		this.tree.remove(node);
	}

}
