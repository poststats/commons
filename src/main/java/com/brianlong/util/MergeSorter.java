package com.brianlong.util;

import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/**
 * This class implements the merge sorting algorithm.
 *
 * A merge sort is a non-human way of sorting. It is only efficient because a
 * computer does it. It can be VERY efficient on parallel processing systems.
 *
 * A quick sort does work and then "breaks it down". A merge sort "breaks it
 * down" and then does work.
 *
 * First, when there are 2 or more items, the merge sort will split the items
 * into items on the left and items on the right. Second, a merge sort will be
 * performed on the items on the left and then to the items on the right. Third,
 * the items on the left (which are sorted) and items on the right (which are
 * sorted) will be merged. It is easy to visualize, as a human, how you would
 * bring together two sorted sets of items in a sorted fashion.
 *
 * This algorithm is the best choice if the items are not very sorted. It has
 * less varied performance results than quick sort. It has a worst case scenario
 * better than the quick sort, but a best case scenario worse than a quick sort.
 *
 * @author Brian M Long
 */
public class MergeSorter implements Sorter {

	private boolean threaded;

	public MergeSorter(boolean threaded) {
		this.threaded = threaded;
	}

	/**
	 * @see Sorter#sort(java.util.List, boolean)
	 */
	public <C extends Comparable<C>> void sort(List<C> items, boolean ascending) {
		this.sort(items, new ComparableComparator<C>(), ascending);
	}

	/**
	 * @see Sorter#sortMapByField(java.util.List, java.lang.Object, boolean)
	 */
	public <K, C extends Comparable<C>, M extends Map<K, C>> void sortMapByField(List<M> items, K sortBy, boolean ascending) {
		this.sort(items, new SimpleMapComparator<K, C, M>(sortBy), ascending);
	}

	/**
	 * @see Sorter#sort(java.util.List, java.util.Comparator, boolean)
	 */
	@SuppressWarnings(value = "unchecked")
	public <O> void sort(List<O> items, Comparator<O> comparator, boolean ascending) {
		int size = items.size();
		if (size < 2) return;

		if (this.threaded) {
			// Java lists cannot be concurrently modified
			O[] leftitems = (O[]) items.subList(0, size / 2)
					.toArray();
			O[] rightitems = (O[]) items.subList(size / 2, size)
					.toArray();

			// use the array sorter
			this.sort(leftitems, comparator, ascending);
			this.sort(rightitems, comparator, ascending);

			// put them back into the list
			items.clear();
			for (int i = 0; i < leftitems.length; i++)
				items.add(leftitems[i]);
			for (int i = 0; i < rightitems.length; i++)
				items.add(rightitems[i]);
		} else {
			this.sort(items.subList(0, size / 2), comparator, ascending);
			this.sort(items.subList(size / 2, size), comparator, ascending);
		}

		List<O> left = items.subList(0, size / 2);
		int right = size / 2;
		ListIterator<O> ileft = left.listIterator();
		while (ileft.hasNext() && left.size() < size) {
			O o1 = ileft.next();
			O o2 = items.get(right);
			int winner = comparator.compare(o1, o2);
			if (!ascending) winner = -winner;

			if (winner > 0) {
				ileft.previous();
				ileft.add(o2);
				right += 2;
			}
		}

		// remove all the garbage on the "right" side of the list after the
		// iterator-based modifications are complete. it is done this way
		// because concurrent modification to the list are illegal.
		if (size < items.size()) items.subList(size + right - items.size(), right)
				.clear();
	}

	/**
	 * @see Sorter#sortMapByField(java.util.List, java.lang.Object, boolean)
	 */
	public <K, O, M extends Map<K, O>> void sortMapByField(List<M> items, K sortBy, Comparator<O> comparator, boolean ascending) {
		this.sort(items, new MapComparator<K, O, M>(sortBy, comparator), ascending);
	}

	/**
	 * @see Sorter#sort(java.lang.Comparable[], boolean)
	 */
	public <C extends Comparable<C>> void sort(C[] items, boolean ascending) {
		this.sort(items, new ComparableComparator<C>(), ascending);
	}

	/**
	 * @see Sorter#sort(java.lang.Object[], java.util.Comparator, boolean)
	 */
	public <O> void sort(O[] items, Comparator<O> comparator, boolean ascending) {
		this.sort(items, 0, items.length, comparator, ascending);
	}

	private <O> void sort(O[] items, int start, int end, Comparator<O> comparator, boolean ascending) {
		int size = end - start;
		if (size < 2) return;

		int half = (start + end) / 2;
		if (this.threaded) {
			Thread threadLeft = new Thread(new ThreadedArraySort<O>(this, items, start, half, comparator, ascending), "merge-sorter-" + start + "-" + half);
			Thread threadRight = new Thread(new ThreadedArraySort<O>(this, items, half, end, comparator, ascending), "merge-sorter-" + half + "-" + end);
			threadLeft.start();
			threadRight.start();

			try {
				threadLeft.join();
				threadRight.join();
			} catch (InterruptedException ie) {
				throw new RuntimeException(ie.getMessage());
			}
		} else {
			this.sort(items, start, half, comparator, ascending);
			this.sort(items, half, end, comparator, ascending);
		}

		for (int left = start, right = half; left < right && right < end;) {
			O o1 = items[left];
			O o2 = items[right];
			int winner = comparator.compare(o1, o2);
			if (!ascending) winner = -winner;

			if (winner > 0) {
				for (int i = right; i > left; i--)
					items[i] = items[i - 1];
				items[left] = o2;
				right++;
			}

			left++;
		}
	}

	/*
	 * private class ThreadedListSort<O> implements Runnable {
	 *
	 * private MergeSorter sorter; private List<O> items; private Comparator<O>
	 * comparator; private boolean ascending;
	 *
	 * public ThreadedListSort(MergeSorter sorter, List<O> items, Comparator<O>
	 * comparator, boolean ascending) { this.sorter = sorter; this.items = items;
	 * this.comparator = comparator; this.ascending = ascending; }
	 *
	 * public void run() { this.sorter.sort(this.items, this.comparator,
	 * this.ascending); }
	 *
	 * }
	 */
	private class ThreadedArraySort<O> implements Runnable {

		private MergeSorter sorter;
		private O[] items;
		private int start;
		private int end;
		private Comparator<O> comparator;
		private boolean ascending;

		public ThreadedArraySort(MergeSorter sorter, O[] items, int start, int end, Comparator<O> comparator, boolean ascending) {
			this.sorter = sorter;
			this.items = items;
			this.start = start;
			this.end = end;
			this.comparator = comparator;
			this.ascending = ascending;
		}

		public void run() {
			this.sorter.sort(this.items, start, end, this.comparator, this.ascending);
		}

	}

}
