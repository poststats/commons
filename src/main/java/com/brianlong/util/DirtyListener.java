package com.brianlong.util;

public interface DirtyListener<T> extends ModifyListener<T>, RemoveListener<T> {

}
