package com.brianlong.util;

public interface ModifyListener<T> extends Listener<T> {

	void modified(T value);

}
