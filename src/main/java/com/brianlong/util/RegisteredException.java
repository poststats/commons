package com.brianlong.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Brian M Long
 */
public class RegisteredException extends Exception {

	private static final long serialVersionUID = 1L;
	private static final Map<String, RegisteredException> EXCEPTIONS = new HashMap<String, RegisteredException>();

	public static void register(String code, String message) {
		register(new RegisteredException(code, message));
	}

	protected static void register(RegisteredException re) {
		synchronized (EXCEPTIONS) {
			EXCEPTIONS.put(re.getCode(), re);
		}
	}

	public static RegisteredException getIt(String code) {
		synchronized (EXCEPTIONS) {
			return EXCEPTIONS.get(code);
		}
	}

	public static void throwIt(String code) throws RegisteredException {
		throw getIt(code);
	}

	private String code;

	protected RegisteredException(String code, String message) {
		super(message);
		this.code = code;
	}

	public String getCode() {
		return this.code;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof String) return this.code.equals(obj);
		return this == obj; // valid because of the control over instantiation
	}

	@Override
	public int hashCode() {
		return this.code.hashCode();
	}

}
