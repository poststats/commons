package com.brianlong.util;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class CollectionsIterator<E> implements ListIterator<E> {

	private ListIterator<E> iterator;

	public CollectionsIterator(final Collection<E>... collections) {
		List<E> lists = new LinkedList<E>();
		for (Collection<E> collection : collections)
			lists.addAll(collection);
		this.iterator = lists.listIterator();
	}

	public void add(E e) {
		this.iterator.add(e);
	}

	public boolean hasNext() {
		return this.iterator.hasNext();
	}

	public boolean hasPrevious() {
		return this.iterator.hasPrevious();
	}

	public E next() {
		return this.iterator.next();
	}

	public int nextIndex() {
		return this.iterator.nextIndex();
	}

	public E previous() {
		return this.iterator.previous();
	}

	public int previousIndex() {
		return this.iterator.previousIndex();
	}

	public void remove() {
		this.iterator.remove();
	}

	public void set(E e) {
		this.iterator.set(e);
	}

}
