package com.brianlong.util;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.temporal.TemporalAccessor;
import java.util.Calendar;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class DateTimeParameterParser {

	private static final DateTimeParameterParser INSTANCE = new DateTimeParameterParser();
	private static final DateTimeFormatter[] DATE_FORMATTERS = new DateTimeFormatter[] { new DateTimeFormatter(DateTimeFormatter.FORMAT_SQL_DATE),
			new DateTimeFormatter("MM-dd-yyyy"), new DateTimeFormatter("MM-dd-yy"), new DateTimeFormatter("M-dd-yyyy"), new DateTimeFormatter("M-dd-yy"),
			new DateTimeFormatter("yyyy-MM-dd") };
	private static final DateTimeFormatter[] TIME_FORMATTERS = new DateTimeFormatter[] { new DateTimeFormatter(DateTimeFormatter.FORMAT_SQL_TIME),
			new DateTimeFormatter("HH:mm:ss.SSS"), new DateTimeFormatter("HH:mm:ss"), new DateTimeFormatter("HH:mm"), new DateTimeFormatter("hh:mm:ss a"),
			new DateTimeFormatter("hh:mm a") };
	private static final DateTimeFormatter[] DATETIME_FORMATTERS = new DateTimeFormatter[] { new DateTimeFormatter(DateTimeFormatter.FORMAT_SQL_DATETIME),
			new DateTimeFormatter("MM-dd-yyyy h:mm a") };

	public static DateTimeParameterParser getInstance() {
		return DateTimeParameterParser.INSTANCE;
	}

	private DateTimeParameterParser() {
	}

	public LocalDate parseAsDate(Object value) {
		List<LocalDate> dates = this.parseAsDates(value);
		return dates.isEmpty() ? null
				: dates.iterator()
						.next();
	}

	public List<LocalDate> parseAsDates(Object value) {
		value = this.prepValue(value);

		List<LocalDate> dates = new LinkedList<LocalDate>();
		if (value == null) {
			// leave empty
		} else if (value instanceof LocalDate) {
			dates.add((LocalDate) value);
		} else if (value instanceof LocalDate[]) {
			for (LocalDate date : (LocalDate[]) value)
				dates.add(date);
		} else if (value instanceof Calendar) {
			dates.add(((Calendar) value).toInstant()
					.atZone(ZoneId.systemDefault())
					.toLocalDate());
		} else if (value instanceof Calendar[]) {
			for (Calendar calendar : (Calendar[]) value)
				dates.add(((Calendar) value).toInstant()
						.atZone(ZoneId.systemDefault())
						.toLocalDate());
		} else if (value instanceof java.util.Date) {
			dates.add(Instant.ofEpochMilli(((java.util.Date) value).getTime())
					.atZone(ZoneId.systemDefault())
					.toLocalDate());
		} else if (value instanceof java.util.Date[]) {
			for (java.util.Date date : (java.util.Date[]) value)
				dates.add(Instant.ofEpochMilli(((java.util.Date) value).getTime())
						.atZone(ZoneId.systemDefault())
						.toLocalDate());
		} else if (value instanceof Object[]) {
			Object[] objvalues = (Object[]) value;
			int baseindex = 0;

			while (baseindex < objvalues.length) {
				boolean isArray = false;

				if (baseindex + 3 <= objvalues.length) {
					if (objvalues[baseindex] != null && objvalues[baseindex + 1] != null && objvalues[baseindex + 2] != null) {
						try {
							java.lang.Number valueMonth = this.parseNumber(objvalues[baseindex]);
							java.lang.Number valueDay = this.parseNumber(objvalues[baseindex + 1]);
							java.lang.Number valueYear = this.parseNumber(objvalues[baseindex + 2]);

							if (valueMonth == null || valueDay == null || valueYear == null) {
								dates.add(null);
							} else {
								dates.add(LocalDate.of(valueYear.intValue(), valueMonth.intValue(), valueDay.intValue()));
							}

							isArray = true;
							baseindex += 3;
						} catch (NumberFormatException nfe) {
							// suppress
						} catch (IllegalArgumentException iae) {
							// suppress
						}
					}
				}

				if (!isArray) {
					if (objvalues[baseindex] == null) {
						dates.add(null);
					} else if (objvalues[baseindex] instanceof LocalDate) {
						dates.add((LocalDate) objvalues[baseindex]);
					} else if (objvalues[baseindex] instanceof Calendar) {
						dates.add(((Calendar) objvalues[baseindex]).toInstant()
								.atZone(ZoneId.systemDefault())
								.toLocalDate());
					} else if (objvalues[baseindex] instanceof java.util.Date) {
						dates.add(Instant.ofEpochMilli(((java.util.Date) objvalues[baseindex]).getTime())
								.atZone(ZoneId.systemDefault())
								.toLocalDate());
					} else if (objvalues[baseindex] instanceof String) {
						dates.add(this.parseDate((String) objvalues[baseindex]));
					} else {
						throw new IllegalArgumentException();
					}

					baseindex++;
				}
			}
		} else {
			throw new IllegalArgumentException("The type, '" + value.getClass() + "', is not supported for a date");
		}

		return dates;
	}

	public LocalTime parseAsTime(Object value, boolean enableSeconds) {
		List<LocalTime> times = this.parseAsTimes(value, enableSeconds);
		return times.isEmpty() ? null
				: times.iterator()
						.next();
	}

	public List<LocalTime> parseAsTimes(Object value, boolean enableSeconds) {
		value = this.prepValue(value);

		List<LocalTime> times = new LinkedList<LocalTime>();
		if (value == null) {
			// leave empty
		} else if (value instanceof LocalTime) {
			times.add((LocalTime) value);
		} else if (value instanceof LocalTime[]) {
			for (LocalTime time : (LocalTime[]) value)
				times.add(time);
		} else if (value instanceof Calendar) {
			times.add(((Calendar) value).toInstant()
					.atZone(ZoneId.systemDefault())
					.toLocalTime());
		} else if (value instanceof Calendar[]) {
			for (Calendar calendar : (Calendar[]) value)
				times.add(((Calendar) value).toInstant()
						.atZone(ZoneId.systemDefault())
						.toLocalTime());
		} else if (value instanceof java.util.Date) {
			times.add(Instant.ofEpochMilli(((java.util.Date) value).getTime())
					.atZone(ZoneId.systemDefault())
					.toLocalTime());
		} else if (value instanceof java.util.Date[]) {
			for (java.util.Date date : (java.util.Date[]) value)
				times.add(Instant.ofEpochMilli(((java.util.Date) value).getTime())
						.atZone(ZoneId.systemDefault())
						.toLocalTime());
		} else if (value instanceof Object[]) {
			Object[] objvalues = (Object[]) value;
			int arrayElements = enableSeconds ? 4 : 3;
			int baseindex = 0;

			while (baseindex < objvalues.length) {
				boolean isArray = false;

				if (baseindex + arrayElements <= objvalues.length) {
					boolean notNull = true;
					for (int i = baseindex; i < baseindex + arrayElements; i++)
						notNull = notNull && objvalues[i] != null;

					if (notNull) {
						try {
							java.lang.Number valueHour = this.parseNumber(objvalues[baseindex]);
							java.lang.Number valueMinute = this.parseNumber(objvalues[baseindex + 1]);
							java.lang.Number valueSecond = enableSeconds ? this.parseNumber(objvalues[baseindex + 2]) : null;
							String valueAmPm = (String) objvalues[baseindex + arrayElements - 1];

							if (valueHour == null || valueMinute == null || valueAmPm == null || (enableSeconds && valueSecond == null)) {
								times.add(null);
							} else {
								int hour = valueHour.intValue() % 12;
								int minute = valueMinute.intValue();
								int second = enableSeconds ? valueSecond.intValue() : 0;
								if (TimeAmPm.PM.equals(TimeAmPm.valueOf(valueAmPm))) hour += 12;
								times.add(LocalTime.of(hour, minute, second));
							}

							isArray = true;
							baseindex += arrayElements;
						} catch (NumberFormatException nfe) {
							// suppress
						} catch (IllegalArgumentException iae) {
							// suppress
						}
					}
				}

				if (!isArray) {
					if (objvalues[baseindex] == null) {
						times.add(null);
					} else if (objvalues[baseindex] instanceof LocalTime) {
						times.add((LocalTime) objvalues[baseindex]);
					} else if (objvalues[baseindex] instanceof Calendar) {
						times.add(((Calendar) objvalues[baseindex]).toInstant()
								.atZone(ZoneId.systemDefault())
								.toLocalTime());
					} else if (objvalues[baseindex] instanceof java.util.Date) {
						times.add(Instant.ofEpochMilli(((java.util.Date) objvalues[baseindex]).getTime())
								.atZone(ZoneId.systemDefault())
								.toLocalTime());
					} else if (objvalues[baseindex] instanceof String) {
						times.add(this.parseTime((String) objvalues[baseindex]));
					} else {
						throw new IllegalArgumentException();
					}

					baseindex++;
				}
			}
		} else {
			throw new IllegalArgumentException("The type, '" + value.getClass() + "', is not supported for a time");
		}

		return times;
	}

	public LocalDateTime parseAsDateTime(Object value, boolean enableSeconds) {
		List<LocalDateTime> datetimes = this.parseAsDateTimes(value, enableSeconds);
		return datetimes.isEmpty() ? null
				: datetimes.iterator()
						.next();
	}

	public List<LocalDateTime> parseAsDateTimes(Object value, boolean enableSeconds) {
		value = this.prepValue(value);

		List<LocalDateTime> datetimes = new LinkedList<LocalDateTime>();
		if (value == null) {
			// leave empty
		} else if (value instanceof LocalDateTime) {
			datetimes.add((LocalDateTime) value);
		} else if (value instanceof LocalDateTime[]) {
			for (LocalDateTime calendar : (LocalDateTime[]) value)
				datetimes.add(calendar);
		} else if (value instanceof Calendar) {
			datetimes.add(((Calendar) value).toInstant()
					.atZone(ZoneId.systemDefault())
					.toLocalDateTime());
		} else if (value instanceof Calendar[]) {
			for (Calendar calendar : (Calendar[]) value)
				datetimes.add(((Calendar) value).toInstant()
						.atZone(ZoneId.systemDefault())
						.toLocalDateTime());
		} else if (value instanceof java.util.Date) {
			datetimes.add(Instant.ofEpochMilli(((java.util.Date) value).getTime())
					.atZone(ZoneId.systemDefault())
					.toLocalDateTime());
		} else if (value instanceof java.util.Date[]) {
			for (java.util.Date date : (java.util.Date[]) value)
				datetimes.add(Instant.ofEpochMilli(((java.util.Date) value).getTime())
						.atZone(ZoneId.systemDefault())
						.toLocalDateTime());
		} else if (value instanceof Object[]) {
			Object[] objvalues = (Object[]) value;
			int arrayElements = enableSeconds ? 7 : 6;
			int baseindex = 0;

			while (baseindex < objvalues.length) {
				boolean isArray = false;

				if (baseindex + arrayElements <= objvalues.length) {
					boolean notNull = true;
					for (int i = baseindex; i < baseindex + arrayElements; i++)
						notNull = notNull && objvalues[i] != null;

					if (notNull) {
						try {
							java.lang.Number valueMonth = this.parseNumber(objvalues[baseindex]);
							java.lang.Number valueDay = this.parseNumber(objvalues[baseindex + 1]);
							java.lang.Number valueYear = this.parseNumber(objvalues[baseindex + 2]);
							java.lang.Number valueHour = this.parseNumber(objvalues[baseindex + 3]);
							java.lang.Number valueMinute = this.parseNumber(objvalues[baseindex + 4]);
							java.lang.Number valueSecond = enableSeconds ? this.parseNumber(objvalues[baseindex + 5]) : null;
							String valueAmPm = (String) objvalues[baseindex + arrayElements - 1];

							if (valueMonth == null || valueDay == null || valueYear == null || valueHour == null || valueMinute == null || valueAmPm == null
									|| (enableSeconds && valueSecond == null)) {
								datetimes.add(null);
							} else {
								int month = valueMonth.intValue();
								int day = valueDay.intValue();
								int year = valueYear.intValue();
								int hour = valueHour.intValue() % 12;
								int minute = valueMinute.intValue();
								int second = enableSeconds ? valueSecond.intValue() : 0;
								if (TimeAmPm.PM.equals(TimeAmPm.valueOf(valueAmPm))) hour += 12;
								datetimes.add(LocalDateTime.of(year, month, day, hour, minute, second));
							}

							isArray = true;
							baseindex += arrayElements;
						} catch (NumberFormatException nfe) {
							// suppress
						} catch (IllegalArgumentException iae) {
							// suppress
						}
					}
				}

				if (!isArray) {
					if (objvalues[baseindex] == null) {
						datetimes.add(null);
					} else if (objvalues[baseindex] instanceof LocalDateTime) {
						datetimes.add((LocalDateTime) objvalues[baseindex]);
					} else if (objvalues[baseindex] instanceof Calendar) {
						datetimes.add(((Calendar) objvalues[baseindex]).toInstant()
								.atZone(ZoneId.systemDefault())
								.toLocalDateTime());
					} else if (objvalues[baseindex] instanceof java.util.Date) {
						datetimes.add(Instant.ofEpochMilli(((java.util.Date) objvalues[baseindex]).getTime())
								.atZone(ZoneId.systemDefault())
								.toLocalDateTime());
					} else if (objvalues[baseindex] instanceof String) {
						datetimes.add(this.parseDateTime((String) objvalues[baseindex]));
					} else {
						throw new IllegalArgumentException();
					}

					baseindex++;
				}
			}
		} else {
			throw new IllegalArgumentException("The type, '" + value.getClass() + "', is not supported for a datetime");
		}

		return datetimes;
	}

	private Object prepValue(Object value) {
		if (value instanceof Collection<?>) {
			Collection<?> dvalues = (Collection<?>) value;
			Object[] objvalues = new Object[dvalues.size()];
			int count = 0;
			for (Object cvalue : dvalues)
				objvalues[count++] = cvalue;
			return objvalues;
		} else if (value instanceof String && "".equals(value)) {
			return null;
		} else {
			return value;
		}
	}

	private LocalDate parseDate(String strvalue) {
		strvalue = strvalue.replace('/', '-');
		TemporalAccessor datetime = this.parse(DATE_FORMATTERS, strvalue);
		return datetime == null ? null : LocalDate.from(datetime);
	}

	private LocalTime parseTime(String strvalue) {
		TemporalAccessor datetime = this.parse(TIME_FORMATTERS, strvalue);
		return datetime == null ? null : LocalTime.from(datetime);
	}

	private LocalDateTime parseDateTime(String strvalue) {
		strvalue = strvalue.replace('/', '-');
		TemporalAccessor datetime = this.parse(DATETIME_FORMATTERS, strvalue);
		return datetime == null ? null : LocalDateTime.from(datetime);
	}

	private TemporalAccessor parse(DateTimeFormatter[] formatters, String strvalue) {
		for (int i = 0; i < formatters.length; i++) {
			try {
				return formatters[i].parse(strvalue);
			} catch (IllegalArgumentException iae) {
				// suppress
			}
		}

		return null;
	}

	private java.lang.Number parseNumber(Object value) {
		if (value == null) {
			return null;
		} else if (value instanceof java.lang.Number) {
			return (java.lang.Number) value;
		} else if (value instanceof String) {
			String strvalue = (String) value;
			return strvalue.length() == 0 ? null : new Integer(strvalue);
		} else {
			throw new IllegalArgumentException("A calendar value cannot be set using the type, '" + value.getClass() + "'");
		}
	}

}
