package com.brianlong.util;

import java.util.LinkedList;
import java.util.List;

public class ServiceManager {

	private List<ServiceListener> listeners = new LinkedList<ServiceListener>();

	public void register(ServiceListener listener) {
		this.listeners.add(listener);
	}

	public void startup() {
		for (ServiceListener listener : this.listeners)
			listener.serviceStarted();
	}

	public void shutdown() {
		for (ServiceListener listener : this.listeners)
			listener.serviceStopped();
	}

}
