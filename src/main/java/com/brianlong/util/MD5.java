package com.brianlong.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MD5 {

	private static final Logger LOGGER = LoggerFactory.getLogger(MD5.class);

	public static String compute() {
		String data = String.valueOf(System.currentTimeMillis()) + '~' + String.valueOf(new Random().nextLong());
		return MD5.compute(data);
	}

	public static String compute(String data) {
		return MD5.compute(data.getBytes());
	}

	public static String compute(byte[] data) {
		try {
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			md5.update(data);
			byte[] md5sum = md5.digest();
			return MD5.toHexString(md5sum);
		} catch (NoSuchAlgorithmException nsae) {
			// this error should never happen
			LOGGER.error(nsae.getMessage(), nsae);
			return null;
		}
	}

	private static String toHexString(byte[] md5sum) {
		StringBuilder hexs = new StringBuilder();
		for (int i = 0; i < md5sum.length; i++) {
			String hex = Integer.toHexString(md5sum[i]);
			if (hex.length() < 2) hexs.append('0');
			hexs.append(hex);
		}
		return hexs.toString();
	}

}
