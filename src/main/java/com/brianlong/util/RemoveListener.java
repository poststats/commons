package com.brianlong.util;

public interface RemoveListener<T> extends Listener<T> {

	void removed(T value);

}
