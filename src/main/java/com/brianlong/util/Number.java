package com.brianlong.util;

import java.lang.reflect.Constructor;

/**
 * @author Brian M Long
 */
public class Number {

	public static String extract(String str) {
		if (str == null) return null;

		StringBuilder strbuilder = new StringBuilder();

		for (int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);
			if (Character.isDigit(ch)) strbuilder.append(ch);
		}

		return strbuilder.toString();
	}

	public static <N extends Number> N extract(Class<N> numberClass, String str) {
		str = Number.extract(str);
		if (str == null) return null;

		try {
			Constructor<N> cons = numberClass.getConstructor(new Class[] { String.class });
			return cons.newInstance(new Object[] { str });
		} catch (RuntimeException re) {
			throw re;
		} catch (Exception e) {
			throw new IllegalArgumentException("The class, '" + numberClass.getName() + "', must have a public 'java.lang.String' constructor");
		}
	}

}
