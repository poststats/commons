package com.brianlong.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Brian M Long
 */
public abstract class JavaBean implements Cloneable {

	public JavaBean() {
	}

	public JavaBean(Object source) {
		this(source, false);
	}

	public JavaBean(Object source, boolean deep) {
		this.clone(source, deep);
	}

	public Map<String, Object> cloneToMap(boolean deep) {
		Map<String, Object> target = new HashMap<String, Object>();
		if (deep) JavaBeanUtils.doDeepCopy(this, target, Byte.MAX_VALUE);
		else JavaBeanUtils.doShallowCopy(this, target);
		return target;
	}

	public void clone(Object source) {
		this.clone(source, false);
	}

	public void clone(Object source, boolean deep) {
		if (deep) JavaBeanUtils.doDeepCopy(source, this, Byte.MAX_VALUE);
		else JavaBeanUtils.doShallowCopy(source, this);
	}

	@Override
	public Object clone() {
		return this.clone(false);
	}

	public Object clone(boolean deep) {
		return deep ? JavaBeanUtils.createDeepCopy(this) : JavaBeanUtils.createShallowCopy(this);
	}

	@Override
	public boolean equals(Object obj) {
		return JavaBeanUtils.doShallowBeanComparison(this, obj);
	}

	@Override
	public abstract int hashCode();

	@Override
	public String toString() {
		return this.getClass()
				.getSimpleName() + " Java Bean";
	}

}
