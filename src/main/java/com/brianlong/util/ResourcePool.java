package com.brianlong.util;

import com.brianlong.thread.PeriodicExecutor;
import com.brianlong.thread.ResourceSemaphore;
import java.util.Iterator;
import java.util.Stack;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Brian M Long
 */
public abstract class ResourcePool<T> {

	private final String id;
	private final int minimum;
	private final Stack<Resource> unused;
	private final ResourceSemaphore rsemaphore;
	private Reaper reaper;

	private static final Logger LOGGER = LoggerFactory.getLogger(ResourcePool.class);

	public ResourcePool(String id, Integer minimum, Integer maximum, long expiration) {
		if (minimum == null || minimum.intValue() < 0) minimum = 0;
		if (maximum == null || maximum.intValue() < 0) maximum = Integer.MAX_VALUE;
		if (minimum > maximum) throw new IllegalArgumentException("The minimum cannot be greater than the maximum");

		this.id = id;
		this.minimum = minimum;
		this.unused = new Stack<Resource>();
		this.rsemaphore = new ResourceSemaphore(maximum);

		this.startReaper(expiration, expiration);
	}

	@Override
	protected void finalize() throws Throwable {
		try {
			this.shutdown();
		} finally {
			super.finalize();
		}
	}

	public String getId() {
		return this.id;
	}

	protected abstract T create() throws Exception;

	protected abstract boolean checkValidity(T obj) throws Exception;

	protected abstract void destroy(T obj) throws Exception;

	public T acquire(boolean wait) {
		return this.acquire(wait, 0L);
	}

	public T acquire(long ms) {
		return this.acquire(true, ms);
	}

	private T acquire(boolean wait, long ms) {
		synchronized (this.rsemaphore) {
			while (true) {
				if (this.unused.size() > 0) {
					T obj = this.unused.pop().obj;

					try {
						if (this.checkValidity(obj)) return obj;
					} catch (RuntimeException re) {
						this.rsemaphore.release();
						throw re;
					} catch (Exception e) {
						LOGGER.warn("Failed to acquire a resource of " + this.getClass()
								.getName() + "; trying another resource", e);
					}

					this.rsemaphore.release();
					return this.acquire(wait, ms);
				} else {
					if (!wait && !this.rsemaphore.acquireNow()) return null;
					if (wait && !this.rsemaphore.acquire(ms)) return null;

					try {
						return this.create();
					} catch (RuntimeException re) {
						this.rsemaphore.release();
						throw re;
					} catch (Exception e) {
						LOGGER.error("Failed to create a resource of " + this.getClass()
								.getName() + "; returning null", e);
						this.rsemaphore.release();
						return null;
					}
				}
			}
		}
	}

	public void release(T obj) {
		synchronized (this.rsemaphore) {
			if (obj != null) {
				this.unused.push(new Resource(obj));
				this.rsemaphore.release();
			}
		}
	}

	public int getPoolSize() {
		synchronized (this.rsemaphore) {
			return this.rsemaphore.getAcquiredThreads() + this.unused.size();
		}
	}

	public void shutdown() throws InterruptedException {
		synchronized (this.rsemaphore) {
			this.stopReaper();

			for (Resource resource : this.unused) {
				try {
					this.destroy(resource.obj);
				} catch (Exception e) {
					LOGGER.warn(e.getMessage(), e);
				}
			}

			this.unused.clear();
		}
	}

	private void startReaper(long objExpiration, long reapInterval) {
		this.reaper = new Reaper(this, objExpiration, reapInterval);
	}

	private void stopReaper() throws InterruptedException {
		if (this.reaper != null) {
			this.reaper.shutdownNow();
			this.reaper.awaitTermination(0, TimeUnit.MILLISECONDS);
			this.reaper = null;
		}
	}

	private class Resource {

		private T obj;
		private long created;

		public Resource(T obj) {
			this.obj = obj;
			this.created = System.currentTimeMillis();
		}

	}

	private class Reaper extends PeriodicExecutor implements Runnable {

		private ResourcePool<T> pool;
		private long expire;

		public Reaper(ResourcePool<T> pool, long objExpiration, long reapInterval) {
			super(pool.getId(), reapInterval, reapInterval, TimeUnit.MILLISECONDS);

			this.pool = pool;
			this.expire = objExpiration;
			this.execute(this);
		}

		public void run() {
			long expiration = System.currentTimeMillis() - this.expire;

			synchronized (this.pool.rsemaphore) {
				Iterator<Resource> i = this.pool.unused.iterator();
				while (i.hasNext()) {
					Resource resource = i.next();
					if (resource.created < expiration && this.pool.minimum < this.pool.getPoolSize()) {
						try {
							this.pool.destroy(resource.obj);
						} catch (RuntimeException re) {
							throw re;
						} catch (Exception e) {
							LOGGER.warn("Failed to destroy a resource of " + this.getClass() + "; ignoring", e);
						} finally {
							i.remove();
						}
					}
				}
			}
		}

	}

}
