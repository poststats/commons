package com.brianlong.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

public class Collections {

	public static String implode(Collection<?> c, String delimiter) {
		if (c.isEmpty()) return "";

		StringBuilder sbuilder = new StringBuilder();
		for (Object s : c)
			sbuilder.append(s.toString())
					.append(delimiter);
		return sbuilder.substring(0, sbuilder.length() - delimiter.length());
	}

	public static List<String> explode(String str, String delimiter) {
		return explode(str, delimiter, String.class);
	}

	public static <T> List<T> explode(String str, String delimiter, Class<T> type) {
		StringTokenizer tokens = new StringTokenizer(str, delimiter);

		List<T> strs = new LinkedList<T>();
		while (tokens.hasMoreTokens()) {
			try {
				Constructor<T> constructor = type.getConstructor(String.class);
				strs.add(constructor.newInstance(tokens.nextToken()));
			} catch (NoSuchMethodException nsme) {
				throw new RuntimeException("This should never happen");
			} catch (InvocationTargetException ite) {
				throw new RuntimeException("This should never happen");
			} catch (IllegalAccessException iae) {
				throw new RuntimeException("This should never happen");
			} catch (InstantiationException ie) {
				throw new RuntimeException("This should never happen");
			}
		}
		return strs;
	}

	public static boolean intersects(Collection<?> c1, Collection<?> c2) {
		for (Object e1 : c1)
			if (c2.contains(e1)) return true;
		return false;
	}

	public static <T> void intersect(Collection<T> c1, Collection<T> c2, Collection<T> i) {
		for (T e1 : c1)
			if (c2.contains(e1)) i.add(e1);
	}

}
