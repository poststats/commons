package com.brianlong.util;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * @author Brian M Long
 */
public class Unsorter implements Sorter {

	public <C extends Comparable<C>> void sort(List<C> items) {
		this.sort(items, null, true);
	}

	public <C extends Comparable<C>> void sort(List<C> items, boolean ascending) {
		this.sort(items, new ComparableComparator<C>(), ascending);
	}

	public <K, C extends Comparable<C>, M extends Map<K, C>> void sortMapByField(List<M> items, K sortBy, boolean ascending) {
		this.sort(items, new SimpleMapComparator<K, C, M>(sortBy), ascending);
	}

	public <O> void sort(List<O> items, Comparator<O> comparator, boolean ascending) {
		if (items.size() < 2) return;

		Random randomizer = new Random();

		for (int i = items.size() - 1; i > 0; i--) {
			int j = randomizer.nextInt(i + 1);
			if (i != j) items.add(items.remove(j));
		}
	}

	public <K, O, M extends Map<K, O>> void sortMapByField(List<M> items, K sortBy, Comparator<O> comparator, boolean ascending) {
		this.sort(items, new MapComparator<K, O, M>(sortBy, comparator), ascending);
	}

	public <C extends Comparable<C>> void sort(C[] items) {
		this.sort(items, null, true);
	}

	public <C extends Comparable<C>> void sort(C[] items, boolean ascending) {
		this.sort(items, new ComparableComparator<C>(), ascending);
	}

	public <O> void sort(O[] items, Comparator<O> comparator, boolean ascending) {
		if (items.length < 2) return;

		Random randomizer = new Random();

		for (int i = items.length - 1; i > 0; i--) {
			int j = randomizer.nextInt(i + 1);
			if (i != j) this.swap(items, i, j);
		}
	}

	private <O> void swap(O[] items, int i1, int i2) {
		O item = items[i1];
		items[i1] = items[i2];
		items[i2] = item;
	}

}
