package com.brianlong.util;

public interface ServiceListener {

	void serviceStarted();

	void serviceStopped();

}
