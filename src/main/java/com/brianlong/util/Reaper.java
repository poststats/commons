package com.brianlong.util;

import java.util.concurrent.Callable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Brian M Long
 */
public abstract class Reaper implements Callable<Void> {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	private final long interval;
	private boolean done = false;

	public Reaper(long reapInterval) {
		this.interval = reapInterval;
	}

	public void stop() {
		this.done = true;
		this.notify();
	}

	public abstract void reap();

	@Override
	public Void call() {
		if (LOGGER.isInfoEnabled()) LOGGER.info("Reaper started");

		try {

			while (!this.done) {
				if (LOGGER.isDebugEnabled()) LOGGER.debug("call(): reaping ...");
				try {
					this.reap();
				} catch (RuntimeException re) {
					LOGGER.warn("Reaper ran into an unexpected issue: " + re.getMessage(), re);
				} catch (Error e) {
					LOGGER.warn("Reaper ran into an unexpected issue: " + e.getMessage(), e);
				} finally {
					if (LOGGER.isDebugEnabled()) LOGGER.debug("call(): reaped ...");
				}

				this.wait(this.interval);
			}

			if (LOGGER.isInfoEnabled()) LOGGER.info("Reaper stopped gracefully");
		} catch (InterruptedException ie) {
			if (LOGGER.isInfoEnabled()) LOGGER.info("Reaper interrupted gracefully");
			Thread.currentThread()
					.interrupt();
		}

		return null;
	}

}
