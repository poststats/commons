package com.brianlong.util;

import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/**
 * This class implements the famous bubble sorting algorithm.
 *
 * A bubble sort is a very simple sort. It is usually not the fastest or
 * efficient sorting method. However, sometimes it is the fastest and the
 * algorithm must be considered when making a sorting algorithm choice.
 *
 * A bubble sort looks at adjacent items and if swapping the items makes the
 * items "more sorted", then the items are swapped. This method goes through all
 * adjacent items from top to bottom. The process loops from top to bottom until
 * no swaps are made. The items are then considered to be sorted.
 *
 * A bidirection bubble sort goes from top to bottom and then bottom to top.
 * This is considerably better when "low" items are at the bottom.
 *
 * If a few random items are being added to the top of existing sorted items, a
 * unidirectional bubble sort is probably better than all other sorting
 * algorithms.
 *
 * If a bunch of items are very close to already being sorted, but some random
 * spots are slightly out of place, then a bidirectional bubble sort is probably
 * better than all other sorting algorithms.
 *
 * @author Brian M Long
 */
public class BubbleSorter implements Sorter {

	private boolean bidirectional;

	public BubbleSorter(boolean bidirectional) {
		this.bidirectional = bidirectional;
	}

	/**
	 * @see Sorter#sort(java.util.List, boolean)
	 */
	public <C extends Comparable<C>> void sort(List<C> items, boolean ascending) {
		this.sort(items, new ComparableComparator<C>(), ascending);
	}

	/**
	 * @see Sorter#sortMapByField(java.util.List, java.lang.Object, boolean)
	 */
	public <K, C extends Comparable<C>, M extends Map<K, C>> void sortMapByField(List<M> items, K sortBy, boolean ascending) {
		this.sort(items, new SimpleMapComparator<K, C, M>(sortBy), ascending);
	}

	/**
	 * @see Sorter#sort(java.util.List, java.util.Comparator, boolean)
	 */
	public <O> void sort(List<O> items, Comparator<O> comparator, boolean ascending) {
		if (items.size() < 2) return;
		ListIterator<O> i = items.listIterator();

		int changes = 1;
		while (changes > 0) {
			changes = 0;

			O o1 = i.next();
			while (i.hasNext()) {
				O o2 = i.next();

				int compareTo = comparator.compare(o1, o2);
				if ((ascending && compareTo > 0) || (!ascending && compareTo < 0)) {
					i.remove();
					i.previous();
					i.add(o2);
					i.next();
					changes++;
				} else {
					o1 = o2;
				}
			}

			if (this.bidirectional && changes > 0) {
				o1 = i.previous();
				while (i.hasPrevious()) {
					O o2 = i.previous();

					int compareTo = comparator.compare(o2, o1);
					if ((ascending && compareTo > 0) || (!ascending && compareTo < 0)) {
						i.remove();
						i.next();
						i.add(o2);
						i.previous();
						i.previous();
						changes++;
					} else {
						o1 = o2;
					}
				}
			} else {
				i = items.listIterator();
			}
		}
	}

	/**
	 * @see Sorter#sortMapByField(java.util.List, java.lang.Object, boolean)
	 */
	public <K, O, M extends Map<K, O>> void sortMapByField(List<M> items, K sortBy, Comparator<O> comparator, boolean ascending) {
		this.sort(items, new MapComparator<K, O, M>(sortBy, comparator), ascending);
	}

	/**
	 * @see Sorter#sort(java.lang.Comparable[], boolean)
	 */
	public <C extends Comparable<C>> void sort(C[] items, boolean ascending) {
		this.sort(items, new ComparableComparator<C>(), ascending);
	}

	/**
	 * @see Sorter#sort(java.lang.Object[], java.util.Comparator, boolean)
	 */
	public <O> void sort(O[] items, Comparator<O> comparator, boolean ascending) {
		if (items.length < 2) return;

		int changes = 1;
		while (changes > 0) {
			changes = 0;
			for (int i = 1; i < items.length; i++) {
				O o1 = items[i - 1];
				O o2 = items[i];

				int compareTo = comparator.compare(o1, o2);
				if ((ascending && compareTo > 0) || (!ascending && compareTo < 0)) {
					items[i - 1] = o2;
					items[i] = o1;
					changes++;
				} else {
					o1 = o2;
				}
			}

			if (this.bidirectional && changes > 0) {
				changes = 0;
				for (int i = items.length - 1; i > 0; i--) {
					O o1 = items[i];
					O o2 = items[i - 1];

					int compareTo = comparator.compare(o2, o1);
					if ((ascending && compareTo > 0) || (!ascending && compareTo < 0)) {
						items[i - 1] = o1;
						items[i] = o2;
						changes++;
					} else {
						o1 = o2;
					}
				}
			}
		}
	}

}
