package com.brianlong.util;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * @author Brian M Long
 */
public interface Sorter {

	/**
	 * This method will sort the specified list.
	 *
	 * @param items     A list of java.lang.Comparable instances.
	 * @param ascending Whether or not to sort in ascending or descending order.
	 */
	<C extends Comparable<C>> void sort(List<C> items, boolean ascending);

	/**
	 * This method will sort the specified list of maps.
	 *
	 * @param items     A list of maps.
	 * @param sortBy    A field in the maps whose value to sort by.
	 * @param ascending Whether or not to sort in ascending or descending order.
	 */
	<K, C extends Comparable<C>, M extends Map<K, C>> void sortMapByField(List<M> items, K sortBy, boolean ascending);

	/**
	 * This method will sort the specified list using the specified
	 * java.lang.Comparator.
	 *
	 * This method is only needed if the items are not java.lang.Comparable.
	 *
	 * @param items      A list of anything.
	 * @param comparator A comparator instance that compares the individual list
	 *                   items.
	 * @param ascending  Whether or not to sort in ascending or descending order.
	 */
	<O> void sort(List<O> items, Comparator<O> comparator, boolean ascending);

	/**
	 * This method will sort the specified list of maps.
	 *
	 * @param items      A list of maps.
	 * @param sortBy     A field in the maps whose value to sort by.
	 * @param comparator A comparator instance that compares the individual list
	 *                   items.
	 * @param ascending  Whether or not to sort in ascending or descending order.
	 */
	<K, O, M extends Map<K, O>> void sortMapByField(List<M> items, K sortBy, Comparator<O> comparator, boolean ascending);

	/**
	 * This method will sort the specified array of java.lang.Comparable instances.
	 *
	 * @param items     An array of java.lang.Comparable instances.
	 * @param ascending Whether or not to sort in ascending or descending order.
	 */
	<C extends Comparable<C>> void sort(C[] items, boolean ascending);

	/**
	 * This method will sort the specified array using the specified
	 * java.lang.Comparator.
	 *
	 * This method is only needed if the items are not java.lang.Comparable.
	 *
	 * @param items      An array of anything.
	 * @param comparator A comparator instance that compares the individual array
	 *                   items.
	 * @param ascending  Whether or not to sort in ascending or descending order.
	 */
	<O> void sort(O[] items, Comparator<O> comparator, boolean ascending);

	/**
	 * This class encapsulates the java.lang.Comparable interface inside a
	 * java.lang.Comparator.
	 *
	 * This class is only useful because all the sorts are done using a
	 * java.lang.Comparator. This eliminates the need for a "special case".
	 */
	public class ComparableComparator<C extends Comparable<C>> implements java.util.Comparator<C> {

		public int compare(C c1, C c2) {
			return c1.compareTo(c2);
		}

	}

	/**
	 * This class encapsulates an object inside a java.lang.Comparator. It assumes
	 * that the object has a 'compareTo' method.
	 *
	 * This class is only useful because all the sorts are done using a
	 * java.lang.Comparator. This eliminates the need for a "special case".
	 */
	public class AssumedComparator implements java.util.Comparator<Object> {

		@SuppressWarnings("unchecked")
		public int compare(Object o1, Object o2) {
			if (!(o1 instanceof Comparable) || !(o2 instanceof Comparable)) throw new IllegalArgumentException();

			return ((Comparable<Object>) o1).compareTo(o2);
		}

	}

	/**
	 * This class allows for the comparison between maps using just one field of two
	 * map instances.
	 *
	 * This is especially useful when sorting a list of rows returned from a
	 * database.
	 */
	public class SimpleMapComparator<K, C extends Comparable<C>, M extends Map<K, C>> implements java.util.Comparator<M> {

		private K sortBy;

		public SimpleMapComparator(K sortBy) {
			this.sortBy = sortBy;
		}

		public int compare(M m1, M m2) {
			C c1 = m1.get(this.sortBy);
			return c1.compareTo(m2.get(this.sortBy));
		}

	}

	/**
	 * This class allows for the comparison between maps using just one field of two
	 * map instances.
	 *
	 * This is especially useful when sorting a list of rows returned from a
	 * database.
	 */
	public class MapComparator<K, O, M extends Map<K, O>> implements java.util.Comparator<M> {

		private K sortBy;
		private Comparator<O> comparator;

		public MapComparator(K sortBy, Comparator<O> comparator) {
			this.sortBy = sortBy;
			this.comparator = comparator;
		}

		public int compare(M m1, M m2) {
			return this.comparator.compare((O) m1.get(this.sortBy), (O) m2.get(this.sortBy));
		}

	}

}
