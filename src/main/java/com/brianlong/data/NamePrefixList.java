package com.brianlong.data;

import java.util.LinkedList;

public class NamePrefixList extends LinkedList<String> {

	private static final long serialVersionUID = 1L;
	private static final NamePrefixList INSTANCE = new NamePrefixList();

	public static NamePrefixList getInstance() {
		return NamePrefixList.INSTANCE;
	}

	private NamePrefixList() {
		this.add("Dr");
		this.add("Mr");
		this.add("Mrs");
		this.add("Ms");
	}

}
