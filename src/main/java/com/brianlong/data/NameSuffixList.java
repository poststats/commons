package com.brianlong.data;

import java.util.LinkedList;

public class NameSuffixList extends LinkedList<String> {

	private static final long serialVersionUID = 1L;
	private static final NameSuffixList INSTANCE = new NameSuffixList();

	public static NameSuffixList getInstance() {
		return NameSuffixList.INSTANCE;
	}

	private NameSuffixList() {
		this.add("Sr");
		this.add("Jr");
		this.add("III");
		this.add("IV");
		this.add("V");
	}

}
