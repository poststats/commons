package com.brianlong.data;

import java.util.LinkedHashMap;

public class GenderMap extends LinkedHashMap<String, String> {

	private static final long serialVersionUID = 1L;
	private static final GenderMap INSTANCE = new GenderMap();

	public static GenderMap getInstance() {
		return GenderMap.INSTANCE;
	}

	private GenderMap() {
		this.put("F", "Female");
		this.put("M", "Male");
	}

}
