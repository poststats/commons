package com.brianlong;

public class DeveloperException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public DeveloperException() {
		super("This should never happen");
	}

	public DeveloperException(String message) {
		super(message);
	}

	public DeveloperException(Throwable t) {
		super("This should never happen", t);
	}

}
