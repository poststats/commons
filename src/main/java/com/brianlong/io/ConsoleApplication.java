package com.brianlong.io;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeoutException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConsoleApplication implements Runnable {

	private final Logger logger;
	private final File executable;
	private final List<String> arguments = new LinkedList<String>();

	public ConsoleApplication(File executableDirectory, String executable) {
		this.logger = LoggerFactory.getLogger(this.getClass());
		this.executable = new File(executableDirectory, executable);
	}

	public void addArgument(String arg) {
		this.arguments.add(arg);
	}

	public void addArgument(File file) {
		this.arguments.add(file.getAbsolutePath());
	}

	public void addArgument(File file, int page) {
		this.arguments.add(file.getAbsolutePath() + "[" + (page - 1) + "]");
	}

	public void addArguments(String arg1, String arg2) {
		this.arguments.add(arg1);
		this.arguments.add(arg2);
	}

	@Override
	public void run() {
		try {
			this.execute(60000L);
		} catch (InterruptedException ie) {
			this.logger.warn("The '" + this.executable.getAbsolutePath() + "' application was interrupted and stopped gracefully");
		} catch (TimeoutException te) {
			this.logger.warn("The '" + this.executable.getAbsolutePath() + "' application timed out and stopped gracefully");
		} catch (IOException ie) {
			this.logger.warn("The '" + this.executable.getAbsolutePath() + "' application failed to execute: " + ie.getMessage());
		}
	}

	public ConsoleApplicationResult execute(final long timeout) throws InterruptedException, TimeoutException, IOException {
		ConsoleApplicationInstance cat = this.executeAsync(timeout);
		return cat.waitForCompletion();
	}

	public ConsoleApplicationInstance executeAsync(final long timeout) throws InterruptedException, TimeoutException, IOException {
		return new ConsoleApplicationInstance(this.executable, this.arguments, timeout);
	}

}
