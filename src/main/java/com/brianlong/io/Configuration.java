package com.brianlong.io;

import com.brianlong.util.DateTimeFormatter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.time.temporal.TemporalAccessor;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Brian M Long
 */
public abstract class Configuration {

	private final String id = UUID.randomUUID()
			.toString();
	private Reloader reloader;
	private Thread reloaderThread;

	private static final Logger LOGGER = LoggerFactory.getLogger(Configuration.class);

	public String getId() {
		return this.id;
	}

	/**
	 * This method retrieves the configuration's source location.
	 *
	 * @return A string representation of the source location.
	 */
	protected abstract String getLocation();

	/**
	 * This method manually starts the configuration reloader using the specified
	 * interval.
	 *
	 * @param minutes An reload interval in minutes.
	 */
	public void startReloader(int minutes) {
		this.reloader = new Reloader(this, minutes);
		this.reloaderThread = new Thread(this.reloader, this.getLocation() + " reloader");
		this.reloaderThread.start();
	}

	/**
	 * This method manually stops the configuration reloader.
	 *
	 * @throws java.lang.InterruptedException While waiting for the reloader to
	 *                                        stop, the calling thread was
	 *                                        interrupted.
	 */
	public void stopReloader() throws InterruptedException {
		if (this.reloader != null) {
			this.reloader.stop();
			this.reloaderThread.join();
		}

		this.reloader = null;
		this.reloaderThread = null;
	}

	protected Long getReloadInterval() {
		return null;
	}

	/**
	 * This method loads the configuration.
	 *
	 * @throws java.lang.Exception Because of unknown nature of the configuration's
	 *                             source, anything could happen.
	 */
	public abstract void load() throws Exception;

	/**
	 * This method retrieves the specified field as a string.
	 *
	 * Most configuration sources are stored as plain text strings. From the
	 * java.lang.String, most other types can be retrieved.
	 *
	 * @param field A field name.
	 * @return The string value associated with the specified field.
	 */
	public abstract String getString(String field);

	/**
	 * This method retrieves the specified field by converting the string value to a
	 * boolean value.
	 *
	 * @param field A field name.
	 * @return The boolean value associated with the specified field.
	 */
	public Boolean getBoolean(String field) {
		String str = this.getString(field);
		return str == null ? null : new Boolean(str);
	}

	/**
	 * This method retrieves the specified field by converting the string value to a
	 * numeric value.
	 *
	 * @param field A field name.
	 * @return The numeric value associated with the specified field.
	 * @throws NumberFormatException The field's value is not empty or a number.
	 */
	public Long getLong(String field) {
		String str = this.getString(field);
		return str == null ? null : new Long(str);
	}

	/**
	 * This method retrieves the specified field by converting the string value to a
	 * numeric value.
	 *
	 * @param field A field name.
	 * @return The numeric value associated with the specified field.
	 * @throws NumberFormatException The field's value is not empty or a number.
	 */
	public Double getDouble(String field) {
		String str = this.getString(field);
		return str == null ? null : new Double(str);
	}

	/**
	 * This method retrieves the specified field by converting the string value to a
	 * date/time value using the specified format.
	 *
	 * @param field            A field name.
	 * @param simpelDateFormat A format string based on the
	 *                         java.text.SimpleDateFormat specifications.
	 * @return The date value associated with the specified field.
	 * @throws ParseException The field's value is not empty or a date/time of the
	 *                        specified format.
	 */
	public TemporalAccessor getDateTime(String field, String simpleDateFormat) throws ParseException {
		String str = this.getString(field);
		return str == null ? null : new DateTimeFormatter(simpleDateFormat).parse(str);
	}

	/**
	 * This method retrieves the specified field by converting the string value to a
	 * file value.
	 *
	 * @param field A field name.
	 * @return A file object.
	 */
	public File getFile(String field) {
		String str = this.getString(field);
		return str == null ? null : new File(str);
	}

	/**
	 * This method retrieves the specified field by converting the string value to a
	 * file value.
	 *
	 * @param field           A field name.
	 * @param mustExist       true if the file must exist; false could either exist
	 *                        or not.
	 * @param mustBeDirectory true if file must be a directory; false could be
	 *                        either a file or directory.
	 * @return A file object.
	 */
	public File getFile(String field, boolean mustExist, boolean mustBeDirectory) throws FileNotFoundException {
		File file = this.getFile(field);
		if (mustExist && (file == null || !file.exists())) throw new FileNotFoundException("The '" + this.getString(field) + "' file must exists");
		if (mustBeDirectory && !file.isDirectory()) throw new FileNotFoundException("The '" + file + "' file must be a directory");
		return file;
	}

	private class Reloader implements Runnable {

		private Configuration config;
		private long reloadInterval;
		private boolean keepRunning = true;

		public Reloader(Configuration config, int minutes) {
			this.config = config;
			this.reloadInterval = minutes * 60000;
		}

		public synchronized void stop() {
			this.keepRunning = false;
			this.notify();
		}

		public synchronized void run() {
			try {
				do {
					try {
						this.config.load();

						Long newReloadInterval = this.config.getReloadInterval();
						if (newReloadInterval != null) this.reloadInterval = newReloadInterval.longValue();

						this.wait(this.reloadInterval);
					} catch (ConfigurationException ce) {
						String msg = "The configuration file, '" + this.config.getLocation() + "', failed to (re)load";
						LOGGER.warn(msg, ce);
					} catch (IOException ie) {
						String msg = "The configuration file, '" + this.config.getLocation() + "', failed to (re)load";
						LOGGER.warn(msg, ie);
					}
				} while (this.keepRunning);
			} catch (InterruptedException ie) {
				// gracefully exit
			} catch (Exception e) {
				String msg = "The configuration file, '" + this.config.getLocation() + "', has stopped auto-reloading";
				LOGGER.warn(msg, e);
			}
		}

	}

}
