package com.brianlong.io;

import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;

public class StringOutputStream extends OutputStream {

	private CharsetDecoder csdecoder;
	private StringBuffer strbuilder;

	public StringOutputStream(Charset charset) {
		this.csdecoder = charset.newDecoder();
		this.strbuilder = new StringBuffer();
	}

	public StringOutputStream(Charset charset, int initialCapacity) {
		this.csdecoder = charset.newDecoder();
		this.strbuilder = new StringBuffer(initialCapacity);
	}

	public void close() {
		// do nothing
	}

	public void flush() {
		// do nothing
	}

	public void write(int b) {
		throw new UnsupportedOperationException();
	}

	public void write(byte[] b) throws CharacterCodingException {
		CharBuffer cbuffer = this.csdecoder.decode(ByteBuffer.wrap(b));
		this.strbuilder.append(cbuffer.array());
	}

	public void write(byte[] b, int off, int len) throws CharacterCodingException {
		CharBuffer cbuffer = this.csdecoder.decode(ByteBuffer.wrap(b, off, len));
		this.strbuilder.append(cbuffer.array());
	}

	public String toString() {
		return this.strbuilder.toString();
	}

	public StringBuffer getStringBuilder() {
		return this.strbuilder;
	}

}
