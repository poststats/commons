package com.brianlong.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class RunnableStreamReader implements Runnable {

	private final InputStream istream;
	private final OutputStream ostream;
	private IOException ie = null;
	private byte[] bytes = null;

	public RunnableStreamReader(InputStream istream) {
		this.istream = istream;
		this.ostream = null;
	}

	public RunnableStreamReader(InputStream istream, OutputStream ostream) {
		this.istream = istream;
		this.ostream = ostream;
	}

	@Override
	public void run() {
		ChunkedInputStream cistream = new ChunkedInputStream(this.istream);
		try {
			try {
				if (this.ostream == null) this.bytes = cistream.readFully(1024);
				else cistream.readFully(1024, this.ostream);
			} finally {
				cistream.close();
			}
		} catch (IOException ie) {
			this.ie = ie;
		}
	}

	public IOException getIOException() {
		return this.ie;
	}

	public byte[] getBytes() {
		return this.bytes;
	}

}
