package com.brianlong.io;

import com.brianlong.thread.ReadWriteSemaphore;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class implements an easy and powerful property file accessor.
 *
 * This class allows the developer to load a property file the traditional way
 * (as a java classpath resource) or through a URI. The URI can be any supported
 * protocol, including file://, ftp://, or http://.
 *
 * Once the property file is loaded, the contents are mapped and cached. Do not
 * store property values in another class; always "get" the value from this
 * class.
 *
 * This class supports a property file reloading through the Configuration super
 * class. There are two ways to enable the reloader: calling 'startReloader' or
 * adding 'RELOAD_INTERVAL' to the property file itself. A reload interval is
 * always expressed in minutes.
 *
 * @author Brian M Long
 */
public class PropertyConfiguration extends Configuration {

	private String resource;
	private URL url;
	private Properties props;
	private ReadWriteSemaphore rwsemaphore = new ReadWriteSemaphore();

	private static final Logger LOGGER = LoggerFactory.getLogger(PropertyConfiguration.class);
	private static final Map<String, PropertyConfiguration> INSTANCES = new HashMap<String, PropertyConfiguration>();

	public static PropertyConfiguration getInstance(String resource) throws IOException {
		synchronized (PropertyConfiguration.INSTANCES) {
			if (!PropertyConfiguration.INSTANCES.containsKey(resource)) {
				PropertyConfiguration config = new PropertyConfiguration(resource);
				config.load();
				PropertyConfiguration.INSTANCES.put(resource, config);
			}
		}

		return PropertyConfiguration.INSTANCES.get(resource);
	}

	/**
	 * This constructor defines a property file relative to the classloader's
	 * classpath.
	 *
	 * @param resource A standard Java resource path.
	 */
	public PropertyConfiguration(String resource) {
		this.resource = resource;
	}

	/**
	 * This constructor defines a property file using a URI.
	 *
	 * @param uri A standard URI.
	 * @throws java.net.MalformedURLException The URI is not a valid URL.
	 */
	public PropertyConfiguration(URI uri) throws MalformedURLException {
		this.url = uri.toURL();
	}

	@Override
	public String getLocation() {
		if (this.resource != null) return this.resource;
		else return this.url.toString();
	}

	@Override
	protected Long getReloadInterval() {
		return this.getLong("RELOAD_INTERVAL");
	}

	@Override
	public void load() throws IOException {
		if (LOGGER.isDebugEnabled()) LOGGER.debug("Loading configuration file: " + this.resource);
		InputStream istream = null;

		if (this.resource != null) {
			istream = this.getClass()
					.getResourceAsStream(this.resource);
			if (istream == null) throw new IOException("The resource, '" + this.resource + "', could not be found");
		} else if (this.url != null) {
			istream = this.url.openStream();
		} else {
			throw new IllegalArgumentException();
		}

		try {
			Properties newProps = new Properties();
			newProps.load(istream);

			this.rwsemaphore.acquireWrite();
			try {
				this.props = newProps;
			} finally {
				this.rwsemaphore.releaseWrite();
			}

			if (LOGGER.isInfoEnabled()) LOGGER.info("Loaded configuration file: " + this.resource);
		} finally {
			istream.close();
		}
	}

	@Override
	public String getString(String field) {
		this.rwsemaphore.acquireRead();
		try {
			return this.props.getProperty(field);
		} finally {
			this.rwsemaphore.releaseRead();
		}
	}

}
