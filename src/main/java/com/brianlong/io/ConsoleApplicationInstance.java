package com.brianlong.io;

import com.brianlong.thread.TimeoutEnforcer;
import com.brianlong.thread.TimeoutEnforcer.Timeoutable;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeoutException;
import org.slf4j.LoggerFactory;

public class ConsoleApplicationInstance implements Timeoutable {

	private static final File TMPDIR = new File(System.getProperty("java.io.tmpdir"));

	private final Process process;
	private final Thread stdoutStreamThread;
	private final Thread stderrStreamThread;
	private final Thread timeoutEnforcerThread;
	private final ConsoleApplicationResult car;
	private boolean timedout = false;

	ConsoleApplicationInstance(File executable, List<String> arguments, long timeout) throws IOException {
		String[] commandArray = null;
		StringOutputStream stdoutStream = null;
		StringOutputStream stderrStream = null;

		boolean ignoreOS = "true".equals(System.getProperty("os.ignore"));
		if (ignoreOS) {
			stdoutStream = new StringOutputStream(Charset.defaultCharset());
			stderrStream = new StringOutputStream(Charset.defaultCharset());

			commandArray = this.generateGeneric(executable, arguments);

			this.car = new ConsoleApplicationStreamResult(stdoutStream, stderrStream);
		} else {
			File stdoutFile = File.createTempFile("stdout-", ".txt", TMPDIR);
			File stderrFile = File.createTempFile("stderr-", ".txt", TMPDIR);

			String osname = System.getProperty("os.name");
			if (osname.startsWith("Windows")) {
				commandArray = this.generateCmd(executable, arguments, stdoutFile, stderrFile);
			} else {
				commandArray = this.generateSh(executable, arguments, stdoutFile, stderrFile);
			}

			this.car = new ConsoleApplicationFileResult(stdoutFile, stderrFile);
		}

		LoggerFactory.getLogger(ConsoleApplicationInstance.class)
				.info(Arrays.toString(commandArray));
		this.process = Runtime.getRuntime()
				.exec(commandArray, null, TMPDIR);
		this.process.getOutputStream()
				.close();

		RunnableStreamReader stdoutStreamReader = new RunnableStreamReader(this.process.getInputStream(), stdoutStream);
		this.stdoutStreamThread = new Thread(stdoutStreamReader);
		this.stdoutStreamThread.start();

		RunnableStreamReader stderrStreamReader = new RunnableStreamReader(this.process.getErrorStream(), stderrStream);
		this.stderrStreamThread = new Thread(stderrStreamReader);
		this.stderrStreamThread.start();

		TimeoutEnforcer timeoutEnforcer = new TimeoutEnforcer(this, timeout);
		this.timeoutEnforcerThread = new Thread(timeoutEnforcer);
		this.timeoutEnforcerThread.start();
	}

	public ConsoleApplicationResult waitForCompletion() throws InterruptedException, TimeoutException, IOException {
		int rc = this.process.waitFor();
		if (this.car instanceof ConsoleApplicationStreamResult) ((ConsoleApplicationStreamResult) this.car).setReturnCode(rc);
		else if (this.car instanceof ConsoleApplicationFileResult) ((ConsoleApplicationFileResult) this.car).setReturnCode(rc);

		if (this.timeoutEnforcerThread.isAlive()) this.timeoutEnforcerThread.interrupt();
		if (this.stderrStreamThread.isAlive()) this.stderrStreamThread.interrupt();
		if (this.stdoutStreamThread.isAlive()) this.stdoutStreamThread.interrupt();

		if (this.timedout) throw new TimeoutException("A console application timed out");

		return this.car;
	}

	@Override
	public void timeout() {
		try {
			this.process.exitValue();
			this.timedout = false;
		} catch (IllegalThreadStateException itse) {
			this.timedout = true;
			this.process.destroy();
		}
	}

	private String[] generateCmd(File executable, List<String> arguments, File stdoutFile, File stderrFile) {
		int c = 0;
		String[] commandArray = new String[7 + arguments.size()];
		commandArray[c++] = "cmd";
		commandArray[c++] = "/c";
		commandArray[c++] = executable.getAbsolutePath();
		for (String argument : arguments)
			commandArray[c++] = argument;
		commandArray[c++] = "1>";
		commandArray[c++] = stdoutFile.getAbsolutePath();
		commandArray[c++] = "2>";
		commandArray[c++] = stderrFile.getAbsolutePath();
		return commandArray;
	}

	private String[] generateSh(File executable, List<String> arguments, File stdoutFile, File stderrFile) {
		String[] commandArray = new String[3];
		commandArray[0] = "/bin/sh";
		commandArray[1] = "-c";

		StringBuilder strbuilder = new StringBuilder();
		strbuilder.append("'")
				.append(executable.getAbsolutePath())
				.append("' ");
		for (String argument : arguments)
			strbuilder.append("'")
					.append(argument)
					.append("' ");
		strbuilder.append("1> ")
				.append("'")
				.append(stdoutFile.getAbsolutePath())
				.append("' ");
		strbuilder.append("2> ")
				.append("'")
				.append(stderrFile.getAbsolutePath())
				.append("'");
		commandArray[2] = strbuilder.toString();

		return commandArray;
	}

	private String[] generateGeneric(File executable, List<String> arguments) {
		int c = 0;
		String[] commandArray = new String[1 + arguments.size()];
		commandArray[c++] = executable.getAbsolutePath();
		for (String argument : arguments)
			commandArray[c++] = argument;
		return commandArray;
	}

}
