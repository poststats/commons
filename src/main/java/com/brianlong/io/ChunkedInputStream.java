package com.brianlong.io;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * This class provides an easier way for a developer to move data from any input
 * stream into any output stream, byte array, or plain text string.
 *
 * @author Brian M Long
 */
public class ChunkedInputStream extends InputStream {

	private BufferedInputStream bistream;

	public ChunkedInputStream(InputStream istream) {
		if (istream instanceof BufferedInputStream) this.bistream = (BufferedInputStream) istream;
		else this.bistream = new BufferedInputStream(istream);
	}

	/**
	 * Memory usage = original stream size + chunk size
	 *
	 * @param chunkSize The number of bytes to read with each pass on the stream.
	 *                  The higher the chunk size, the faster the read and the
	 *                  higher the memory usage.
	 * @return A full array of bytes
	 */
	public byte[] readFully(int chunkSize) throws IOException {
		ByteArrayOutputStream baostream = new ByteArrayOutputStream(this.bistream.available());
		this.readFully(chunkSize, baostream);
		return baostream.toByteArray();
	}

	/**
	 * Memory usage = original stream size + chunk size + charset expansion
	 *
	 * @param chunkSize   The number of bytes to read with each pass on the stream.
	 *                    The higher the chunk size, the faster the read and the
	 *                    higher the memory usage.
	 * @param charsetName A charset (e.g. UTF-8, UTF-16, or ASCII)
	 * @return A string
	 */
	public String readFully(int chunkSize, String charsetName) throws IOException {
		byte[] bytes = this.readFully(chunkSize);
		return new String(bytes, charsetName);
	}

	/**
	 * Memory usage = chunk size (if output stream is not in memory)
	 *
	 * @param chunkSize The number of bytes to read with each pass on the stream.
	 *                  The higher the chunk size, the faster the read and the
	 *                  higher the memory usage.
	 * @param ostream   An arbitrary output stream
	 * @return The total number of bytes read starting at 0
	 */
	public long readFully(int chunkSize, OutputStream ostream) throws IOException {
		long totalBytesRead = 0;

		int bytesRead = -1;
		byte[] bytes = new byte[chunkSize];
		while ((bytesRead = this.bistream.read(bytes, 0, chunkSize)) != -1) {
			ostream.write(bytes, 0, bytesRead);
			totalBytesRead += bytesRead;
		}
		this.bistream.close();

		return totalBytesRead;
	}

	@Override
	public int read() throws IOException {
		return this.bistream.read();
	}

	@Override
	public int available() throws IOException {
		return this.bistream.available();
	}

	@Override
	public void close() throws IOException {
		// suppress
	}

	@Override
	public synchronized void mark(int readlimit) {
		this.bistream.mark(readlimit);
	}

	@Override
	public boolean markSupported() {
		return this.bistream.markSupported();
	}

	@Override
	public int read(byte[] b) throws IOException {
		return this.bistream.read(b);
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		return this.bistream.read(b, off, len);
	}

	@Override
	public synchronized void reset() throws IOException {
		this.bistream.reset();
	}

	@Override
	public long skip(long n) throws IOException {
		return this.bistream.skip(n);
	}

}
