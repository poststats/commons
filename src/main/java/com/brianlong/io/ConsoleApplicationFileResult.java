package com.brianlong.io;

import com.brianlong.util.StringUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;

public class ConsoleApplicationFileResult implements ConsoleApplicationResult {

	private final File stdoutFile;
	private final File stderrFile;
	private int rc;

	ConsoleApplicationFileResult(File stdoutFile, File stderrFile) {
		this.stdoutFile = stdoutFile;
		this.stderrFile = stderrFile;
	}

	@Override
	protected void finalize() throws Throwable {
		try {
			this.stdoutFile.delete();
			this.stderrFile.delete();
		} finally {
			super.finalize();
		}
	}

	public int getReturnCode() {
		return this.rc;
	}

	@SuppressWarnings("resource")
	public String getStandardOutput() throws IOException {
		FileInputStream fistream = new FileInputStream(this.stdoutFile);
		try {
			String str = new ChunkedInputStream(fistream).readFully(1024, Charset.defaultCharset()
					.name());
			return StringUtil.getInstance()
					.trim(str);
		} finally {
			fistream.close();
		}
	}

	@SuppressWarnings("resource")
	public String getStandardErrorOutput() throws IOException {
		FileInputStream fistream = new FileInputStream(this.stderrFile);
		try {
			String str = new ChunkedInputStream(fistream).readFully(1024, Charset.defaultCharset()
					.name());
			return StringUtil.getInstance()
					.trim(str);
		} finally {
			fistream.close();
		}
	}

	void setReturnCode(int rc) {
		this.rc = rc;
	}

}
