package com.brianlong.io;

import java.io.IOException;

public interface ConsoleApplicationResult {

	int getReturnCode() throws IOException;

	String getStandardOutput() throws IOException;

	String getStandardErrorOutput() throws IOException;

}
