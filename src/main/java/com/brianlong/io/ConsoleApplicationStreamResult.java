package com.brianlong.io;

import com.brianlong.util.StringUtil;

public class ConsoleApplicationStreamResult implements ConsoleApplicationResult {

	private final StringOutputStream stdoutStream;
	private final StringOutputStream stderrStream;
	private int rc = -1;

	ConsoleApplicationStreamResult(StringOutputStream stdoutStream, StringOutputStream stderrStream) {
		this.stdoutStream = stdoutStream;
		this.stderrStream = stderrStream;
	}

	public int getReturnCode() {
		return this.rc;
	}

	public String getStandardOutput() {
		String str = this.stdoutStream.toString();
		return StringUtil.getInstance()
				.trim(str);
	}

	public String getStandardErrorOutput() {
		String str = this.stderrStream.toString();
		return StringUtil.getInstance()
				.trim(str);
	}

	void setReturnCode(int rc) {
		this.rc = rc;
	}

}
