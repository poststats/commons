package com.brianlong.time;

import java.time.Duration;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;

public class Range<T extends Temporal> {

	public static Range<? extends Temporal> of(Temporal from, Period period) {
		return new Range<>(from, period.addTo(from));
	}

	public static Range<? extends Temporal> of(Temporal from, Duration duration) {
		return new Range<>(from, duration.addTo(from));
	}

	public static Range<? extends Temporal> of(Temporal from, Temporal to) {
		return new Range<>(from, to);
	}

	private final T from;
	private final T to;

	public Range(T from, T to) {
		this.from = from;
		this.to = to;
	}

	public T getFrom() {
		return this.from;
	}

	public T getTo() {
		return this.to;
	}

	public Period getPeriod() {
		return Period.ofDays((int) this.from.until(this.to, ChronoUnit.DAYS));
	}

	public Duration getDuration() {
		return Duration.ofMillis(this.from.until(this.to, ChronoUnit.MILLIS));
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Range)) return false;

		Range<?> range = (Range<?>) obj;
		return this.from.equals(range.from) && this.toString()
				.equals(range.to);
	}

	@Override
	public String toString() {
		return "[" + this.from.toString() + " through " + this.to.toString() + "]";
	}

}
