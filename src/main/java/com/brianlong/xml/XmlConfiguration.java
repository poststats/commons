package com.brianlong.xml;

import com.brianlong.io.Configuration;
import com.brianlong.thread.ReadWriteSemaphore;
import com.brianlong.util.DateTimeFormatter;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.text.ParseException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.Validator;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Brian M Long
 */
public class XmlConfiguration extends Configuration {

	private final String resource;
	private final URL url;
	private NamespaceContext namespaceContext = InternalNamespaceContext.getInstance();
	private Element rootElement;
	private final List<String> validationSchemas = new LinkedList<String>();
	private final ReadWriteSemaphore rwsemaphore = new ReadWriteSemaphore();

	private static final Logger LOGGER = LoggerFactory.getLogger(XmlConfiguration.class);

	public XmlConfiguration(String resource, String... validationSchemas) {
		this.resource = resource;
		this.url = null;
		for (String validationSchema : validationSchemas)
			this.validationSchemas.add(validationSchema);
	}

	public XmlConfiguration(URI uri, String... validationSchemas) throws MalformedURLException {
		this.resource = null;
		this.url = uri.toURL();
		for (String validationSchema : validationSchemas)
			this.validationSchemas.add(validationSchema);
	}

	public void setNamespaceContext(NamespaceContext namespaceContext) {
		this.namespaceContext = namespaceContext;
	}

	@Override
	public String getLocation() {
		if (this.resource != null) return this.resource;
		else return this.url.toString();
	}

	@Override
	public void startReloader(int minutes) {
		if (this.resource != null) {
			LOGGER.warn("The configuration reloader will not work with a classpath reference (i.e. resource); ignoring start");
		} else {
			super.startReloader(minutes);
		}
	}

	@Override
	public Long getReloadInterval() {
		return this.getLong("@reloadInterval");
	}

	@Override
	public final void load() throws IOException, SAXException {
		InputStream istream = null;

		if (this.resource != null) {
			LOGGER.info("Loading configuration file: " + this.resource);
			istream = this.getClass()
					.getResourceAsStream(this.resource);
			if (istream == null) throw new IOException("The resource, '" + this.resource + "', could not be found");
		} else if (this.url != null) {
			LOGGER.info("Loading configuration file: " + this.url);
			istream = this.url.openStream();
		} else {
			throw new IllegalArgumentException();
		}

		try {
			DocumentBuilderFactory docbuilderFactory = DocumentBuilderFactory.newInstance();
			docbuilderFactory.setIgnoringComments(true);
			docbuilderFactory.setNamespaceAware(true);

			DocumentBuilder docbuilder = docbuilderFactory.newDocumentBuilder();
			Document newDocument = docbuilder.parse(new BufferedInputStream(istream));

			newDocument = this.validate(newDocument);

			this.rwsemaphore.acquireWrite();
			try {
				Document document = newDocument;
				this.rootElement = document.getDocumentElement();
			} finally {
				this.rwsemaphore.releaseWrite();
			}

			LOGGER.info("Loaded configuration file");
		} catch (ParserConfigurationException pce) {
			throw new XmlConfigurationException(pce.getMessage(), pce);
		} finally {
			istream.close();
		}
	}

	public final void addValidationSchema(String resource) {
		this.validationSchemas.add(resource);
	}

	public final Document validate(Document document) throws IOException, SAXException {
		for (String resource : this.validationSchemas) {
			LOGGER.info("Validating configuration file against '" + resource + "'");

			InputStream schemaStream = this.getClass()
					.getResourceAsStream(resource);
			Schema schema = XmlFactory.newSchema(new StreamSource(schemaStream));
			Validator validator = schema.newValidator();
			validator.validate(new DOMSource(document));
		}

		return document;
	}

	public <T> T parseJAXB(Class<T> jaxbRootType) throws JAXBException {
		JAXBContext jaxbcontext = JAXBContext.newInstance(jaxbRootType);
		Unmarshaller jaxbunmarshaller = jaxbcontext.createUnmarshaller();
		return jaxbunmarshaller.unmarshal(new DOMSource(this.rootElement.getOwnerDocument()), jaxbRootType)
				.getValue();
	}

	@Override
	public String getString(String xpathExpression) {
		XPath xpath = this.namespaceContext == null ? XmlFactory.newXPath() : XmlFactory.newXPath(this.namespaceContext);

		this.rwsemaphore.acquireRead();
		try {
			Node node = (Node) xpath.evaluate(xpathExpression, this.rootElement, XPathConstants.NODE);
			return this.getTextContent(node);
		} catch (XPathExpressionException xee) {
			throw new XmlConfigurationException(xee.getMessage(), xee);
		} finally {
			this.rwsemaphore.releaseRead();
		}
	}

	public Element getElement(String xpathExpression) {
		XPath xpath = this.namespaceContext == null ? XmlFactory.newXPath() : XmlFactory.newXPath(this.namespaceContext);

		this.rwsemaphore.acquireRead();
		try {
			return (Element) xpath.evaluate(xpathExpression, this.rootElement, XPathConstants.NODE);
		} catch (XPathExpressionException xee) {
			throw new XmlConfigurationException(xee.getMessage(), xee);
		} finally {
			this.rwsemaphore.releaseRead();
		}
	}

	public Element getElement(String xpathExpression, Element element) {
		XPath xpath = this.namespaceContext == null ? XmlFactory.newXPath() : XmlFactory.newXPath(this.namespaceContext);

		this.rwsemaphore.acquireRead();
		try {
			return (Element) xpath.evaluate(xpathExpression, element, XPathConstants.NODE);
		} catch (XPathExpressionException xee) {
			throw new XmlConfigurationException(xee.getMessage(), xee);
		} finally {
			this.rwsemaphore.releaseRead();
		}
	}

	public NodeList getNodeList(String xpathExpression) {
		XPath xpath = this.namespaceContext == null ? XmlFactory.newXPath() : XmlFactory.newXPath(this.namespaceContext);

		this.rwsemaphore.acquireRead();
		try {
			return (NodeList) xpath.evaluate(xpathExpression, this.rootElement, XPathConstants.NODESET);
		} catch (XPathExpressionException xee) {
			throw new XmlConfigurationException(xee.getMessage(), xee);
		} finally {
			this.rwsemaphore.releaseRead();
		}
	}

	public List<String> getStrings(String xpathExpression) {
		try {
			Constructor<String> constructor = String.class.getConstructor(new Class[] { String.class });
			return this.getObjects(xpathExpression, constructor, String.class);
		} catch (RuntimeException re) {
			throw re;
		} catch (Exception e) {
			throw new XmlConfigurationException(e.getMessage(), e);
		}
	}

	public List<Long> getLongs(String xpathExpression) {
		try {
			Constructor<Long> constructor = Long.class.getConstructor(new Class[] { String.class });
			return this.getObjects(xpathExpression, constructor, Long.class);
		} catch (RuntimeException re) {
			throw re;
		} catch (Exception e) {
			throw new XmlConfigurationException(e.getMessage(), e);
		}
	}

	public List<Double> getDoubles(String xpathExpression) {
		try {
			Constructor<Double> constructor = Double.class.getConstructor(new Class[] { String.class });
			return this.getObjects(xpathExpression, constructor, Double.class);
		} catch (RuntimeException re) {
			throw re;
		} catch (Exception e) {
			throw new XmlConfigurationException(e.getMessage(), e);
		}
	}

	public List<Date> getDates(String xpathExpression, String simpleDateFormat) throws ParseException {
		DateTimeFormatter sdf = new DateTimeFormatter(simpleDateFormat);

		try {
			Method method = sdf.getClass()
					.getMethod("parse", new Class[] { String.class });
			return this.getObjects(xpathExpression, method, Date.class);
		} catch (RuntimeException re) {
			throw re;
		} catch (Exception e) {
			throw new XmlConfigurationException(e.getMessage(), e);
		}
	}

	@SuppressWarnings(value = "unchecked")
	private <T> List<T> getObjects(String xpathExpression, AccessibleObject aobj, Class<T> clazz)
			throws InstantiationException, IllegalAccessException, InvocationTargetException {
		XPath xpath = this.namespaceContext == null ? XmlFactory.newXPath() : XmlFactory.newXPath(this.namespaceContext);
		NodeList nodes = null;

		this.rwsemaphore.acquireRead();
		try {
			nodes = (NodeList) xpath.evaluate(xpathExpression, this.rootElement, XPathConstants.NODESET);
		} catch (XPathExpressionException xee) {
			throw new XmlConfigurationException(xee.getMessage(), xee);
		} finally {
			this.rwsemaphore.releaseRead();
		}

		if (nodes == null) return null;

		List<T> strs = new LinkedList<T>();
		for (int i = 0; i < nodes.getLength(); i++) {
			String str = this.getTextContent(nodes.item(i));
			if (str == null) {
				strs.add(null);
			} else if (aobj instanceof Constructor) {
				Object obj = ((Constructor<?>) aobj).newInstance(new Object[] { str });
				strs.add((T) obj);
			} else if (aobj instanceof Method) {
				Object obj = ((Method) aobj).invoke(new Object[] { str });
				strs.add((T) obj);
			} else {
				throw new IllegalArgumentException();
			}
		}
		return strs;
	}

	private String getTextContent(Node node) {
		if (node == null) return null;

		switch (node.getNodeType()) {
		case Node.ATTRIBUTE_NODE:
			return node.getNodeValue();
		case Node.ELEMENT_NODE:
			return node.hasChildNodes() ? node.getTextContent() : null;
		default:
			return node.getTextContent();
		}
	}

}
