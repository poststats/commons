package com.brianlong.xml;

import javax.xml.XMLConstants;

public class InternalNamespaceContext extends NamespaceContext {

	private static final InternalNamespaceContext INSTANCE = new InternalNamespaceContext();

	public static InternalNamespaceContext getInstance() {
		return InternalNamespaceContext.INSTANCE;
	}

	protected InternalNamespaceContext() {
		this.addNamespace("cfg", "http://brianlong.com/schema");
		this.addNamespace("xsi", XMLConstants.W3C_XML_SCHEMA_NS_URI);
	}

}
