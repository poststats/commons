package com.brianlong.xml;

import java.io.StringWriter;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * @author Brian M Long
 */
public class FlexElement {

	private Element element;

	public FlexElement(Element element) {
		this.element = element;
	}

	public Element getElement() {
		return this.element;
	}

	public FlexElement getFirstChild() {
		Node node = this.element.getFirstChild();
		while (node != null && node.getNodeType() != Node.ELEMENT_NODE)
			node = node.getNextSibling();
		return node == null ? null : new FlexElement((Element) node);
	}

	public FlexElement getNextSibling() {
		Node node = this.element.getNextSibling();
		while (node != null && node.getNodeType() != Node.ELEMENT_NODE)
			node = node.getNextSibling();
		return node == null ? null : new FlexElement((Element) node);
	}

	public String transformToString() throws TransformerException {
		StringWriter swriter = new StringWriter();

		Transformer transformer = XmlFactory.newTransformer();
		transformer.transform(new DOMSource(this.element), new StreamResult(swriter));

		return swriter.toString();
	}

}
