package com.brianlong.xml;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;
import org.xml.sax.SAXException;

/**
 * @author Brian M Long
 */
public class XmlFactory {

	private static final DocumentBuilderFactory DOCUMENT_BUILDER_FACTORY = DocumentBuilderFactory.newInstance();
	private static final SchemaFactory SCHEMA_FACTORY = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
	private static final SAXParserFactory SAX_PARSER_FACTORY = SAXParserFactory.newInstance();
	private static final TransformerFactory TRANSFORMER_FACTORY = TransformerFactory.newInstance();
	private static final XPathFactory XPATH_FACTORY = XPathFactory.newInstance();

	public static DocumentBuilder newDocumentBuilder() throws ParserConfigurationException {
		return DOCUMENT_BUILDER_FACTORY.newDocumentBuilder();
	}

	public static Schema newSchema() throws SAXException {
		return SCHEMA_FACTORY.newSchema();
	}

	public static Schema newSchema(Source source) throws SAXException {
		return SCHEMA_FACTORY.newSchema(source);
	}

	public static SAXParser newSAXParser() throws ParserConfigurationException, SAXException {
		return SAX_PARSER_FACTORY.newSAXParser();
	}

	public static Transformer newTransformer() throws TransformerConfigurationException {
		return TRANSFORMER_FACTORY.newTransformer();
	}

	public static XPath newXPath() {
		return XPATH_FACTORY.newXPath();
	}

	public static XPath newXPath(javax.xml.namespace.NamespaceContext namespaceContext) {
		XPath xpath = XmlFactory.newXPath();
		xpath.setNamespaceContext(namespaceContext);
		return xpath;
	}

}
