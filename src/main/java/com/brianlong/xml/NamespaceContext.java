package com.brianlong.xml;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.xml.XMLConstants;

public abstract class NamespaceContext implements javax.xml.namespace.NamespaceContext {

	Map<String, String> ns2prefix = new HashMap<String, String>();
	Map<String, String> prefix2ns = new HashMap<String, String>();

	protected void setDefaultNamespace(String namespaceURI) {
		this.addNamespace(XMLConstants.DEFAULT_NS_PREFIX, namespaceURI);
	}

	protected void addNamespace(String prefix, String namespaceURI) {
		this.ns2prefix.put(namespaceURI, prefix);
		this.prefix2ns.put(prefix, namespaceURI);
	}

	public String getNamespaceURI(String prefix) {
		if (this.prefix2ns.containsKey(prefix)) return this.prefix2ns.get(prefix);
		return XMLConstants.NULL_NS_URI;
	}

	public String getPrefix(String namespaceURI) {
		return this.ns2prefix.get(namespaceURI);
	}

	public Iterator<String> getPrefixes(String namespaceURI) {
		return this.prefix2ns.keySet()
				.iterator();
	}

}
