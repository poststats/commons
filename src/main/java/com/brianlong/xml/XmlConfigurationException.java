package com.brianlong.xml;

import com.brianlong.io.ConfigurationException;

/**
 *
 * @author Brian M Long
 */
public class XmlConfigurationException extends ConfigurationException {

	private static final long serialVersionUID = 1L;

	public XmlConfigurationException(String msg) {
		super(msg);
	}

	public XmlConfigurationException(Throwable t) {
		super(t);
	}

	public XmlConfigurationException(String msg, Throwable t) {
		super(msg, t);
	}

}
