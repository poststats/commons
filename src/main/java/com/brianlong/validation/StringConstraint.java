package com.brianlong.validation;

/**
 * @author Brian M Long
 */
public class StringConstraint implements Constraint {

	private Number minimumLength = null;
	private Number maximumLength = null;
	private boolean allowSpaces = true;
	private String regex;

	public StringConstraint(Number minimumLength, Number maximumLength) {
		this(minimumLength, maximumLength, true);
	}

	public StringConstraint(boolean allowSpaces) {
		this(null, null, allowSpaces);
	}

	public StringConstraint(Number minimumLength, Number maximumLength, boolean allowSpaces) {
		this.minimumLength = minimumLength;
		this.maximumLength = maximumLength;
		this.allowSpaces = allowSpaces;
	}

	public StringConstraint(String regex) {
		this.regex = regex;
	}

	public Number getMaximumLength() {
		return this.maximumLength;
	}

	public void validate(Object value) throws ConstraintException {
		if (value == null) return;

		String strvalue = value.toString();
		if (strvalue.length() == 0) return;

		if (this.minimumLength != null && strvalue.length() < this.minimumLength.intValue())
			throw new ConstraintException("must be at least " + this.minimumLength + " characters");
		if (this.maximumLength != null && strvalue.length() > this.maximumLength.intValue())
			throw new ConstraintException("must be at most " + this.maximumLength + " characters");
		if (!this.allowSpaces && strvalue.indexOf(' ') >= 0) throw new ConstraintException("must not have spaces");
		if (this.regex != null && !strvalue.matches(this.regex)) throw new ConstraintException("must match a specific pattern");
	}

}
