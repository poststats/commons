package com.brianlong.validation;

import com.brianlong.cache.CacheException;
import com.brianlong.gis.GeocodingException;
import com.brianlong.gis.GoogleMapsGeocoder;
import com.brianlong.gis.GoogleMapsLocation;
import com.brianlong.util.FlexMap;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Brian M Long
 */
public class GoogleMapsLocationConstraint implements LocationConstraint<GoogleMapsLocation> {

	private static final Map<String, Quality> codeMap = new HashMap<String, GoogleMapsLocationConstraint.Quality>(50);

	// most precise (lat/long) to least precise (universe)
	public enum Quality {

		Geocode("geocode"), Room("room"), Floor("floor"), SubPremise("subpremise"), Premise("premise"),
		Place("point_of_interest", "natural_feature", "transit_station"), StreetAddress("street_address"), Intersection("intersection"), Route("route"),
		SubLocalityLevel1("sublocality_level_1"), SubLocalityLevel2("sublocality_level_2"), SubLocalityLevel3("sublocality_level_3"),
		SubLocalityLevel4("sublocality_level_4"), SubLocalityLevel5("sublocality_level_5"), SubLocality("sublocality"),
		AdministrativeAreaLevel5("administrative_area_level_5"), Neighborhood("neighborhood"), Locality("locality"), PostalTown("postal_town"),
		AdministrativeAreaLevel4("administrative_area_level_4"), PostBox("post_box"), PostalCode("postal_code"),
		AdministrativeAreaLevel3("administrative_area_level_3"), ColloquialArea("colloquial_area"), AdministrativeAreaLevel2("administrative_area_level_2"),
		AdministrativeAreaLevel1("administrative_area_level_1"), Political("political"), Country("country");

		private final Set<String> codes;

		Quality(String... codes) {
			this.codes = new HashSet<String>(codes.length);
			for (String code : codes) {
				this.codes.add(code);
				codeMap.put(code, this);
			}
		}

		public static Quality getQuality(String code) {
			Quality quality = codeMap.get(code);
			return quality == null ? Quality.Place : quality;
		}

	}

	private final String googleApiKey;
	private final Quality minimumQuality;

	public GoogleMapsLocationConstraint(String googleApiKey, Quality minimumQuality) {
		this.googleApiKey = googleApiKey;
		this.minimumQuality = minimumQuality;
	}

	public Quality getMinimumQuality() {
		return this.minimumQuality;
	}

	public void validate(Object value) throws ConstraintException {
		this._validate((String) value);
	}

	public GoogleMapsLocation validate(FlexMap data, String locationField) throws ValidationException {
		String location = data.getString(locationField);
		try {
			return this._validate(location);
		} catch (ConstraintException ce) {
			ValidationException ve = new ValidationException();
			ve.addConstraintException(locationField, ce);
			throw ve;
		}
	}

	public GoogleMapsLocation validate(FlexMap data, String cityField, String stateField, String countryField) throws ValidationException {
		return this.validate(data, null, cityField, stateField, countryField);
	}

	public GoogleMapsLocation validate(FlexMap data, String linesField, String cityField, String stateField, String countryField) throws ValidationException {
		String addrlines = data.getString(linesField);
		if (addrlines == null || addrlines.length() == 0) addrlines = "";
		else addrlines += ", ";

		String addrcity = data.getString(cityField);
		if (addrcity == null || addrcity.length() == 0) {
			ValidationException ve = new ValidationException();
			ve.addConstraintException(cityField, new ConstraintException("is required"));
			throw ve;
		}

		String addrstate = data.getString(stateField);
		if (addrstate == null || addrstate.length() == 0) addrstate = "";
		else addrstate += ", ";

		String addrcountry = data.getString(countryField);
		if (addrcountry == null || addrcountry.length() == 0) {
			ValidationException ve = new ValidationException();
			ve.addConstraintException(countryField, new ConstraintException("is required"));
			throw ve;
		}

		try {
			return this._validate(addrlines + addrcity + ", " + addrstate + addrcountry);
		} catch (ConstraintException ce) {
			ValidationException ve = new ValidationException();
			if (linesField != null) ve.addConstraintException(linesField, ce);
			if (cityField != null) ve.addConstraintException(cityField, ce);
			if (stateField != null) ve.addConstraintException(stateField, ce);
			if (countryField != null) ve.addConstraintException(countryField, ce);
			throw ve;
		}
	}

	private GoogleMapsLocation _validate(String locationQuery) throws ConstraintException {
		if (locationQuery == null || locationQuery.length() == 0) throw new ConstraintException("is required");

		GoogleMapsGeocoder geocoder = new GoogleMapsGeocoder(this.googleApiKey);
		try {
			GoogleMapsLocation location = geocoder.find(locationQuery);
			if (location == null) throw new ConstraintException("could not be found");

			String type = location.getType();
			Quality quality = Quality.getQuality(type);
			if (quality.compareTo(this.minimumQuality) > 0) {
				throw new ConstraintException("is not narrow or specific enough; try a more exact address");
			}

			return location;
		} catch (GeocodingException ge) {
			throw new ConstraintException("does not reference a valid location");
		} catch (IOException ie) {
			throw new ConstraintException("could not be validated at this time");
		} catch (CacheException ce) {
			throw new ConstraintException("could not be validated at this time");
		}
	}

}
