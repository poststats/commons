package com.brianlong.validation;

import java.io.File;
import java.util.Set;

/**
 * @author Brian M Long
 */
public class FileConstraint implements Constraint {

	private Number minimumSize;
	private Number maximumSize;
	private Set<String> mimeTypes;

	public FileConstraint() {
		this(null, null, null);
	}

	public FileConstraint(Number minimumSize, Number maximumSize) {
		this(minimumSize, maximumSize, null);
	}

	public FileConstraint(Set<String> acceptableMimeTypes) {
		this(null, null, acceptableMimeTypes);
	}

	public FileConstraint(Number maximumSize, Set<String> acceptableMimeTypes) {
		this(null, maximumSize, acceptableMimeTypes);
	}

	public FileConstraint(Number minimumSize, Number maximumSize, Set<String> acceptableMimeTypes) {
		this.minimumSize = minimumSize;
		this.maximumSize = maximumSize;
		this.mimeTypes = acceptableMimeTypes;
	}

	public void validate(Object value) throws ConstraintException {
		if (value == null) return;

		String mimeType = null;
		File file = null;
		if (value instanceof File) {
			file = (File) value;
		} else if (value instanceof String) {
			file = new File((String) value);
		}

		if (file == null) throw new ConstraintException("must be a file");
		if (!file.exists()) throw new ConstraintException("must be a file that exists");
		if (this.minimumSize != null && this.minimumSize.longValue() > file.length())
			throw new ConstraintException("must be at least " + this.minimumSize + " bytes");
		if (this.maximumSize != null && this.maximumSize.longValue() < file.length())
			throw new ConstraintException("must be at most " + this.maximumSize + " bytes");
		if (this.mimeTypes != null && this.mimeTypes.contains(mimeType)) throw new ConstraintException("must be an accepted MIME type");
	}

}
