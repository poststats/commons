package com.brianlong.validation;

import com.brianlong.gis.GeocodingException;
import com.brianlong.gis.MapQuestGeocoder;
import com.brianlong.gis.MapQuestLocation;
import com.brianlong.util.FlexMap;
import java.io.IOException;

/**
 * @author Brian M Long
 */
public class MapQuestLocationConstraint implements LocationConstraint<MapQuestLocation> {

	public enum Quality {
		Property, Vicinity, Town, Region
	}

	private String mqkey;
	private Quality minimumQuality;

	public MapQuestLocationConstraint(String mqkey, Quality minimumQuality) {
		this.mqkey = mqkey;
		this.minimumQuality = minimumQuality;
	}

	public Quality getMinimumQuality() {
		return this.minimumQuality;
	}

	public void validate(Object value) throws ConstraintException {
		this._validate((String) value);
	}

	public MapQuestLocation validate(FlexMap data, String locationField) throws ValidationException {
		String location = data.getString(locationField);
		try {
			return this._validate(location);
		} catch (ConstraintException ce) {
			ValidationException ve = new ValidationException();
			ve.addConstraintException(locationField, ce);
			throw ve;
		}
	}

	public MapQuestLocation validate(FlexMap data, String cityField, String stateField, String countryField) throws ValidationException {
		return this.validate(data, null, cityField, stateField, countryField);
	}

	public MapQuestLocation validate(FlexMap data, String linesField, String cityField, String stateField, String countryField) throws ValidationException {
		String addrlines = data.getString(linesField);
		if (addrlines == null || addrlines.length() == 0) addrlines = "";
		else addrlines += ", ";

		String addrcity = data.getString(cityField);
		if (addrcity == null || addrcity.length() == 0) {
			ValidationException ve = new ValidationException();
			ve.addConstraintException(cityField, new ConstraintException("is required"));
			throw ve;
		}

		String addrstate = data.getString(stateField);
		if (addrstate == null || addrstate.length() == 0) addrstate = "";
		else addrstate += ", ";

		String addrcountry = data.getString(countryField);
		if (addrcountry == null || addrcountry.length() == 0) {
			ValidationException ve = new ValidationException();
			ve.addConstraintException(countryField, new ConstraintException("is required"));
			throw ve;
		}

		try {
			return this._validate(addrlines + addrcity + ", " + addrstate + addrcountry);
		} catch (ConstraintException ce) {
			ValidationException ve = new ValidationException();
			if (linesField != null) ve.addConstraintException(linesField, ce);
			if (cityField != null) ve.addConstraintException(cityField, ce);
			if (stateField != null) ve.addConstraintException(stateField, ce);
			if (countryField != null) ve.addConstraintException(countryField, ce);
			throw ve;
		}
	}

	private MapQuestLocation _validate(String locationQuery) throws ConstraintException {
		if (locationQuery == null || locationQuery.length() == 0) throw new ConstraintException("is required");

		MapQuestGeocoder geocoder = new MapQuestGeocoder(this.mqkey);
		try {
			MapQuestLocation location = geocoder.find(locationQuery);
			if (location == null) throw new ConstraintException("could not be found");

			String quality = location.getQuality();
			char type = quality.charAt(0);
			int typenum = (int) quality.charAt(1) - (int) '0';
			char conf1 = quality.charAt(2);
			char conf2 = quality.charAt(3);
			char conf3 = quality.charAt(4);

			if ((type == 'P' || type == 'L' || type == 'I' || type == 'Z' && typenum >= 3) && conf1 <= 'B') {
			} else if (this.minimumQuality.compareTo(Quality.Vicinity) >= 0
					&& ((type == 'P' || type == 'L' || type == 'I' || type == 'B') && conf1 <= 'C' || (type == 'Z' && typenum >= 2 && conf3 <= 'B'))) {
					} else
				if (this.minimumQuality.compareTo(Quality.Town) >= 0
						&& ((type == 'A' && typenum >= 5) && (conf1 <= 'B' || conf2 <= 'B') || (type == 'Z' && conf3 <= 'B'))) {
						} else
					if (this.minimumQuality.compareTo(Quality.Region) >= 0 && (type == 'A' && typenum >= 3) && (conf1 <= 'B' || conf2 <= 'B')) {
					} else {
						throw new ConstraintException("is not narrow or specific enough; try a more exact address");
					}

			return location;
		} catch (GeocodingException ge) {
			throw new ConstraintException("does not reference a valid location");
		} catch (IOException ie) {
			throw new ConstraintException("could not be validated at this time");
		}
	}

}
