package com.brianlong.validation;

/**
 *
 * @author Brian M Long
 */
public class ConstraintException extends Exception {

	private static final long serialVersionUID = 1L;

	private Integer arrayIndex;
	private String[] examples;

	public ConstraintException(String predicate) {
		this(predicate, null);
	}

	public ConstraintException(String predicate, int arrayIndex) {
		super(predicate);
		this.arrayIndex = new Integer(arrayIndex);
	}

	public ConstraintException(String predicate, String[] examples) {
		super(predicate);
		this.examples = examples;
	}

	public ConstraintException(String predicate, int arrayIndex, String[] examples) {
		super(predicate);
		this.arrayIndex = new Integer(arrayIndex);
		this.examples = examples;
	}

	public String getMessage(String subject) {
		return subject + ' ' + this.getMessage();
	}

	public Integer getArrayIndex() {
		return this.arrayIndex;
	}

	public String[] getExamples() {
		return this.examples;
	}

}
