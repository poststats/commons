package com.brianlong.validation;

import com.brianlong.util.FlexMap;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * This class implements a field validator. The validator is plugin driven. All
 * validation is done with com.brianlong.validation.Constraint instances. A
 * required field com.brianlong.validation.Constraint has been built in because
 * of its heavy use.
 *
 * The validator also supports sub-validators. A sub-validator is executed when
 * a certain condition is met. For instance, if a combo-box has a certain value,
 * another set of fields will be validated by the defined sub-validator. A
 * sub-validator is exactly the same as a plain old validator.
 *
 * @author Brian M Long
 */
public class Validator {

	private Map<String, List<Constraint>> fieldConstraints = new LinkedHashMap<String, List<Constraint>>();
	private Map<String, Map<Object, Validator>> fieldMatchValidators = new LinkedHashMap<String, Map<Object, Validator>>();
	private Map<String, Map<Object, Validator>> fieldNoMatchValidators = new LinkedHashMap<String, Map<Object, Validator>>();

	private static final RequiredConstraint REQUIRED_CONSTRAINT = new RequiredConstraint();

	/**
	 * This method defines a required field.
	 *
	 * @param field A field name
	 */
	public void addRequiredField(String field) {
		this.addConstrainedField(field, REQUIRED_CONSTRAINT);
	}

	/**
	 * This method defines a set of required fields.
	 *
	 * @param fields An array of field names
	 */
	public void addRequiredFields(String... fields) {
		for (int i = 0; i < fields.length; i++)
			this.addRequiredField(fields[i]);
	}

	/**
	 * This method defines a constrained field.
	 *
	 * @param field      A field name
	 * @param constraint A com.brianlong.validation.Constraint instance
	 */
	public void addConstrainedField(String field, Constraint constraint) {
		if (!this.fieldConstraints.containsKey(field)) this.fieldConstraints.put(field, new LinkedList<Constraint>());
		List<Constraint> constraints = this.fieldConstraints.get(field);
		constraints.add(constraint);
	}

	/**
	 * This method defines a set of constrained fields.
	 *
	 * @param fields     An array of field names
	 * @param constraint A com.brianlong.validation.Constraint instance
	 */
	public void addConstrainedFields(String[] fields, Constraint constraint) {
		for (int i = 0; i < fields.length; i++)
			this.addConstrainedField(fields[i], constraint);
	}

	/**
	 * This method defines a sub-validator executed when the specified 'equals'
	 * condition is met.
	 *
	 * @param field     A field name
	 * @param value     A value that makes the condition true
	 * @param validator A validator to execute when the condition is true
	 */
	public void addEqualsValidator(String field, Object value, Validator validator) {
		if (!this.fieldMatchValidators.containsKey(field)) this.fieldMatchValidators.put(field, new HashMap<Object, Validator>());
		Map<Object, Validator> validators = this.fieldMatchValidators.get(field);
		validators.put(value, validator);
	}

	/**
	 * This method defines a sub-validator executed when the specified 'equals'
	 * condition is not met.
	 *
	 * @param field     A field name
	 * @param value     A value that makes the condition true
	 * @param validator A validator to execute when the condition is false
	 */
	public void addNotEqualsValidator(String field, Object value, Validator validator) {
		if (!this.fieldNoMatchValidators.containsKey(field)) this.fieldNoMatchValidators.put(field, new HashMap<Object, Validator>());
		Map<Object, Validator> validators = this.fieldNoMatchValidators.get(field);
		validators.put(value, validator);
	}

	public Collection<Constraint> getConstraints(String field) {
		List<Constraint> constraints = this.fieldConstraints.get(field);
		return constraints == null ? null : Collections.unmodifiableList(constraints);
	}

	/**
	 * This method executes this validator. The validator will go through all the
	 * field constraints and apply them constraint-by-constraint and field-by-field.
	 * The sub-validators will be executed last.
	 *
	 * Most invalid fields will be reported in the
	 * com.brianlong.validation.ValidationException. If there are any invalid
	 * fields, the com.brianlong.validation.ValidationException will contain at
	 * least one com.brianlong.validation.ConstraintException.
	 *
	 * @param data A flex map
	 * @throws com.brianlong.validation.util.ValidationException At least one field
	 *                                                           is invalid
	 */
	public void validate(FlexMap data) throws ValidationException {
		ValidationException ve = null;

		for (Iterator<String> fields = this.fieldConstraints.keySet()
				.iterator(); fields.hasNext();) {
			String field = fields.next();
			Object value = data.get(field);

			List<Constraint> constraints = this.fieldConstraints.get(field);
			for (Iterator<Constraint> i = constraints.iterator(); i.hasNext();) {
				Constraint constraint = i.next();
				try {
					constraint.validate(value);
				} catch (ConstraintException ce) {
					if (ve == null) ve = new ValidationException();
					ve.addConstraintException(field, ce);
				}
			}
		}

		for (Iterator<String> fields = this.fieldMatchValidators.keySet()
				.iterator(); fields.hasNext();) {
			String field = fields.next();
			Object value = data.get(field);

			Map<Object, Validator> validators = this.fieldMatchValidators.get(field);
			Validator validator = validators.get(value);
			if (validator != null) {
				try {
					validator.validate(data);
				} catch (ValidationException subve) {
					if (ve == null) ve = subve;
					else ve.addValidationException(subve);
				}
			}
		}

		for (Iterator<String> fields = this.fieldNoMatchValidators.keySet()
				.iterator(); fields.hasNext();) {
			String field = fields.next();
			Object value = data.get(field);

			Map<Object, Validator> validators = this.fieldNoMatchValidators.get(field);
			for (Object key : validators.keySet()) {
				Validator validator = null;
				if (key == null) {
					if (value != null) validator = validators.get(key);
				} else if (!key.equals(value)) {
					validator = validators.get(key);
				}

				if (validator != null) {
					try {
						validator.validate(data);
					} catch (ValidationException subve) {
						if (ve == null) ve = subve;
						else ve.addValidationException(subve);
					}
				}
			}
		}

		if (ve != null) throw ve;
	}

	public static class RequiredConstraint implements Constraint {

		public void validate(Object value) throws ConstraintException {
			if (value == null) {
				throw new ConstraintException("is required");
			} else if (value instanceof String) {
				String strvalue = (String) value;
				if (strvalue.length() == 0) throw new ConstraintException("is required");
			} else if (value instanceof Object[]) {
				Object[] objvalues = (Object[]) value;
				for (int i = 0; i < objvalues.length; i++) {
					this.validate(objvalues[i]);
				}
			}
		}

	}

}
