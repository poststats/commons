package com.brianlong.validation;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * @author Brian M Long
 */
public class NumericConstraint implements Constraint {

	private Number minimum;
	private Number maximum;
	private boolean allowDecimal;

	public NumericConstraint() {
		this(null, null);
	}

	public NumericConstraint(Number minimum, Number maximum) {
		this(minimum, maximum, false);
	}

	public NumericConstraint(boolean allowDecimal) {
		this(null, null, allowDecimal);
	}

	public NumericConstraint(Number minimum, Number maximum, boolean allowDecimal) {
		this.minimum = minimum;
		this.maximum = maximum;
		this.allowDecimal = allowDecimal;
	}

	public void validate(Object value) throws ConstraintException {
		if (value == null) return;

		Number number = null;
		if (value instanceof Number) {
			number = (Number) value;
		} else {
			String strvalue = value.toString();
			if (strvalue.length() == 0) return;

			try {
				if (this.allowDecimal) {
					number = new BigDecimal(strvalue);
				} else {
					number = new BigInteger(strvalue);
				}
			} catch (NumberFormatException nfe) {
				throw new ConstraintException("must be numeric");
			}
		}

		if (this.allowDecimal) {
			if (this.minimum != null && number.doubleValue() < this.minimum.doubleValue()) throw new ConstraintException("is too small");
			if (this.maximum != null && number.doubleValue() > this.maximum.doubleValue()) throw new ConstraintException("is too large");
		} else {
			if (this.minimum != null && number.longValue() < this.minimum.longValue()) throw new ConstraintException("is too small");
			if (this.maximum != null && number.longValue() > this.maximum.longValue()) throw new ConstraintException("is too large");
		}
	}

}
