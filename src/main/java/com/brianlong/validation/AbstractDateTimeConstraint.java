package com.brianlong.validation;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalAccessor;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Brian M Long
 */
public class AbstractDateTimeConstraint {

	private final boolean hasDate;
	private final boolean hasTime;
	private final String message;
	private boolean disallowFuture = false;
	private boolean disallowPast = false;
	private TemporalAccessor earliest = null;
	private TemporalAccessor latest = null;

	public AbstractDateTimeConstraint(boolean hasDate, boolean hasTime) {
		if (!hasDate && !hasTime) throw new IllegalArgumentException();
		this.hasDate = hasDate;
		this.hasTime = hasTime;

		StringBuilder builder = new StringBuilder("must be a valid ");
		if (this.hasDate) builder.append("date");
		if (this.hasDate && this.hasTime) builder.append("/");
		if (this.hasTime) builder.append("time");
		this.message = builder.toString();
	}

	public AbstractDateTimeConstraint(boolean hasDate, boolean hasTime, boolean disallowFuture, boolean disallowPast) {
		this(hasDate, hasTime);
		this.disallowFuture = disallowFuture;
		this.disallowPast = disallowPast;
	}

	public AbstractDateTimeConstraint(boolean hasDate, boolean hasTime, TemporalAccessor earliest, TemporalAccessor latest) {
		this(hasDate, hasTime);

		if (earliest != null) {
			if (this.hasDate && this.hasTime) {
				if (!(earliest instanceof LocalDateTime)) throw new IllegalArgumentException();
			} else if (this.hasDate) {
				if (!(earliest instanceof LocalDate)) throw new IllegalArgumentException();
			} else if (this.hasTime && !(earliest instanceof LocalTime)) {
				throw new IllegalArgumentException();
			}
		}

		if (latest != null) {
			if (this.hasDate && this.hasTime) {
				if (!(latest instanceof LocalDateTime)) throw new IllegalArgumentException();
			} else if (this.hasDate) {
				if (!(latest instanceof LocalDate)) throw new IllegalArgumentException();
			} else if (this.hasTime && !(latest instanceof LocalTime)) {
				throw new IllegalArgumentException();
			}
		}

		this.earliest = earliest;
		this.latest = latest;
	}

	protected boolean hasDate() {
		return this.hasDate;
	}

	protected boolean hasTime() {
		return this.hasTime;
	}

	protected String getMessage() {
		return this.message;
	}

	protected Temporal toLocal(Object value) {
		if (value == null) {
			if (this.hasDate && this.hasTime) return LocalDateTime.now();
			else if (this.hasDate) return LocalDate.now();
			else return LocalTime.now();
		} else if (value instanceof LocalDateTime) {
			if (this.hasDate && this.hasTime) return (LocalDateTime) value;
			else if (this.hasDate) return ((LocalDateTime) value).toLocalDate();
			else return ((LocalDateTime) value).toLocalTime();
		} else if (value instanceof LocalDate) {
			if (this.hasDate && this.hasTime) return ((LocalDate) value).atStartOfDay();
			else if (this.hasDate) return (LocalDate) value;
			else throw new IllegalArgumentException();
		} else if (value instanceof LocalTime) {
			if (this.hasDate && this.hasTime) throw new IllegalArgumentException();
			else if (this.hasDate) throw new IllegalArgumentException();
			else return (LocalTime) value;
		} else if (value instanceof Date) {
			ZonedDateTime datetime = Instant.ofEpochMilli(((Date) value).getTime())
					.atZone(ZoneId.systemDefault());
			if (this.hasDate && this.hasTime) return datetime.toLocalDateTime();
			else if (this.hasDate) return datetime.toLocalDate();
			else return datetime.toLocalTime();
		} else if (value instanceof Calendar) {
			ZonedDateTime datetime = ((Calendar) value).toInstant()
					.atZone(ZoneId.systemDefault());
			if (this.hasDate && this.hasTime) return datetime.toLocalDateTime();
			else if (this.hasDate) return datetime.toLocalDate();
			else return datetime.toLocalTime();
		} else if (value instanceof Instant) {
			if (this.hasDate && this.hasTime) return ((Instant) value).atZone(ZoneId.systemDefault())
					.toLocalDateTime();
			else if (this.hasDate) return ((Instant) value).atZone(ZoneId.systemDefault())
					.toLocalTime();
			else return ((Instant) value).atZone(ZoneId.systemDefault())
					.toLocalDate();
		} else {
			return null;
		}
	}

	protected void validate(TemporalAccessor now, Comparable<TemporalAccessor> lvalue) throws ConstraintException {
		if (this.disallowFuture && lvalue.compareTo(now) > 0) throw new ConstraintException("must not be in the future");
		if (this.disallowPast && lvalue.compareTo(now) < 0) throw new ConstraintException("must not be in the past");
		if (this.earliest != null && lvalue.compareTo(this.earliest) < 0) throw new ConstraintException("is too early");
		if (this.latest != null && lvalue.compareTo(this.latest) > 0) throw new ConstraintException("is too late");
	}

}
