package com.brianlong.validation;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;

/**
 * @author Brian M Long
 */
public class NumericArrayConstraint implements Constraint {

	private Number minimum;
	private Number maximum;
	private boolean allowDecimal;

	public NumericArrayConstraint() {
		this(null, null);
	}

	public NumericArrayConstraint(Number minimum, Number maximum) {
		this(minimum, maximum, false);
	}

	public NumericArrayConstraint(boolean allowDecimal) {
		this(null, null, allowDecimal);
	}

	public NumericArrayConstraint(Number minimum, Number maximum, boolean allowDecimal) {
		this.minimum = minimum;
		this.maximum = maximum;
		this.allowDecimal = allowDecimal;
	}

	public void validate(Object value) throws ConstraintException {
		if (value == null) return;

		Object[] objs = null;
		if (value instanceof Object[]) {
			objs = (Object[]) value;
		} else if (value instanceof Collection<?>) {
			objs = ((Collection<?>) value).toArray();
		} else {
			objs = new Object[] { value };
		}

		Number[] numbers = new Number[objs.length];
		for (int i = 0; i < numbers.length; i++) {
			if (objs[i] == null) continue;

			if (objs[i] instanceof Number) {
				numbers[i] = (Number) objs[i];
			} else {
				String strvalue = objs[i].toString()
						.trim();
				if (strvalue.length() == 0) continue;

				try {
					if (this.allowDecimal) {
						numbers[i] = new BigDecimal(strvalue);
					} else {
						numbers[i] = new BigInteger(strvalue);
					}
				} catch (NumberFormatException nfe) {
					throw new ConstraintException("must be numeric", i);
				}
			}

			if (this.allowDecimal) {
				if (this.minimum != null && numbers[i].doubleValue() < this.minimum.doubleValue()) throw new ConstraintException("is too small", i);
				if (this.maximum != null && numbers[i].doubleValue() > this.maximum.doubleValue()) throw new ConstraintException("is too large", i);
			} else {
				if (this.minimum != null && numbers[i].longValue() < this.minimum.longValue()) throw new ConstraintException("is too small", i);
				if (this.maximum != null && numbers[i].longValue() > this.maximum.longValue()) throw new ConstraintException("is too large", i);
			}
		}
	}

}
