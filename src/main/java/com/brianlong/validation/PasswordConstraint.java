package com.brianlong.validation;

/**
 * @author Brian M Long
 */
public class PasswordConstraint extends StringConstraint {

	private boolean forceNumbers;
	private boolean forceSpecial;

	public PasswordConstraint(Number minimumLength, Number maximumLength, boolean forceNumbers, boolean forceSpecial) {
		super(minimumLength, maximumLength, false);

		this.forceNumbers = forceNumbers;
		this.forceSpecial = forceSpecial;
	}

	public void validate(Object value) throws ConstraintException {
		super.validate(value);

		if (value == null) return;

		String strvalue = value.toString();
		if (strvalue.length() == 0) return;

		boolean hasNumber = false;
		boolean hasSpecial = false;

		for (int i = 0; i < strvalue.length() && (this.forceNumbers != hasNumber || this.forceSpecial != hasSpecial); i++) {
			char ch = strvalue.charAt(i);
			if (Character.isLetter(ch)) {
			} else if (Character.isDigit(ch)) {
				hasNumber = true;
			} else {
				hasSpecial = true;
			}
		}

		if (this.forceNumbers && !hasNumber) throw new ConstraintException("must have at least one number");
		if (this.forceSpecial && !hasSpecial) throw new ConstraintException("must have at least one special character");
	}

}
