package com.brianlong.validation;

import com.brianlong.gis.GeocodingException;
import com.brianlong.gis.OpenStreetMapGeocoder;
import com.brianlong.gis.OpenStreetMapLocation;
import com.brianlong.gis.OpenStreetMapLocation.Classification;
import com.brianlong.gis.OpenStreetMapLocation.Type;
import com.brianlong.util.FlexMap;
import java.io.IOException;

/**
 * @author Brian M Long
 */
public class OpenStreetMapLocationConstraint implements LocationConstraint<OpenStreetMapLocation> {

	public enum Quality {
		Building, Locality, Region
	}

	private String mqkey;
	private Quality minimumQuality;

	public OpenStreetMapLocationConstraint(String mqkey, Quality minimumQuality) {
		this.mqkey = mqkey;
		this.minimumQuality = minimumQuality;
	}

	public Quality getMinimumQuality() {
		return this.minimumQuality;
	}

	public void validate(Object value) throws ConstraintException {
		this._validate((String) value);
	}

	public OpenStreetMapLocation validate(FlexMap data, String locationField) throws ValidationException {
		String location = data.getString(locationField);
		try {
			return this._validate(location);
		} catch (ConstraintException ce) {
			ValidationException ve = new ValidationException();
			ve.addConstraintException(locationField, ce);
			throw ve;
		}
	}

	public OpenStreetMapLocation validate(FlexMap data, String cityField, String stateField, String countryField) throws ValidationException {
		return this.validate(data, null, cityField, stateField, countryField);
	}

	public OpenStreetMapLocation validate(FlexMap data, String linesField, String cityField, String stateField, String countryField)
			throws ValidationException {
		String addrlines = data.getString(linesField);
		if (addrlines == null || addrlines.length() == 0) addrlines = "";
		else addrlines += ", ";

		String addrcity = data.getString(cityField);
		if (addrcity == null || addrcity.length() == 0) {
			ValidationException ve = new ValidationException();
			ve.addConstraintException(cityField, new ConstraintException("is required"));
			throw ve;
		}

		String addrstate = data.getString(stateField);
		if (addrstate == null || addrstate.length() == 0) addrstate = "";
		else addrstate += ", ";

		String addrcountry = data.getString(countryField);
		if (addrcountry == null || addrcountry.length() == 0) {
			ValidationException ve = new ValidationException();
			ve.addConstraintException(countryField, new ConstraintException("is required"));
			throw ve;
		}

		try {
			return this._validate(addrlines + addrcity + ", " + addrstate + addrcountry);
		} catch (ConstraintException ce) {
			ValidationException ve = new ValidationException();
			if (linesField != null) ve.addConstraintException(linesField, ce);
			if (cityField != null) ve.addConstraintException(cityField, ce);
			if (stateField != null) ve.addConstraintException(stateField, ce);
			if (countryField != null) ve.addConstraintException(countryField, ce);
			throw ve;
		}
	}

	private OpenStreetMapLocation _validate(String locationQuery) throws ConstraintException {
		if (locationQuery == null || locationQuery.length() == 0) throw new ConstraintException("is required");

		OpenStreetMapGeocoder geocoder = new OpenStreetMapGeocoder(this.mqkey);
		try {
			OpenStreetMapLocation location = geocoder.find(locationQuery);
			if (location == null) throw new ConstraintException("could not be found");

			Classification classification = location.getClassification();
			Type type = location.getType();

			if (Classification.Place.equals(classification)) {
			} else if (Classification.Highway.equals(classification) && Type.Residential.equals(type) && !Quality.Building.equals(this.minimumQuality)) {
			} else if (Classification.Boundary.equals(classification)
					&& (Type.Administrative.equals(type) || Type.City.equals(type) || Type.Postcode.equals(type) || Type.Village.equals(type))
					&& !Quality.Building.equals(this.minimumQuality)) {
					} else {
						throw new ConstraintException("is not narrow or specific enough; try a more exact address");
					}

			return location;
		} catch (GeocodingException ge) {
			throw new ConstraintException("does not reference a valid location");
		} catch (IOException ie) {
			throw new ConstraintException("could not be validated at this time");
		}
	}

}
