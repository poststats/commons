package com.brianlong.validation;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Brian M Long
 */
public class ValidationException extends Exception {

	private static final long serialVersionUID = 1L;

	private List<ConstraintException> exceptions = new LinkedList<ConstraintException>();
	private Map<String, List<ConstraintException>> fields = new LinkedHashMap<String, List<ConstraintException>>();

	public ValidationException() {
		super("validation failed");
	}

	public void addValidationException(ValidationException ve) {
		this.exceptions.addAll(ve.exceptions);

		for (Iterator<String> i = ve.iterateFields(); i.hasNext();) {
			String field = i.next();
			List<ConstraintException> fieldExceptions = ve.getConstraintExceptions(field);

			if (!this.fields.containsKey(field)) this.fields.put(field, new LinkedList<ConstraintException>());
			this.fields.get(field)
					.addAll(fieldExceptions);
		}
	}

	public void addConstraintException(ConstraintException ce) {
		this.exceptions.add(ce);
	}

	public void addConstraintException(String field, ConstraintException ce) {
		if (!this.fields.containsKey(field)) this.fields.put(field, new LinkedList<ConstraintException>());
		this.fields.get(field)
				.add(ce);
	}

	public Iterator<String> iterateFields() {
		return this.fields.keySet()
				.iterator();
	}

	public List<ConstraintException> getConstraintExceptions() {
		return this.exceptions;
	}

	public List<ConstraintException> getConstraintExceptions(String field) {
		return this.fields.get(field);
	}

	public String getConsolidatedMessage() {
		StringBuilder message = new StringBuilder(this.getMessage());

		List<ConstraintException> ces = this.getConstraintExceptions();
		for (ConstraintException ce : ces)
			message.append("; ")
					.append(ce.getMessage());

		Iterator<String> fields = this.iterateFields();
		while (fields.hasNext()) {
			String field = fields.next();
			ces = this.getConstraintExceptions(field);
			for (ConstraintException ce : ces)
				message.append("; ")
						.append(ce.getMessage(field));
		}

		return message.toString();
	}

	public Map<String, List<ConstraintException>> toMap() {
		return Collections.unmodifiableMap(this.fields);
	}

}
