package com.brianlong.validation;

import com.brianlong.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Brian M Long
 */
public class DatabaseBooleanConstraint implements Constraint {

	private DataSource ds;
	private String sql;
	private String predicate;

	public DatabaseBooleanConstraint(DataSource ds, String parameterizedSQL, String falseMessage) {
		this.ds = ds;
		this.sql = parameterizedSQL;
		this.predicate = falseMessage;
	}

	public void validate(Object value) throws ConstraintException {
		if (value == null) return;

		Connection dbcon = this.ds.acquire(true);
		try {
			PreparedStatement ps = dbcon.prepareStatement(this.sql);
			try {
				ps.setObject(1, value);
				ResultSet rs = ps.executeQuery();
				if (!rs.next()) {
					throw new ConstraintException(this.predicate);
				} else if (rs.getMetaData()
						.getColumnCount() == 1 && rs.getObject(1) instanceof Number && ((Number) rs.getObject(1)).intValue() == 0) {
							throw new ConstraintException(this.predicate);
						} else
					if (rs.getMetaData()
							.getColumnCount() == 1 && rs.getObject(1) instanceof Boolean && !((Boolean) rs.getObject(1)).booleanValue()) {
								throw new ConstraintException(this.predicate);
							}
			} finally {
				ps.close();
			}
		} catch (SQLException se) {
			throw new ConstraintException(this.predicate);
		} finally {
			this.ds.release(dbcon);
		}
	}

}
