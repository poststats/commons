package com.brianlong.validation;

import com.brianlong.util.Number;

/**
 * @author Brian M Long
 */
public class ZipCodeConstraint implements Constraint {

	private boolean allow9;
	private boolean require9;

	public ZipCodeConstraint() {
		this(false);
	}

	public ZipCodeConstraint(boolean allow9) {
		this(allow9, false);
	}

	public ZipCodeConstraint(boolean allow9, boolean require9) {
		if (require9 && !allow9) throw new IllegalArgumentException("If you require 9 zip code digits, you must allow 9");

		this.allow9 = allow9;
		this.require9 = require9;
	}

	public boolean doesAllow9Digits() {
		return this.allow9;
	}

	public void validate(Object value) throws ConstraintException {
		if (value == null) return;

		String strvalue = value.toString();
		if (strvalue.length() == 0) return;

		String number = Number.extract(strvalue);
		int numberCount = number.length();

		if (this.require9) {
			if (numberCount != 9) throw new ConstraintException("must have exactly 9 digits", new String[] { "12345-6789" });
		} else if (this.allow9) {
			if (numberCount != 5 && numberCount != 9) throw new ConstraintException("must have exactly 5 or 9 digits", new String[] { "12345", "12345-6789" });
		} else if (numberCount != 5) {
			throw new ConstraintException("must have exactly 5 digits", new String[] { "12345" });
		}
	}

}
