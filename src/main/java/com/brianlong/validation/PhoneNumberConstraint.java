package com.brianlong.validation;

import com.brianlong.util.Number;

/**
 * @author Brian M Long
 */
public class PhoneNumberConstraint implements Constraint {

	private boolean requireAreaCode;

	public PhoneNumberConstraint() {
		this(true);
	}

	public PhoneNumberConstraint(boolean requireAreaCode) {
		this.requireAreaCode = requireAreaCode;
	}

	public void validate(Object value) throws ConstraintException {
		if (value == null) return;

		String strvalue = value.toString();
		if (strvalue.length() == 0) return;

		String number = Number.extract(strvalue);
		int numberCount = number.length();

		if (this.requireAreaCode && numberCount < 10) {
			throw new ConstraintException("must have an area code");
		} else if (numberCount != 10 && numberCount != 7) {
			throw new ConstraintException("must be 7 or 10 numbers", new String[] { "555.5555", "(555) 555-5555" });
		}
	}

}
