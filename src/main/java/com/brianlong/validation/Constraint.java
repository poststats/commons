package com.brianlong.validation;

/**
 * This interface provides a contract for plugin validation capability.
 *
 * If there is anything that needs validated, including against a database, just
 * implement a constraint and use it in the Validator.
 *
 * @author Brian M Long
 */
public interface Constraint {

	void validate(Object value) throws ConstraintException;

}
