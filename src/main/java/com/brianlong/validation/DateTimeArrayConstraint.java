package com.brianlong.validation;

import com.brianlong.util.TimeAmPm;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.TemporalAccessor;

/**
 * @author Brian M Long
 */
public class DateTimeArrayConstraint extends AbstractDateTimeConstraint implements Constraint {

	private final boolean hasSeconds;

	public DateTimeArrayConstraint(boolean hasDate, boolean hasTime) {
		super(hasDate, hasTime);
		this.hasSeconds = hasTime;
	}

	public DateTimeArrayConstraint(boolean hasDate, boolean hasTime, boolean hasSeconds) {
		super(hasDate, hasTime);
		this.hasSeconds = hasTime && hasSeconds;
	}

	public DateTimeArrayConstraint(boolean hasDate, boolean hasTime, boolean disallowFuture, boolean disallowPast) {
		super(hasDate, hasTime, disallowFuture, disallowPast);
		this.hasSeconds = hasTime;
	}

	public DateTimeArrayConstraint(boolean hasDate, boolean hasTime, boolean hasSeconds, boolean disallowFuture, boolean disallowPast) {
		super(hasDate, hasTime, disallowFuture, disallowPast);
		this.hasSeconds = hasTime && hasSeconds;
	}

	public DateTimeArrayConstraint(boolean hasDate, boolean hasTime, TemporalAccessor earliest, TemporalAccessor latest) {
		super(hasDate, hasTime, earliest, latest);
		this.hasSeconds = hasTime;
	}

	public DateTimeArrayConstraint(boolean hasDate, boolean hasTime, boolean hasSeconds, TemporalAccessor earliest, TemporalAccessor latest) {
		super(hasDate, hasTime, earliest, latest);
		this.hasSeconds = hasTime && hasSeconds;
	}

	public void validate(Object value) throws ConstraintException {
		if (value == null) return;

		TemporalAccessor now = this.toLocal(null);

		TemporalAccessor lvalue = this.toLocal(value);
		if (lvalue == null) {
			Object[] objs = (Object[]) value;
			int elementsPerField = 0;
			if (this.hasDate() && this.hasTime()) elementsPerField = this.hasSeconds ? 7 : 6;
			if (this.hasDate() && !this.hasTime()) elementsPerField = 3;
			if (!this.hasDate() && this.hasTime()) elementsPerField = this.hasSeconds ? 4 : 3;
			if (objs.length % elementsPerField != 0) throw new ConstraintException(this.getMessage());

			for (int f = 0; f < objs.length / elementsPerField; f++) {
				Number[] nums = new Number[elementsPerField];
				if (this.validate(objs, f * elementsPerField, nums, this.hasTime())) return;

				int baseIndex = 0;

				try {
					if (this.hasDate()) {
						lvalue = LocalDate.of(nums[baseIndex + 2].intValue(), nums[baseIndex].intValue(), nums[baseIndex + 1].intValue());
						baseIndex += 3;
					}
					if (this.hasTime()) {
						int hour = nums[baseIndex].intValue() % 12;
						if (TimeAmPm.PM.equals(TimeAmPm.valueOf(objs[objs.length - 1].toString()))) hour += 12;
						int minute = nums[baseIndex + 1].intValue();
						int second = this.hasSeconds ? nums[nums.length - 2].intValue() : 0;
						LocalTime ltime = LocalTime.of(hour, minute, second);

						lvalue = lvalue == null ? ltime : ((LocalDate) lvalue).atTime(ltime);
					}
				} catch (DateTimeException dte) {
					throw new ConstraintException("is not valid");
				}

				this.validate(now, (Comparable<TemporalAccessor>) lvalue);
			}
		}
	}

	private boolean validate(Object[] objs, int baseElement, Number[] nums, boolean lastValueString) throws ConstraintException {
		boolean isComplete = true;
		boolean isEmpty = true;

		for (int index = 0; index < nums.length; index++) {
			if (objs[index + baseElement] == null) {
				isComplete = false;
			} else if (objs[index + baseElement] instanceof Number) {
				nums[index] = (Number) objs[index + baseElement];
				isEmpty = false;
			} else {
				String str = objs[index + baseElement].toString();
				if (str == null || str.length() == 0) {
					isComplete = false;
				} else if (lastValueString && index + 1 == nums.length) {
					// do nothing
				} else {
					try {
						nums[index] = new Integer(str);
						isEmpty = false;
					} catch (NumberFormatException nfe) {
						isComplete = false;
					}
				}
			}
		}

		if (isEmpty) return true;
		else if (isComplete) return false;
		else return true;
	}

}
