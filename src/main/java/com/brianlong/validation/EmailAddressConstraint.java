package com.brianlong.validation;

/**
 * @author Brian M Long
 */
public class EmailAddressConstraint extends StringConstraint {

	private static final String REGEX_VALIDATION = "^[A-Za-z0-9](([_\\.\\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\\.\\-]?[a-zA-Z0-9]+)*)\\.([A-Za-z]{2,})$";

	public EmailAddressConstraint() {
		super(REGEX_VALIDATION);
	}

	@Override
	public void validate(Object value) throws ConstraintException {
		try {
			super.validate(value);
		} catch (ConstraintException ce) {
			if (ce.getMessage()
					.endsWith("pattern"))
				throw new ConstraintException("must be a valid format", new String[] { "username@domain.com" });
			throw ce;
		}
	}

}
