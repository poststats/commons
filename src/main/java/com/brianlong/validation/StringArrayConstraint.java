package com.brianlong.validation;

/**
 * @author Brian M Long
 */
public class StringArrayConstraint implements Constraint {

	private Number minimumLength = null;
	private Number maximumLength = null;
	private boolean allowSpaces = true;
	private String regex;

	public StringArrayConstraint(Number minimumLength, Number maximumLength) {
		this(minimumLength, maximumLength, true);
	}

	public StringArrayConstraint(boolean allowSpaces) {
		this(null, null, allowSpaces);
	}

	public StringArrayConstraint(Number minimumLength, Number maximumLength, boolean allowSpaces) {
		this.minimumLength = minimumLength;
		this.maximumLength = maximumLength;
		this.allowSpaces = allowSpaces;
	}

	public StringArrayConstraint(String regex) {
		this.regex = regex;
	}

	public void validate(Object value) throws ConstraintException {
		if (value == null) return;

		Object[] objs = null;
		if (value instanceof Object[]) {
			objs = (Object[]) value;
		} else {
			objs = new Object[] { value };
		}

		for (int i = 0; i < objs.length; i++) {
			if (objs[i] == null) continue;

			String strvalue = objs[i].toString();
			if (strvalue.length() == 0) continue;

			if (this.minimumLength != null && strvalue.length() < this.minimumLength.intValue())
				throw new ConstraintException("must be at least " + this.minimumLength + " characters", i);
			if (this.maximumLength != null && strvalue.length() > this.maximumLength.intValue())
				throw new ConstraintException("must be at most " + this.maximumLength + " characters", i);
			if (!this.allowSpaces && strvalue.indexOf(' ') >= 0) throw new ConstraintException("must not have spaces", i);
			if (this.regex != null && !strvalue.matches(this.regex)) throw new ConstraintException("must match a specific pattern", i);
		}
	}

}
