package com.brianlong.validation;

import com.brianlong.gis.GeocodingException;
import com.brianlong.gis.YahooGeocoder;
import com.brianlong.gis.YahooLocation;
import com.brianlong.util.FlexMap;
import java.io.IOException;

/**
 * @author Brian M Long
 */
public class YahooLocationConstraint implements LocationConstraint<YahooLocation> {

	public enum Quality {
		Exact((byte) 80), Vicinity((byte) 70), Region((byte) 35), Country((byte) 5);

		private byte value;

		Quality(byte value) {
			this.value = value;
		}

		public byte getValue() {
			return this.value;
		}

	}

	private Quality minimumQuality;

	public YahooLocationConstraint(Quality minimumQuality) {
		this.minimumQuality = minimumQuality;
	}

	public Quality getMinimumQuality() {
		return this.minimumQuality;
	}

	public void validate(Object value) throws ConstraintException {
		this._validate((String) value);
	}

	public YahooLocation validate(FlexMap data, String locationField) throws ValidationException {
		String location = data.getString(locationField);
		try {
			return this._validate(location);
		} catch (ConstraintException ce) {
			ValidationException ve = new ValidationException();
			ve.addConstraintException(locationField, ce);
			throw ve;
		}
	}

	public YahooLocation validate(FlexMap data, String cityField, String stateField, String countryField) throws ValidationException {
		return this.validate(data, null, cityField, stateField, countryField);
	}

	public YahooLocation validate(FlexMap data, String linesField, String cityField, String stateField, String countryField) throws ValidationException {
		String addrlines = data.getString(linesField);
		if (addrlines == null || addrlines.length() == 0) addrlines = "";
		else addrlines += ", ";

		String addrcity = data.getString(cityField);
		if (addrcity == null || addrcity.length() == 0) {
			ValidationException ve = new ValidationException();
			ve.addConstraintException(cityField, new ConstraintException("is required"));
			throw ve;
		}

		String addrstate = data.getString(stateField);
		if (addrstate == null || addrstate.length() == 0) addrstate = "";
		else addrstate += ", ";

		String addrcountry = data.getString(countryField);
		if (addrcountry == null || addrcountry.length() == 0) {
			ValidationException ve = new ValidationException();
			ve.addConstraintException(countryField, new ConstraintException("is required"));
			throw ve;
		}

		try {
			return this._validate(addrlines + addrcity + ", " + addrstate + addrcountry);
		} catch (ConstraintException ce) {
			ValidationException ve = new ValidationException();
			if (linesField != null) ve.addConstraintException(linesField, ce);
			if (cityField != null) ve.addConstraintException(cityField, ce);
			if (stateField != null) ve.addConstraintException(stateField, ce);
			if (countryField != null) ve.addConstraintException(countryField, ce);
			throw ve;
		}
	}

	private YahooLocation _validate(String locationQuery) throws ConstraintException {
		if (locationQuery == null || locationQuery.length() == 0) throw new ConstraintException("is required");

		YahooGeocoder geocoder = new YahooGeocoder();
		try {
			YahooLocation location = geocoder.find(locationQuery);
			if (location == null) throw new ConstraintException("does not reference a valid location");
			if (location.getQuality()
					.byteValue() < this.minimumQuality.getValue())
				throw new ConstraintException("is not valid or specific enough");
			return location;
		} catch (GeocodingException ge) {
			throw new ConstraintException("does not reference a valid location");
		} catch (IOException ie) {
			throw new ConstraintException("could not be validated at this time");
		}
	}

}
