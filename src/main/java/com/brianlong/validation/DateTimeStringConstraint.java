package com.brianlong.validation;

import com.brianlong.util.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;

/**
 * @author Brian M Long
 */
public class DateTimeStringConstraint extends AbstractDateTimeConstraint implements Constraint {

	private final String format;

	public DateTimeStringConstraint(String format) {
		super(format.indexOf("M") >= 0 || format.indexOf("d") >= 0 || format.indexOf("y") >= 0,
				format.indexOf("m") >= 0 || format.indexOf("h") >= 0 || format.indexOf("H") >= 0);
		this.format = format;
	}

	public DateTimeStringConstraint(String format, boolean disallowFuture, boolean disallowPast) {
		super(format.indexOf("M") >= 0 || format.indexOf("d") >= 0 || format.indexOf("y") >= 0,
				format.indexOf("m") >= 0 || format.indexOf("h") >= 0 || format.indexOf("H") >= 0, disallowFuture, disallowPast);
		this.format = format;
	}

	public DateTimeStringConstraint(String format, TemporalAccessor earliest, TemporalAccessor latest) {
		super(format.indexOf("M") >= 0 || format.indexOf("d") >= 0 || format.indexOf("y") >= 0,
				format.indexOf("m") >= 0 || format.indexOf("h") >= 0 || format.indexOf("H") >= 0, earliest, latest);
		this.format = format;
	}

	public void validate(Object value) throws ConstraintException {
		if (value == null) return;

		TemporalAccessor lvalue = this.toLocal(value);
		if (lvalue == null) {
			String strvalue = value.toString();
			if (strvalue.length() == 0) return;

			try {
				TemporalAccessor datetime = new DateTimeFormatter(this.format).parse(strvalue);
				lvalue = this.toLocal(datetime);
			} catch (IllegalArgumentException iae) {
				throw new ConstraintException(this.getMessage());
			}
		}

		TemporalAccessor now = this.toLocal(null);
		this.validate(now, (Comparable<TemporalAccessor>) lvalue);
	}

}
