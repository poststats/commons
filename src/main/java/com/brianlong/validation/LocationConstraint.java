package com.brianlong.validation;

import com.brianlong.gis.Location;
import com.brianlong.util.FlexMap;

public interface LocationConstraint<T extends Location> {

	T validate(FlexMap data, String locationField) throws ValidationException;

	T validate(FlexMap data, String cityField, String stateField, String countryField) throws ValidationException;

	T validate(FlexMap data, String linesField, String cityField, String stateField, String countryField) throws ValidationException;

}
