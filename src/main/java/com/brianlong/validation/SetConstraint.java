package com.brianlong.validation;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Brian M Long
 */
public class SetConstraint<T> implements Constraint {

	private Set<T> set;

	public SetConstraint(T[] objs) {
		for (int i = 0; i < objs.length; i++)
			this.set.add(objs[i]);
	}

	public SetConstraint(Collection<T> objs) {
		if (objs instanceof Set<?>) this.set = (Set<T>) objs;
		else this.set = new HashSet<T>(objs);
	}

	public SetConstraint(Map<T, ?> objs) {
		this.set = objs.keySet();
	}

	public void validate(Object value) throws ConstraintException {
		if (value == null) return;

		if (!this.set.contains(value)) throw new ConstraintException("is not valid");
	}

}
