package com.brianlong.validation;

public class ArrayConstraint implements Constraint {

	private int atLeast = 1;

	public ArrayConstraint(int atLeast) {
		if (atLeast < 1) throw new IllegalArgumentException();
		this.atLeast = atLeast;
	}

	public void validate(Object value) throws ConstraintException {
		if (value == null) {
			throw new ConstraintException("is required");
		} else if (value instanceof String) {
			String strvalue = (String) value;
			if (strvalue.length() == 0) throw new ConstraintException("is required");
		} else if (value instanceof Object[]) {
			Object[] objvalues = (Object[]) value;
			for (int i = 0; i < objvalues.length; i++)
				if (objvalues[i] != null && objvalues[i] instanceof String && ((String) objvalues[i]).length() > 0) return;

			throw new ConstraintException("requires at least " + this.atLeast + " selection(s)");
		}
	}

}
