package com.brianlong.sql;

import com.brianlong.io.Configuration;
import com.brianlong.thread.ReadWriteSemaphore;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Brian M Long
 */
public class DatabaseConfiguration extends Configuration {

	private String dbUrl;
	private String dbUsername;
	private String dbPassword;
	private String sql;
	private Map<String, String> cache;
	private ReadWriteSemaphore rwsemaphore = new ReadWriteSemaphore();

	private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseConfiguration.class);

	public DatabaseConfiguration(String url, String username, String password, String tableOrView, String keyColumn, String valueColumn) {
		this.dbUrl = url;
		this.dbUsername = username;
		this.dbPassword = password;
		this.sql = "SELECT " + keyColumn + ", " + valueColumn + " FROM " + tableOrView;
	}

	@Override
	public String getLocation() {
		return this.dbUrl;
	}

	@Override
	public Long getReloadInterval() {
		return this.getLong("RELOAD_INTERVAL");
	}

	@Override
	public void load() throws SQLException {
		LOGGER.info("Loading configuration ...");

		Connection dbcon = DriverManager.getConnection(this.dbUrl, this.dbUsername, this.dbPassword);
		try {
			PreparedStatement ps = dbcon.prepareStatement(this.sql);
			try {
				ResultSet rs = ps.executeQuery();

				rwsemaphore.acquireWrite();
				try {
					this.cache = new HashMap<String, String>();
					while (rs.next())
						this.cache.put(rs.getString(1), rs.getString(2));
				} finally {
					rwsemaphore.releaseWrite();
				}
			} finally {
				ps.close();
			}

			LOGGER.info("Loaded configuration");
		} finally {
			dbcon.close();
		}
	}

	@Override
	public String getString(String field) {
		this.rwsemaphore.acquireRead();
		try {
			return this.cache.get(field);
		} finally {
			this.rwsemaphore.releaseRead();
		}
	}

}
