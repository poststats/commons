package com.brianlong.sql;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.WKBWriter;
import com.vividsolutions.jts.io.WKTWriter;
import java.io.ByteArrayInputStream;
import java.io.StringReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Types;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Brian M Long
 */
public class FlexPreparedStatement {

	private final Pattern pattern = Pattern.compile("([A-Za-z0-9_-]+)\\(\\?\\)|\\?");
	private final PreparedStatement ps;
	private final boolean countFoundRows;
	private int[] parameterTypes;

	public FlexPreparedStatement(PreparedStatement ps) {
		this.ps = ps;
		this.countFoundRows = false;
	}

	public FlexPreparedStatement(Connection dbcon, String sql) throws SQLException {
		this.ps = dbcon.prepareStatement(sql);
		this.countFoundRows = false;
		this.determineParameterTypes(sql);
	}

	/**
	 * @param flag For SELECT queries, calculate found rows; for INSERT statements,
	 *             return generated keys
	 */
	public FlexPreparedStatement(Connection dbcon, String sql, boolean flag) throws SQLException {
		boolean isSelect = (sql.length() > 5) ? sql.substring(0, 6)
				.equalsIgnoreCase("SELECT") : false;
		if (isSelect) {
			sql = "SELECT SQL_CALC_FOUND_ROWS" + sql.substring(6);
			this.ps = dbcon.prepareStatement(sql);
			this.countFoundRows = flag;
		} else {
			this.ps = dbcon.prepareStatement(sql, flag ? PreparedStatement.RETURN_GENERATED_KEYS : PreparedStatement.NO_GENERATED_KEYS);
			this.countFoundRows = false;
		}

		this.determineParameterTypes(sql);
	}

	public FlexPreparedStatement(Connection dbcon, String sql, Collection<?> values) throws SQLException {
		this(dbcon, sql, new Collection[] { values });
	}

	public FlexPreparedStatement(Connection dbcon, String sql, Collection<?>[] values) throws SQLException {
		this.ps = dbcon.prepareStatement(FlexStatement.formatCollectionSql(sql, values));
		this.countFoundRows = false;
		this.determineParameterTypes(sql);
	}

	protected void finalize() throws Throwable {
		try {
			this.ps.close();
		} finally {
			super.finalize();
		}
	}

	public PreparedStatement getPreparedStatement() {
		return this.ps;
	}

	public void close() throws SQLException {
		this.ps.close();
	}

	public int executeUpdate() throws SQLException {
		return this.ps.executeUpdate();
	}

	public FlexResultSet executeQuery() throws SQLException {
		return new FlexResultSet(this.ps.executeQuery(), this.countFoundRows);
	}

	public FlexResultSet getGeneratedKeys() throws SQLException {
		return new FlexResultSet(this.ps.getGeneratedKeys());
	}

	public Number executeAndGetId() throws SQLException {
		this.executeUpdate();
		return this.getGeneratedKeys()
				.getNumber();
	}

	public <T extends Number> T executeAndGetId(Class<T> type) throws SQLException {
		this.executeUpdate();
		return this.getGeneratedKeys()
				.getNumber(type);
	}

	public void setBoolean(int col, boolean value) throws SQLException {
		this.ps.setBoolean(col, value);
	}

	public void setBoolean(int col, Boolean value) throws SQLException {
		if (value == null) this.ps.setNull(col, Types.BOOLEAN);
		else this.ps.setBoolean(col, value.booleanValue());
	}

	public void setBit(int col, byte value) throws SQLException {
		this.ps.setByte(col, value);
	}

	public void setBit(int col, Number value) throws SQLException {
		if (value == null) this.ps.setNull(col, Types.BIT);
		else this.ps.setByte(col, value.byteValue());
	}

	public void setTinyint(int col, byte value) throws SQLException {
		this.ps.setByte(col, value);
	}

	public void setTinyint(int col, Number value) throws SQLException {
		if (value == null) this.ps.setNull(col, Types.TINYINT);
		else this.ps.setByte(col, value.byteValue());
	}

	public void setTinyintU(int col, short value) throws SQLException {
		this.ps.setShort(col, value);
	}

	public void setTinyintU(int col, Number value) throws SQLException {
		if (value == null) this.ps.setNull(col, Types.TINYINT);
		else this.ps.setShort(col, value.shortValue());
	}

	public void setSmallint(int col, short value) throws SQLException {
		this.ps.setShort(col, value);
	}

	public void setSmallint(int col, Number value) throws SQLException {
		if (value == null) this.ps.setNull(col, Types.SMALLINT);
		else this.ps.setShort(col, value.shortValue());
	}

	public void setSmallintU(int col, int value) throws SQLException {
		this.ps.setInt(col, value);
	}

	public void setSmallintU(int col, Number value) throws SQLException {
		if (value == null) this.ps.setNull(col, Types.SMALLINT);
		else this.ps.setInt(col, value.intValue());
	}

	public void setInteger(int col, int value) throws SQLException {
		this.ps.setInt(col, value);
	}

	public void setInteger(int col, Number value) throws SQLException {
		if (value == null) this.ps.setNull(col, Types.INTEGER);
		else this.ps.setInt(col, value.intValue());
	}

	public void setIntegerU(int col, long value) throws SQLException {
		this.ps.setLong(col, value);
	}

	public void setIntegerU(int col, Number value) throws SQLException {
		if (value == null) this.ps.setNull(col, Types.INTEGER);
		else this.ps.setLong(col, value.longValue());
	}

	public void setBigint(int col, long value) throws SQLException {
		this.ps.setLong(col, value);
	}

	public void setBigint(int col, Number value) throws SQLException {
		if (value == null) this.ps.setNull(col, Types.BIGINT);
		else this.ps.setLong(col, value.longValue());
	}

	public void setBigintU(int col, BigInteger value) throws SQLException {
		if (value == null) this.ps.setNull(col, Types.BIGINT);
		else this.ps.setObject(col, value);
	}

	public void setFloat(int col, float value) throws SQLException {
		this.ps.setFloat(col, value);
	}

	public void setFloat(int col, Number value) throws SQLException {
		if (value == null) this.ps.setNull(col, Types.FLOAT);
		else this.ps.setFloat(col, value.floatValue());
	}

	public void setFloatU(int col, double value) throws SQLException {
		this.ps.setDouble(col, value);
	}

	public void setFloatU(int col, Number value) throws SQLException {
		if (value == null) this.ps.setNull(col, Types.FLOAT);
		else this.ps.setDouble(col, value.doubleValue());
	}

	public void setDouble(int col, double value) throws SQLException {
		this.ps.setDouble(col, value);
	}

	public void setDouble(int col, Number value) throws SQLException {
		if (value == null) this.ps.setNull(col, Types.DOUBLE);
		else this.ps.setDouble(col, value.doubleValue());
	}

	public void setDoubleU(int col, BigDecimal value) throws SQLException {
		if (value == null) this.ps.setNull(col, Types.DOUBLE);
		else this.ps.setBigDecimal(col, value);
	}

	public void setDecimal(int col, double value) throws SQLException {
		this.ps.setDouble(col, value);
	}

	public void setDecimal(int col, Number value) throws SQLException {
		if (value == null) this.ps.setNull(col, Types.DECIMAL);
		else this.ps.setDouble(col, value.doubleValue());
	}

	public void setDecimal(int col, BigDecimal value) throws SQLException {
		if (value == null) this.ps.setNull(col, Types.DECIMAL);
		else this.ps.setBigDecimal(col, value);
	}

	public void setReal(int col, double value) throws SQLException {
		this.ps.setDouble(col, value);
	}

	public void setReal(int col, Number value) throws SQLException {
		if (value == null) this.ps.setNull(col, Types.REAL);
		else this.ps.setDouble(col, value.doubleValue());
	}

	public void setReal(int col, BigDecimal value) throws SQLException {
		if (value == null) this.ps.setNull(col, Types.REAL);
		else this.ps.setBigDecimal(col, value);
	}

	public void setChar(int col, String value) throws SQLException {
		if (value == null) this.ps.setNull(col, Types.CHAR);
		else this.ps.setString(col, value);
	}

	public void setVarchar(int col, String value) throws SQLException {
		if (value == null) this.ps.setNull(col, Types.VARCHAR);
		else this.ps.setString(col, value);
	}

	public void setLongvarchar(int col, String value) throws SQLException {
		if (value == null) this.ps.setNull(col, Types.LONGVARCHAR);
		else this.ps.setString(col, value);
	}

	public void setClob(int col, String value) throws SQLException {
		if (value == null) this.ps.setNull(col, Types.CLOB);
		else this.ps.setClob(col, new StringReader(value));
	}

	public void setBinary(int col, byte[] value) throws SQLException {
		if (value == null) this.ps.setNull(col, Types.BINARY);
		else this.ps.setBytes(col, value);
	}

	public void setVarbinary(int col, byte[] value) throws SQLException {
		if (value == null) this.ps.setNull(col, Types.VARBINARY);
		else this.ps.setBytes(col, value);
	}

	public void setLongvarbinary(int col, byte[] value) throws SQLException {
		if (value == null) this.ps.setNull(col, Types.LONGVARBINARY);
		else this.ps.setBytes(col, value);
	}

	public void setBlob(int col, byte[] value) throws SQLException {
		if (value == null) this.ps.setNull(col, Types.BLOB);
		else this.ps.setBlob(col, new ByteArrayInputStream(value));
	}

	public void setDate(int col, Date value) throws SQLException {
		if (value == null) this.ps.setNull(col, Types.DATE);
		else this.ps.setDate(col, value);
	}

	public void setDate(int col, LocalDate value) throws SQLException {
		if (value == null) this.ps.setNull(col, Types.DATE);
		else this.ps.setDate(col, Date.valueOf(value));
	}

	public void setTime(int col, LocalTime value) throws SQLException {
		if (value == null) this.ps.setNull(col, Types.TIME);
		else this.ps.setTime(col, Time.valueOf(value));
	}

	public void setTimestamp(int col, Instant value) throws SQLException {
		if (value == null) this.ps.setNull(col, Types.TIMESTAMP);
		else this.ps.setTimestamp(col, Timestamp.from(value));
	}

	public void setTimestamp(int col, LocalDateTime value) throws SQLException {
		if (value == null) this.ps.setNull(col, Types.TIMESTAMP);
		else this.ps.setTimestamp(col, Timestamp.valueOf(value));
	}

	public void setGeometry(int col, Geometry value) throws SQLException {
		if (value == null) {
			if (this.parameterTypes[col] != 0) {
				this.ps.setNull(col, this.parameterTypes[col]);
			} else {
				this.ps.setNull(col, 0);
			}
		} else {
			switch (this.parameterTypes[col]) {
			case Types.BINARY:
			case Types.VARBINARY:
			case Types.LONGVARBINARY:
			case Types.BLOB:
				this.ps.setBytes(col, new WKBWriter().write(value));
				break;
			case Types.NULL:
			default:
				this.ps.setString(col, new WKTWriter().write(value));
			}
		}
	}

	private void determineParameterTypes(String sql) throws SQLException {
		this.parameterTypes = new int[this.ps.getParameterMetaData()
				.getParameterCount() + 1];

		Matcher matcher = this.pattern.matcher(sql);
		for (int c = 1; matcher.find(); c++) {
			if (matcher.group(1) != null) {
				if (matcher.group(1)
						.endsWith("FromText")) {
					this.parameterTypes[c] = Types.VARCHAR;
				} else if (matcher.group(1)
						.endsWith("FromWKB")) {
							this.parameterTypes[c] = Types.VARBINARY;
						}
			}
		}
	}

}
