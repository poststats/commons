package com.brianlong.sql;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Brian M Long
 */
public class DataSetColumnExtractor {

	public static <T> Set<T> extract(Collection<DataSet> rows, String columnName, Class<T> columnClass) {
		Set<T> values = new HashSet<T>();
		for (DataSet row : rows) {
			T value = row.get(columnName, columnClass);
			if (value != null) values.add(value);
		}

		return values;
	}

}
