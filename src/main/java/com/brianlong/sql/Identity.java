package com.brianlong.sql;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

public class Identity {

	public static <T extends Number> T getNumber(Connection dbcon, Class<T> type) throws SQLException {
		CallableStatement cs = dbcon.prepareCall("CALL IDENTITY()");
		try {
			return new FlexResultSet(cs.executeQuery()).getNumber(type);
		} finally {
			cs.close();
		}
	}
}
