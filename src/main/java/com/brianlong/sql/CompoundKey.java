package com.brianlong.sql;

public abstract class CompoundKey<K1, K2> {

	private K1 key1;
	private K2 key2;

	public CompoundKey(K1 key1, K2 key2) {
		this.key1 = key1;
		this.key2 = key2;
	}

	protected K1 getFirstKey() {
		return this.key1;
	}

	protected K2 getSecondKey() {
		return this.key2;
	}

	@Override
	@SuppressWarnings("unchecked")
	public boolean equals(Object obj) {
		if (!(obj instanceof CompoundKey)) return false;

		CompoundKey<K1, K2> key = (CompoundKey<K1, K2>) obj;
		return this.key1.equals(key.key1) && this.key2.equals(key.key2);
	}

	@Override
	public int hashCode() {
		return this.key1.hashCode() + this.key2.hashCode();
	}

}
