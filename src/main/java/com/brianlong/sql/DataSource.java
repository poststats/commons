package com.brianlong.sql;

import com.brianlong.util.ResourcePool;
import java.lang.reflect.Method;
import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Struct;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;

/**
 * @author Brian M Long
 */
public class DataSource extends ResourcePool<Connection> {

	private String url;
	private String username;
	private String password;
	private Method isValidMethod = null;

	public DataSource(String url, String username, String password, int minimum, int maximum, int expireSeconds) {
		super(url, minimum, maximum, 1000L * expireSeconds);

		this.url = url;
		this.username = username;
		this.password = password;

		try {
			this.isValidMethod = Connection.class.getMethod("isValid", Integer.class);
		} catch (NoSuchMethodException nsme) {
			// suppress
		}
	}

	@Override
	protected boolean checkValidity(Connection dbcon) throws SQLException {
		if (this.isValidMethod != null) {
			// JRE 6
			try {
				return ((Boolean) this.isValidMethod.invoke(dbcon, 1)).booleanValue();
			} catch (Exception e) {
				return false;
			}
		} else {
			if (dbcon.isClosed()) return false;

			Statement stmt = dbcon.createStatement();
			stmt.executeQuery("SELECT 1");
			return true;
		}
	}

	@Override
	protected Connection create() throws SQLException {
		try {
			return new DataSourceConnection(this, DriverManager.getConnection(this.url, this.username, this.password));
		} catch (SQLException se) {
			return new DataSourceConnection(this, DriverManager.getConnection(this.url, this.username, this.password));
		}
	}

	@Override
	protected void destroy(Connection dbcon) throws SQLException {
		((DataSourceConnection) dbcon).destroy();
	}

	public Connection acquireTX(boolean wait) {
		Connection dbcon = this.acquire(wait);
		this.acquireTX(dbcon);
		return dbcon;
	}

	public Connection acquireTX(long ms) {
		Connection dbcon = this.acquire(ms);
		this.acquireTX(dbcon);
		return dbcon;
	}

	private void acquireTX(Connection dbcon) {
		if (dbcon == null) return;

		try {
			dbcon.setAutoCommit(false);
		} catch (SQLException se) {
			this.release(dbcon);
			throw new IllegalStateException(se.getMessage(), se);
		}
	}

	public void releaseTX(Connection dbcon) {
		this.release(dbcon);
	}

	@Override
	public void release(Connection dbcon) {
		try {
			if (dbcon != null && !dbcon.getAutoCommit()) {
				dbcon.rollback();
				dbcon.setAutoCommit(true);
			}
		} catch (SQLException se) {
			throw new IllegalStateException(se.getMessage(), se);
		} finally {
			super.release(dbcon);
		}
	}

	private class DataSourceConnection implements Connection {

		private final DataSource dbds;
		private final Connection dbcon;

		public DataSourceConnection(DataSource dbds, Connection dbcon) {
			this.dbds = dbds;
			this.dbcon = dbcon;
		}

		void destroy() throws SQLException {
			this.dbcon.close();
		}

		@Override
		public void abort(Executor executor) throws SQLException {
			this.dbcon.abort(executor);
		}

		@Override
		public void clearWarnings() throws SQLException {
			this.dbcon.clearWarnings();
		}

		@Override
		public void close() throws SQLException {
			this.dbds.release(this);
		}

		@Override
		public void commit() throws SQLException {
			this.dbcon.commit();
		}

		@Override
		public Array createArrayOf(String typeName, Object[] elements) throws SQLException {
			return this.dbcon.createArrayOf(typeName, elements);
		}

		@Override
		public Blob createBlob() throws SQLException {
			return this.dbcon.createBlob();
		}

		@Override
		public Clob createClob() throws SQLException {
			return this.dbcon.createClob();
		}

		@Override
		public NClob createNClob() throws SQLException {
			return this.dbcon.createNClob();
		}

		@Override
		public SQLXML createSQLXML() throws SQLException {
			return this.dbcon.createSQLXML();
		}

		@Override
		public Statement createStatement() throws SQLException {
			return this.dbcon.createStatement();
		}

		@Override
		public Statement createStatement(int resultSetType, int resultSetConcurrency) throws SQLException {
			return this.dbcon.createStatement(resultSetType, resultSetConcurrency);
		}

		@Override
		public Statement createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
			return this.dbcon.createStatement(resultSetType, resultSetConcurrency, resultSetHoldability);
		}

		@Override
		public Struct createStruct(String typeName, Object[] attributes) throws SQLException {
			return this.dbcon.createStruct(typeName, attributes);
		}

		@Override
		public boolean getAutoCommit() throws SQLException {
			return this.dbcon.getAutoCommit();
		}

		@Override
		public String getCatalog() throws SQLException {
			return this.dbcon.getCatalog();
		}

		@Override
		public Properties getClientInfo() throws SQLException {
			return this.dbcon.getClientInfo();
		}

		@Override
		public String getClientInfo(String name) throws SQLException {
			return this.dbcon.getClientInfo(name);
		}

		@Override
		public int getHoldability() throws SQLException {
			return this.dbcon.getHoldability();
		}

		@Override
		public DatabaseMetaData getMetaData() throws SQLException {
			return this.dbcon.getMetaData();
		}

		@Override
		public int getNetworkTimeout() throws SQLException {
			return this.dbcon.getNetworkTimeout();
		}

		@Override
		public String getSchema() throws SQLException {
			return this.dbcon.getSchema();
		}

		@Override
		public int getTransactionIsolation() throws SQLException {
			return this.dbcon.getTransactionIsolation();
		}

		@Override
		public Map<String, Class<?>> getTypeMap() throws SQLException {
			return this.dbcon.getTypeMap();
		}

		@Override
		public SQLWarning getWarnings() throws SQLException {
			return this.dbcon.getWarnings();
		}

		@Override
		public boolean isClosed() throws SQLException {
			return this.dbcon.isClosed();
		}

		@Override
		public boolean isReadOnly() throws SQLException {
			return this.dbcon.isReadOnly();
		}

		@Override
		public boolean isValid(int timeout) throws SQLException {
			return this.dbcon.isValid(timeout);
		}

		@Override
		public boolean isWrapperFor(Class<?> iface) throws SQLException {
			return this.dbcon.isWrapperFor(iface);
		}

		@Override
		public String nativeSQL(String sql) throws SQLException {
			return this.dbcon.nativeSQL(sql);
		}

		@Override
		public CallableStatement prepareCall(String sql) throws SQLException {
			return this.dbcon.prepareCall(sql);
		}

		@Override
		public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
			return this.dbcon.prepareCall(sql, resultSetType, resultSetConcurrency);
		}

		@Override
		public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
			return this.dbcon.prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
		}

		@Override
		public PreparedStatement prepareStatement(String sql) throws SQLException {
			return this.dbcon.prepareStatement(sql);
		}

		@Override
		public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException {
			return this.dbcon.prepareStatement(sql, autoGeneratedKeys);
		}

		@Override
		public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws SQLException {
			return this.dbcon.prepareStatement(sql, columnIndexes);
		}

		@Override
		public PreparedStatement prepareStatement(String sql, String[] columnNames) throws SQLException {
			return this.dbcon.prepareStatement(sql, columnNames);
		}

		@Override
		public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
			return this.dbcon.prepareStatement(sql, resultSetType, resultSetConcurrency);
		}

		@Override
		public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
			return this.dbcon.prepareStatement(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
		}

		@Override
		public void releaseSavepoint(Savepoint savepoint) throws SQLException {
			this.dbcon.releaseSavepoint(savepoint);
		}

		@Override
		public void rollback() throws SQLException {
			this.dbcon.rollback();
		}

		@Override
		public void rollback(Savepoint savepoint) throws SQLException {
			this.dbcon.rollback(savepoint);
		}

		@Override
		public void setAutoCommit(boolean autoCommit) throws SQLException {
			this.dbcon.setAutoCommit(autoCommit);
		}

		@Override
		public void setCatalog(String catalog) throws SQLException {
			this.dbcon.setCatalog(catalog);
		}

		@Override
		public void setClientInfo(Properties properties) throws SQLClientInfoException {
			this.dbcon.setClientInfo(properties);
		}

		@Override
		public void setClientInfo(String name, String value) throws SQLClientInfoException {
			this.dbcon.setClientInfo(name, value);
		}

		@Override
		public void setHoldability(int holdability) throws SQLException {
			this.dbcon.setHoldability(holdability);
		}

		@Override
		public void setNetworkTimeout(Executor executor, int milliseconds) throws SQLException {
			this.dbcon.setNetworkTimeout(executor, milliseconds);
		}

		@Override
		public void setReadOnly(boolean readOnly) throws SQLException {
			this.dbcon.setReadOnly(readOnly);
		}

		@Override
		public Savepoint setSavepoint() throws SQLException {
			return this.dbcon.setSavepoint();
		}

		@Override
		public Savepoint setSavepoint(String name) throws SQLException {
			return this.dbcon.setSavepoint(name);
		}

		@Override
		public void setSchema(String schema) throws SQLException {
			this.dbcon.setSchema(schema);
		}

		@Override
		public void setTransactionIsolation(int level) throws SQLException {
			this.dbcon.setTransactionIsolation(level);
		}

		@Override
		public void setTypeMap(Map<String, Class<?>> map) throws SQLException {
			this.dbcon.setTypeMap(map);
		}

		@Override
		public <T> T unwrap(Class<T> iface) throws SQLException {
			return this.dbcon.unwrap(iface);
		}

	}

}
