package com.brianlong.sql;

import com.brianlong.cache.HazelcastCacheAdapter;
import com.brianlong.cache.Reaper;
import com.brianlong.cache.ReaperFactory;
import com.brianlong.util.FlexMap;
import com.hazelcast.core.HazelcastInstance;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public abstract class ClusterAwareDataSetCacher<ID> extends DataSetCacher<ID> {

	public ClusterAwareDataSetCacher(long expirationMilliseconds, HazelcastInstance hzInstance) {
		this(expirationMilliseconds, ReaperFactory.getDefaultInstance(), hzInstance);
	}

	public ClusterAwareDataSetCacher(long expirationMilliseconds, Reaper reaper, HazelcastInstance hzInstance) {
		super(expirationMilliseconds, reaper);

		if (hzInstance != null) {
			HazelcastCacheAdapter<ID> adapter = new HazelcastCacheAdapter<ID>(hzInstance, this.getClass()
					.getName());
			this.addListener(adapter);
			adapter.addListener(this);
		}
	}

	public void bulkCache(Collection<? extends FlexMap> rows, String columnName) throws SQLException {
		this.bulkCache(rows, columnName, null);
	}

	@SuppressWarnings("unchecked")
	public void bulkCache(Collection<? extends FlexMap> rows, String idColumnName, String copyCacheToColumnName) throws SQLException {
		if (rows == null || rows.isEmpty()) return;

		Set<ID> ids = new HashSet<ID>(rows.size());
		for (FlexMap row : rows) {
			ID value = (ID) row.get(idColumnName);
			if (value != null) ids.add(value);
		}

		if (ids.isEmpty()) return;

		Map<ID, DataSet> cache = this.getDataSets(ids);
		if (copyCacheToColumnName == null) return;

		for (FlexMap row : rows) {
			ID value = (ID) row.get(idColumnName);
			if (value != null) row.put(copyCacheToColumnName, cache.get(value));
		}
	}

	public void bulkCache(Collection<ID> ids) throws SQLException {
		if (ids == null || ids.isEmpty()) return;
		this.getDataSets(ids);
	}

	@Override
	public DataSet getDataSet(ID id) throws SQLException {
		Map<ID, DataSet> cache = this.getDataSets(Arrays.asList(id));
		return cache.isEmpty() ? null : cache.get(id);
	}

	@Override
	public abstract Map<ID, DataSet> getDataSets(Collection<ID> ids) throws SQLException;

}
