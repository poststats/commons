package com.brianlong.sql;

import com.brianlong.util.FlexMap;
import java.lang.reflect.Constructor;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

/**
 *
 * @author Brian M Long
 */
public class FlexResultSet implements Iterable<DataSet> {

	private final ResultSet rs;
	private final Long foundRows;

	public FlexResultSet(ResultSet rs) {
		this.rs = rs;
		this.foundRows = null;
	}

	public FlexResultSet(ResultSet rs, boolean getFoundRows) throws SQLException {
		this.rs = rs;
		if (getFoundRows) {
			FlexPreparedStatement fps = new FlexPreparedStatement(rs.getStatement()
					.getConnection()
					.prepareStatement("SELECT FOUND_ROWS()"));
			try {
				this.foundRows = fps.executeQuery()
						.getOne(Long.class);
			} finally {
				fps.close();
			}
		} else {
			this.foundRows = null;
		}
	}

	public ResultSet getResultSet() {
		return this.rs;
	}

	public Long getFoundRows() {
		return this.foundRows;
	}

	public Object getOne() throws SQLException {
		return this.rs.next() ? this.rs.getObject(1) : null;
	}

	public <T> T getOne(Class<T> type) throws SQLException {
		if (!this.rs.next()) return null;
		return this.getGenericObject(type, this.rs.getObject(1));
	}

	public Number getNumber() throws SQLException {
		if (!this.rs.next()) return null;
		return (Number) this.rs.getObject(1);
	}

	public <T extends Number> T getNumber(Class<T> type) throws SQLException {
		return this.getOne(type);
	}

	public List<?> getFirstColumn() throws SQLException {
		List<Object> objs = new LinkedList<Object>();
		while (this.rs.next())
			objs.add(this.rs.getObject(1));
		return objs;
	}

	public <T> List<T> getFirstColumn(Class<T> type) throws SQLException {
		List<T> objs = new LinkedList<T>();
		this.getFirstColumn(type, objs);
		return objs;
	}

	public <T> void getFirstColumn(Class<T> type, Collection<T> objs) throws SQLException {
		while (this.rs.next())
			objs.add(this.getGenericObject(type, this.rs.getObject(1)));
	}

	public Map<?, ?> getFirstTwoColumns() throws SQLException {
		Map<Object, Object> objs = new LinkedHashMap<Object, Object>();
		while (this.rs.next())
			objs.put(this.rs.getObject(1), this.rs.getObject(2));
		return objs;
	}

	public <K, V> Map<K, V> getFirstTwoColumns(Class<K> keyType, Class<V> valueType) throws SQLException {
		boolean hasValue = this.rs.getMetaData()
				.getColumnCount() > 1;
		Map<K, V> objs = new LinkedHashMap<K, V>();
		while (this.rs.next()) {
			K key = this.getGenericObject(keyType, this.rs.getObject(1));
			V value = hasValue ? this.getGenericObject(valueType, this.rs.getObject(2)) : null;
			objs.put(key, value);
		}
		return objs;
	}

	public <K, V> Map<K, Set<V>> getFirstTwoColumnsOneToMany(Class<K> keyType, Class<V> valueType) throws SQLException {
		Map<K, Set<V>> objs = new LinkedHashMap<K, Set<V>>();
		while (this.rs.next()) {
			K key = this.getGenericObject(keyType, this.rs.getObject(1));
			V value = this.getGenericObject(valueType, this.rs.getObject(2));

			Set<V> values = objs.get(key);
			if (values == null) objs.put(key, values = new HashSet<V>());
			values.add(value);
		}

		return objs;
	}

	public DataSet getNextRow() throws SQLException {
		if (!this.rs.next()) return null;
		return new DataSet(this.rs);
	}

	public List<DataSet> getAllRows() throws SQLException {
		List<DataSet> rows = new LinkedList<DataSet>();

		DataSet row;
		while ((row = this.getNextRow()) != null)
			rows.add(row);
		return rows;
	}

	public Map<Object, DataSet> getAllRows(String uniqueKeyColName) throws SQLException {
		Map<Object, DataSet> rows = new LinkedHashMap<Object, DataSet>();

		DataSet row;
		while ((row = this.getNextRow()) != null)
			rows.put(row.get(uniqueKeyColName), row);

		return rows;
	}

	public <K> Map<K, DataSet> getAllRows(String uniqueKeyColName, Class<K> uniqueKeyColType) throws SQLException {
		Map<K, DataSet> rows = new LinkedHashMap<K, DataSet>();

		DataSet row;
		while ((row = this.getNextRow()) != null) {
			Object key = row.get(uniqueKeyColName);
			rows.put(this.getGenericObject(uniqueKeyColType, key), row);
		}

		return rows;
	}

	public Map<List<Object>, DataSet> getAllRows(List<String> uniqueKeyColNames) throws SQLException {
		Map<List<Object>, DataSet> rows = new LinkedHashMap<List<Object>, DataSet>();

		DataSet row;
		while ((row = this.getNextRow()) != null) {
			List<Object> uniqueKey = new LinkedList<Object>();
			for (String uniqueKeyColName : uniqueKeyColNames)
				uniqueKey.add(row.get(uniqueKeyColName));
			rows.put(uniqueKey, row);
		}

		return rows;
	}

	public Map<Object, DataSet> getAllRows(String keyColName, String firstNotUniqueColName) throws SQLException {
		return this.getAllRows(keyColName, Object.class, firstNotUniqueColName);
	}

	public <K> Map<K, DataSet> getAllRows(String keyColName, Class<K> keyColType, String firstNotUniqueColName) throws SQLException {
		for (int col = 1; col <= this.rs.getMetaData()
				.getColumnCount(); col++)
			if (this.rs.getMetaData()
					.getColumnLabel(col)
					.equals(firstNotUniqueColName))
				return this.getAllRows(keyColName, keyColType, col);

		throw new IllegalArgumentException("The column, '" + firstNotUniqueColName + "', does not exist");
	}

	@SuppressWarnings("unchecked")
	private <K> Map<K, DataSet> getAllRows(String keyColName, Class<K> keyColType, int firstNotUniqueColNum) throws SQLException {
		Map<K, DataSet> uniqueRows = new LinkedHashMap<K, DataSet>();

		DataSet row;
		while ((row = this.getNextRow()) != null) {
			K key = row.get(keyColName, keyColType);

			boolean allNull = true;
			FlexMap groupedRow = new FlexMap();
			for (int col = firstNotUniqueColNum; col <= this.rs.getMetaData()
					.getColumnCount(); col++) {
				String colName = this.rs.getMetaData()
						.getColumnLabel(col);
				Object colValue = row.get(colName);
				if (colValue != null) allNull = false;

				groupedRow.put(colName, colValue);
				row.remove(colName);
			}

			DataSet uniqueRow = uniqueRows.get(key);
			if (uniqueRow == null) uniqueRows.put(key, uniqueRow = row);

			List<FlexMap> groupedRows = (List<FlexMap>) uniqueRow.get("groupedList");
			if (groupedRows == null) uniqueRow.put("groupedList", groupedRows = new LinkedList<FlexMap>());

			if (!allNull) groupedRows.add(groupedRow);
		}

		return uniqueRows;
	}

	public <K, S> Map<K, DataSet> getAllRows(String keyColName, Class<K> keyColType, String onlyNotUniqueColName, Class<S> onlyNotUniqueColType)
			throws SQLException {
		for (int col = 1; col <= this.rs.getMetaData()
				.getColumnCount(); col++)
			if (this.rs.getMetaData()
					.getColumnLabel(col)
					.equals(onlyNotUniqueColName))
				return this.getAllRows(keyColName, keyColType, col, onlyNotUniqueColType);

		throw new IllegalArgumentException("The column, '" + onlyNotUniqueColName + "', does not exist");
	}

	@SuppressWarnings("unchecked")
	private <K, S> Map<K, DataSet> getAllRows(String keyColName, Class<K> keyColType, int onlyNotUniqueColNum, Class<S> onlyNotUniqueColType)
			throws SQLException {
		Map<K, DataSet> uniqueRows = new LinkedHashMap<K, DataSet>();

		DataSet row;
		while ((row = this.getNextRow()) != null) {
			K key = row.get(keyColName, keyColType);

			String colName = this.rs.getMetaData()
					.getColumnLabel(onlyNotUniqueColNum);
			S groupedValue = row.get(colName, onlyNotUniqueColType);

			DataSet uniqueRow = uniqueRows.get(key);
			if (uniqueRow == null) {
				row.remove(colName);
				uniqueRows.put(key, uniqueRow = row);
			}

			List<S> groupedRows = (List<S>) uniqueRow.get("groupedList");
			if (groupedRows == null) uniqueRow.put("groupedList", groupedRows = new LinkedList<S>());

			if (groupedValue != null) groupedRows.add(groupedValue);
		}

		return uniqueRows;
	}

	public Map<List<Object>, DataSet> getAllRows(List<String> keyColNames, String firstNotUniqueColName) throws SQLException {
		for (int col = 1; col <= this.rs.getMetaData()
				.getColumnCount(); col++)
			if (this.rs.getMetaData()
					.getColumnLabel(col)
					.equals(firstNotUniqueColName))
				return this.getAllRows(keyColNames, col);

		throw new IllegalArgumentException("The column, '" + firstNotUniqueColName + "', does not exist");
	}

	private Map<List<Object>, DataSet> getAllRows(List<String> keyColNames, int firstNotUniqueColNum) throws SQLException {
		Map<List<Object>, DataSet> uniqueRows = new LinkedHashMap<List<Object>, DataSet>();

		DataSet row;
		while ((row = this.getNextRow()) != null) {
			List<Object> key = new LinkedList<Object>();
			for (String keyColName : keyColNames)
				key.add(row.get(keyColName));

			boolean allNull = true;
			FlexMap groupedRow = new FlexMap();
			for (int col = firstNotUniqueColNum; col <= this.rs.getMetaData()
					.getColumnCount(); col++) {
				String colName = this.rs.getMetaData()
						.getColumnLabel(col);
				Object colValue = row.get(colName);
				if (colValue != null) allNull = false;

				groupedRow.put(colName, colValue);
				row.remove(colName);
			}

			List<FlexMap> groupedRows = new LinkedList<FlexMap>();
			if (!uniqueRows.containsKey(key)) {
				row.put("groupedList", groupedRows = new LinkedList<FlexMap>());
				uniqueRows.put(key, row);
			}

			if (!allNull) groupedRows.add(groupedRow);
		}

		return uniqueRows;
	}

	public Map<Object, List<DataSet>> getAllRowsGrouped(String groupingColName) throws SQLException {
		return this.getAllRowsGrouped(groupingColName, Object.class);
	}

	@SuppressWarnings("unchecked")
	public <K> Map<K, List<DataSet>> getAllRowsGrouped(String groupingColName, Class<K> groupingColType) throws SQLException {
		int groupingColNum = -1;

		for (int col = 1; col <= this.rs.getMetaData()
				.getColumnCount() && groupingColNum < 0; col++)
			if (this.rs.getMetaData()
					.getColumnLabel(col)
					.equals(groupingColName))
				groupingColNum = col;

		if (groupingColNum < 0) throw new IllegalArgumentException("The column, '" + groupingColName + "', does not exist");

		Map<K, List<DataSet>> map = new HashMap<K, List<DataSet>>();
		DataSet row = null;
		while ((row = this.getNextRow()) != null) {
			K key = (K) this.rs.getObject(groupingColNum);

			List<DataSet> rows = map.get(key);
			if (rows == null) map.put(key, rows = new LinkedList<DataSet>());
			rows.add(row);
		}

		return map;

	}

	@SuppressWarnings("unchecked")
	private <T> T getGenericObject(Class<T> targetType, Object sourceObject) {
		if (sourceObject == null) return null;
		if (targetType == sourceObject.getClass()) return (T) sourceObject;

		try {
			Constructor<T> cons = targetType.getConstructor(String.class);
			return cons.newInstance(sourceObject.toString());
		} catch (RuntimeException re) {
			throw re;
		} catch (Exception e) {
			throw new IllegalArgumentException(
					"The target type, '" + targetType.getName() + "', cannot be converted from source type, '" + sourceObject.getClass()
							.getName() + "'");
		}
	}

	@SuppressWarnings("unchecked")
	public static void merge(Map<Object, DataSet> data, String field, String keyColName, List<DataSet> mergingData) {
		for (DataSet mergingDataSet : mergingData) {
			Object key = mergingDataSet.get(keyColName);
			if (key == null) {
				StringBuilder fields = new StringBuilder();
				for (java.util.Iterator<String> i = mergingDataSet.iterator(); i.hasNext();)
					fields.append(i.next())
							.append('|');
				throw new IllegalArgumentException("A 'key' may not be null under any circumstance; " + fields.substring(0, fields.length() - 1));
			}

			DataSet dataSet = data.get(key);
			if (dataSet != null) {
				List<DataSet> mergedData = (List<DataSet>) dataSet.get(field);
				if (mergedData == null) dataSet.put(field, mergedData = new LinkedList<DataSet>());
				mergedData.add(mergingDataSet);
			}
		}
	}

	@SuppressWarnings("unchecked")
	public static void merge(Map<List<Object>, DataSet> data, String field, List<String> keyColNames, List<DataSet> mergingData) {
		for (DataSet mergingDataSet : mergingData) {
			List<Object> key = new LinkedList<Object>();
			for (String keyColName : keyColNames)
				key.add(mergingDataSet.get(keyColName));

			DataSet dataSet = data.get(key);
			if (dataSet != null) {
				List<DataSet> mergedData = (List<DataSet>) dataSet.get(field);
				if (mergedData == null) dataSet.put(field, mergedData = new LinkedList<DataSet>());
				mergedData.add(mergingDataSet);
			}
		}
	}

	/**
	 * This method is here because java.util.Map.values() may not retain order
	 */
	public static List<DataSet> convert(Map<Object, DataSet> dataSets) {
		List<DataSet> dataSetList = new LinkedList<DataSet>();
		for (Object key : dataSets.keySet())
			dataSetList.add(dataSets.get(key));
		return dataSetList;
	}

	@Override
	public java.util.Iterator<DataSet> iterator() {
		return new Iterator(this);
	}

	private class Iterator implements java.util.Iterator<DataSet> {

		private final FlexResultSet frs;
		private DataSet next = null;

		public Iterator(FlexResultSet frs) {
			this.frs = frs;
		}

		@Override
		public boolean hasNext() {
			if (this.next != null) {
				return true;
			} else {
				try {
					this.next = this.frs.getNextRow();
					return this.next != null;
				} catch (SQLException se) {

					return false;
				}
			}
		}

		@Override
		public DataSet next() {
			if (this.next != null) {
				DataSet next = this.next;
				this.next = null;
				return next;
			} else {
				try {
					DataSet next = this.frs.getNextRow();
					if (next == null) throw new NoSuchElementException();
					return next;
				} catch (SQLException se) {
					throw new NoSuchElementException(se.getMessage());
				}
			}
		}

	}

}
