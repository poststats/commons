package com.brianlong.sql;

public class Paging {

	private final int page;
	private final int perPage;
	private final long totalCount;

	public Paging(int perPage, int page) {
		this.page = page;
		this.perPage = perPage;
		this.totalCount = -1L;
	}

	public Paging(int perPage, int page, long totalCount) {
		this.page = page;
		this.perPage = perPage;
		this.totalCount = totalCount;
	}

	public Paging(Paging paging, long totalCount) {
		this.page = paging.getPage();
		this.perPage = paging.getPerPage();
		this.totalCount = totalCount;
	}

	public int getPage() {
		return this.page;
	}

	public int getPerPage() {
		return this.perPage;
	}

	public long getFirstIndex() {
		return (this.page - 1) * this.perPage;
	}

	public long getLastIndex() {
		long lastIndex = (long) this.page * this.perPage;
		return (this.totalCount < 0L || lastIndex < this.totalCount) ? lastIndex : this.totalCount;
	}

	public long getTotalCount() {
		return this.totalCount;
	}

	public int getLastPage() {
		return (int) ((this.totalCount - 1L) / this.perPage) + 1;
	}

}
