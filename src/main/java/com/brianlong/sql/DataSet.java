package com.brianlong.sql;

import com.brianlong.util.FlexMap;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Arrays;

/**
 *
 * @author Brian M Long
 */
public class DataSet extends FlexMap {

	private static final long serialVersionUID = 1L;

	private ResultSet rs = null;

	DataSet() {
	}

	public DataSet(ResultSet rs) throws SQLException {
		this(rs, true);
	}

	public DataSet(ResultSet rs, boolean cache) throws SQLException {
		if (cache) {
			for (int col = 1; col <= rs.getMetaData()
					.getColumnCount(); col++)
				this.put(rs.getMetaData()
						.getColumnLabel(col), rs.getObject(col));
		} else {
			this.rs = rs;
		}
	}

	@Override
	public Object get(String key) {
		if (this.rs == null) return super.get(key);

		try {
			return this.rs.getObject(key);
		} catch (SQLException se) {
			return null;
		}
	}

	@Override
	public Object put(String key, Object value) {
		if (this.rs == null) return super.put(key, value);

		try {
			Object oldvalue = this.rs.getObject(key);
			this.rs.updateObject(key, value);
			return oldvalue;
		} catch (SQLException se) {
			return null;
		}
	}

	public Boolean getBoolean(String field) {
		Object obj = this.get(field);

		Boolean value = null;
		if (obj == null) {
		} else if (obj instanceof Boolean) {
			value = (Boolean) obj;
		} else if (obj instanceof Number) {
			value = ((Number) obj).intValue() != 0;
		} else if (obj instanceof String) {
			value = new Boolean((String) obj);
		} else {
			value = false;
		}

		this.cache(field, value);
		return value;
	}

	public LocalDate getDate(String field) {
		Object obj = this.get(field);
		if (obj == null) return null;
		if (obj instanceof LocalDate) return (LocalDate) obj;

		java.util.Date date = (java.util.Date) obj;
		return Instant.ofEpochMilli(date.getTime())
				.atZone(ZoneId.systemDefault())
				.toLocalDate();
	}

	public LocalTime getTime(String field) {
		Object obj = this.get(field);
		if (obj == null) return null;
		if (obj instanceof LocalTime) return (LocalTime) obj;

		java.util.Date date = (java.util.Date) obj;
		return Instant.ofEpochMilli(date.getTime())
				.atZone(ZoneId.systemDefault())
				.toLocalTime();
	}

	public LocalDateTime getDateTime(String field) {
		Object obj = this.get(field);
		if (obj == null) return null;
		if (obj instanceof LocalDateTime) return (LocalDateTime) obj;

		java.util.Date date = (java.util.Date) obj;
		return Instant.ofEpochMilli(date.getTime())
				.atZone(ZoneId.systemDefault())
				.toLocalDateTime();
	}

	public Instant getTimestamp(String field) {
		Object obj = this.get(field);
		if (obj == null) return null;
		if (obj instanceof Instant) return (Instant) obj;
		if (obj.getClass()
				.isArray())
			throw new IllegalArgumentException("The '" + field + "' field is an array instead of a Date; value = " + Arrays.toString((byte[]) obj));
		if (!(obj instanceof java.util.Date))
			throw new IllegalArgumentException("The '" + field + "' field is of '" + obj.getClass() + "' type instead of a Date; value = " + obj.toString());

		java.util.Date date = (java.util.Date) obj;
		return Instant.ofEpochMilli(date.getTime());
	}

}
