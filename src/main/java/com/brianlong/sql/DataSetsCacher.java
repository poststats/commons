package com.brianlong.sql;

import com.brianlong.cache.CacheException;
import com.brianlong.cache.CacheRetrievalException;
import com.brianlong.cache.MemoryCacher;
import com.brianlong.cache.Reaper;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public abstract class DataSetsCacher<ID> extends MemoryCacher<ID, List<DataSet>> {

	public DataSetsCacher(long expirationMilliseconds, Reaper reaper) {
		super(expirationMilliseconds, reaper);
	}

	@Override
	public final List<DataSet> get(ID id) throws CacheRetrievalException {
		if (!this.isCached(id)) {
			try {
				this.add(id, this.getDataSets(id), false);
			} catch (CacheException ce) {
				throw new CacheRetrievalException(ce.getMessage(), ce);
			} catch (SQLException se) {
				throw new CacheRetrievalException(se.getMessage(), se);
			}
		}

		return super.get(id);
	}

	@Override
	public final Map<ID, List<DataSet>> get(Collection<ID> ids) throws CacheRetrievalException {
		List<ID> idsToCache = new LinkedList<ID>();

		Map<ID, List<DataSet>> map = new LinkedHashMap<ID, List<DataSet>>(ids.size());
		for (ID id : ids) {
			if (this.isCached(id)) {
				map.put(id, this.get(id));
			} else {
				idsToCache.add(id);
			}
		}

		try {
			Map<ID, List<DataSet>> bulkMap = this.getDataSets(idsToCache);
			for (Entry<ID, List<DataSet>> entry : bulkMap.entrySet()) {
				map.put(entry.getKey(), entry.getValue());
				this.add(entry.getKey(), entry.getValue(), false);
			}
		} catch (CacheException ce) {
			throw new CacheRetrievalException(ce.getMessage(), ce);
		} catch (SQLException se) {
			throw new CacheRetrievalException(se.getMessage(), se);
		}

		return map;
	}

	public abstract List<DataSet> getDataSets(ID id) throws SQLException;

	public Map<ID, List<DataSet>> getDataSets(Collection<ID> ids) throws SQLException {
		Map<ID, List<DataSet>> map = new LinkedHashMap<ID, List<DataSet>>(ids.size());
		for (ID id : ids) {
			List<DataSet> obj = this.getDataSets(id);
			map.put(id, obj);
		}

		return map;
	}

}
