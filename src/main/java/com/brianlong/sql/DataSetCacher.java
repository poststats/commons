package com.brianlong.sql;

import com.brianlong.cache.CacheException;
import com.brianlong.cache.CacheRetrievalException;
import com.brianlong.cache.MemoryCacher;
import com.brianlong.cache.Reaper;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public abstract class DataSetCacher<ID> extends MemoryCacher<ID, DataSet> {

	public DataSetCacher(long expirationMilliseconds, Reaper reaper) {
		super(expirationMilliseconds, reaper);
	}

	@Override
	public final DataSet get(ID id) throws CacheRetrievalException {
		if (!this.isCached(id)) {
			try {
				this.add(id, this.getDataSet(id), false);
			} catch (CacheException ce) {
				throw new CacheRetrievalException(ce.getMessage(), ce);
			} catch (SQLException se) {
				throw new CacheRetrievalException(se.getMessage(), se);
			}
		}

		return super.get(id);
	}

	@Override
	public final Map<ID, DataSet> get(Collection<ID> ids) throws CacheRetrievalException {
		List<ID> idsToCache = new LinkedList<ID>();

		Map<ID, DataSet> map = new LinkedHashMap<ID, DataSet>(ids.size());
		for (ID id : ids) {
			if (this.isCached(id)) {
				map.put(id, this.get(id));
			} else {
				idsToCache.add(id);
			}
		}

		try {
			Map<ID, DataSet> bulkMap = this.getDataSets(idsToCache);
			for (Entry<ID, DataSet> entry : bulkMap.entrySet()) {
				map.put(entry.getKey(), entry.getValue());
				this.add(entry.getKey(), entry.getValue(), false);
			}
		} catch (CacheException ce) {
			throw new CacheRetrievalException(ce.getMessage(), ce);
		} catch (SQLException se) {
			throw new CacheRetrievalException(se.getMessage(), se);
		}

		return map;
	}

	public abstract DataSet getDataSet(ID id) throws SQLException;

	public Map<ID, DataSet> getDataSets(Collection<ID> ids) throws SQLException {
		Map<ID, DataSet> map = new LinkedHashMap<ID, DataSet>(ids.size());
		for (ID id : ids) {
			DataSet obj = this.getDataSet(id);
			map.put(id, obj);
		}

		return map;
	}

}
