package com.brianlong.math;

import java.util.Arrays;

public class Vector {

	private double[] values;

	private Vector(double[] values) {
		this.values = values;
	}

	public Vector(double x, double y) {
		this.values = new double[] { x, y };
	}

	public Vector(double x, double y, double z) {
		this.values = new double[] { x, y, z };
	}

	double getValue(int dimension) {
		return this.values[dimension];
	}

	public double getX() {
		return this.values[0];
	}

	public double getY() {
		return this.values[1];
	}

	public double getZ() {
		return this.values[2];
	}

	public int getDimensions() {
		return this.values.length;
	}

	public double length() {
		double sqrs = 0.0;
		for (int d = 0; d < this.getDimensions(); d++)
			sqrs += Math.pow(this.values[d], 2.0);
		return Math.sqrt(sqrs);
	}

	public Vector plus(Vector vector) {
		if (this.getDimensions() != vector.getDimensions()) throw new IllegalArgumentException("An addition requires two vectors of equal dimensions");

		double[] values = new double[this.getDimensions()];
		for (int d = 0; d < this.getDimensions(); d++)
			values[d] = this.values[d] + vector.values[d];
		return new Vector(values);
	}

	public Vector minus(Vector vector) {
		if (this.getDimensions() != vector.getDimensions()) throw new IllegalArgumentException("An addition requires two vectors of equal dimensions");

		double[] values = new double[this.getDimensions()];
		for (int d = 0; d < this.getDimensions(); d++)
			values[d] = this.values[d] - vector.values[d];
		return new Vector(values);
	}

	public Vector not() {
		double[] values = new double[this.getDimensions()];
		for (int d = 0; d < this.getDimensions(); d++)
			values[d] = -this.values[d];
		return new Vector(values);
	}

	public Vector normalize() {
		double length = this.length();
		if (length == 0.0) throw new IllegalStateException("A zero-length vector cannot be normalized");

		double[] values = new double[this.values.length];
		for (int d = 0; d < this.getDimensions(); d++)
			values[d] = this.values[d] / length;
		return new Vector(values);
	}

	public double dot(Vector vector) {
		if (this.getDimensions() != vector.getDimensions()) throw new IllegalArgumentException("A dot product requires two vectors of equal dimensions");

		double dot = 0.0;
		for (int d = 0; d < this.getDimensions(); d++)
			dot += this.values[d] * vector.values[d];
		return dot;
	}

	public Vector cross(Vector vector) {
		if (this.getDimensions() != 3 || vector.getDimensions() != 3)
			throw new IllegalArgumentException("A cross product requires each vector to have 3 dimensions");

		return new Vector(this.getY() * vector.getZ() - vector.getY() * this.getZ(), this.getZ() * vector.getX() - vector.getZ() * this.getX(),
				this.getX() * vector.getY() - vector.getX() * this.getY());
	}

	public double angleInRadians(Vector vector) {
		try {
			return Math.acos(this.normalize()
					.dot(vector.normalize()));
		} catch (IllegalStateException ise) {
			return Double.NaN;
		}
	}

	public double angleInDegrees(Vector vector) {
		return Math.toDegrees(this.angleInRadians(vector));
	}

	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof Vector)) return false;

		Vector vector = (Vector) obj;
		return Arrays.equals(this.values, vector.values);
	}

	public int hashCode() {
		return Arrays.hashCode(this.values);
	}

	public String toString() {
		StringBuilder strbuilder = new StringBuilder();
		strbuilder.append("< ");
		for (double value : this.values)
			strbuilder.append(value)
					.append(" , ");
		strbuilder.delete(strbuilder.length() - 2, strbuilder.length());
		strbuilder.append(">");
		return strbuilder.toString();
	}

}
