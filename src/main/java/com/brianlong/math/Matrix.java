package com.brianlong.math;

import java.util.Arrays;

public class Matrix {

	private double[][] values;

	public static Matrix createIdentity(int dimensions) {
		double[][] values = new double[dimensions][dimensions];
		for (int d = 0; d < dimensions; d++)
			values[d][d] = 1.0;
		return new Matrix(values);
	}

	public static Matrix createScale(Vector vector) {
		double[][] values = new double[vector.getDimensions() + 1][vector.getDimensions() + 1];
		for (int d = 0; d < vector.getDimensions(); d++)
			values[d][d] = vector.getValue(d);
		values[values.length - 1][values.length - 1] = 1.0;
		return new Matrix(values);
	}

	public static Matrix createTranslate(Vector vector) {
		double[][] values = new double[vector.getDimensions() + 1][vector.getDimensions() + 1];
		for (int d = 0; d < vector.getDimensions(); d++) {
			values[d][d] = 1.0;
			values[values.length - 1][d] = vector.getValue(d);
		}
		values[values.length - 1][values.length - 1] = 1.0;
		return new Matrix(values);
	}

	public Matrix(double[][] values) {
		this.values = values;
	}

	public Matrix plus(Matrix matrix) {
		if (this.values.length != matrix.values.length) throw new IllegalArgumentException("An addition requires two matricies of equal sizes");

		double[][] values = new double[this.values.length][];
		for (int x = 0; x < this.values.length; x++) {
			if (this.values[x].length != matrix.values[x].length) throw new IllegalArgumentException("An addition requires two matricies of equal sizes");

			values[x] = new double[this.values[x].length];
			for (int y = 0; y < this.values[x].length; y++)
				values[x][y] = this.values[x][y] + matrix.values[x][y];
		}

		return new Matrix(values);
	}

	public Matrix minus(Matrix matrix) {
		if (this.values.length != matrix.values.length) throw new IllegalArgumentException("An addition requires two matricies of equal sizes");

		double[][] values = new double[this.values.length][];
		for (int x = 0; x < this.values.length; x++) {
			if (this.values[x].length != matrix.values[x].length) throw new IllegalArgumentException("An addition requires two matricies of equal sizes");

			values[x] = new double[this.values[x].length];
			for (int y = 0; y < this.values[x].length; y++)
				values[x][y] = this.values[x][y] - matrix.values[x][y];
		}

		return new Matrix(values);
	}

	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof Matrix)) return false;

		Matrix matrix = (Matrix) obj;
		return Arrays.deepEquals(this.values, matrix.values);
	}

	public int hashCode() {
		return Arrays.deepHashCode(this.values);
	}

	public String toString() {
		StringBuilder strbuilder = new StringBuilder();

		for (int x = 0; x < this.values.length; x++) {
			strbuilder.append("| ");
			for (int y = 0; y < this.values[x].length; y++)
				strbuilder.append(this.values[x][y])
						.append(" ");
			strbuilder.append("|\r\n");
		}

		return strbuilder.toString();
	}

}
