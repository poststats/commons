package com.brianlong;

public class ExternalSystemException extends Exception {

	private static final long serialVersionUID = 1L;

	public ExternalSystemException(String message) {
		super(message);
	}

	public ExternalSystemException(String message, Throwable t) {
		super(message, t);
	}

}
