package com.brianlong.gis;

import com.brianlong.GoogleApis;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

public class GoogleMapsCartographer {

	public enum MapType {
		roadmap, satellite, hybrid, terrain
	}

	public static final URI BASE_URI = URI.create("https://maps.googleapis.com/maps/api/staticmap");

	public URL getMapAsURL(Location location, double radiusInMiles, int widthInPixels, int heightInPixels) throws IOException {
		HttpGet method = this.getMapAsMethod(location, radiusInMiles, widthInPixels, heightInPixels);
		URI uri = method.getURI();
		return new URL(uri.toString());
	}

	public InputStream getMapAsStream(Location location, double radiusInMiles, int widthInPixels, int heightInPixels) throws IOException {
		HttpGet method = this.getMapAsMethod(location, radiusInMiles, widthInPixels, heightInPixels);

		CloseableHttpClient client = HttpClientBuilder.create()
				.build();
		CloseableHttpResponse response = client.execute(method);
		if (response.getStatusLine()
				.getStatusCode() % 100 != 2)
			throw new IOException("A status code of 20x was expected");

		return response.getEntity()
				.getContent();
	}

	private HttpGet getMapAsMethod(Location location, double radiusInMiles, int widthInPixels, int heightInPixels) {
		try {
			URI uri = new URIBuilder(BASE_URI).addParameter("center", location.getLatitudeInDegrees() + "," + location.getLongitudeInDegrees())
					.addParameter("zoom", String.valueOf(miles2zoom(radiusInMiles, widthInPixels, heightInPixels)))
					.addParameter("size", widthInPixels + "x" + heightInPixels)
					.addParameter("format", "PNG")
					.addParameter("maptype", MapType.roadmap.toString())
					.addParameter("sensor", "false")
					.addParameter("key", GoogleApis.KEY)
					.build();

			return new HttpGet(uri);
		} catch (URISyntaxException use) {
			throw new RuntimeException("This should never happen", use);
		}
	}

	public static int miles2zoom(double radiusInMiles, int widthInPixels, int heightInPixels) {
		int pixels = Math.min(widthInPixels, heightInPixels);
		double milesPerPixel = radiusInMiles * 2.0 / pixels;
		double milesPer90Pixels = milesPerPixel * 90.0;
		double zoomLevel = 13.0 - Math.log(milesPer90Pixels) / Math.log(2);
		return (int) Math.floor(zoomLevel);
	}

}
