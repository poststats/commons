package com.brianlong.gis;

import com.brianlong.xml.XmlFactory;
import java.io.StringReader;
import java.io.StringWriter;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class MapQuestLocation implements Location, USALocation, CanadaLocation {

	private static final long serialVersionUID = 1L;

	private static final TransformerFactory TRANSFORMER_FACTORY = TransformerFactory.newInstance();
	private static XPathExpression xpathQuality;
	private static XPathExpression xpathLatitude;
	private static XPathExpression xpathLongitude;
	private static XPathExpression xpathCountryCode;
	private static XPathExpression xpathStateCode;
	private static XPathExpression xpathCounty;
	private static XPathExpression xpathCity;
	private static XPathExpression xpathAddress;
	private static XPathExpression xpathZip;

	static {
		XPath xpath = XmlFactory.newXPath();
		try {
			xpathQuality = xpath.compile("geocodeQualityCode/text()");
			xpathLatitude = xpath.compile("latLng/lat/text()");
			xpathLongitude = xpath.compile("latLng/lng/text()");
			xpathCountryCode = xpath.compile("adminArea1/text()");
			xpathStateCode = xpath.compile("adminArea3/text()");
			xpathCounty = xpath.compile("adminArea4/text()");
			xpathCity = xpath.compile("adminArea5/text()");
			xpathAddress = xpath.compile("street/text()");
			xpathZip = xpath.compile("postalCode/text()");
		} catch (XPathExpressionException xpee) {
			throw new RuntimeException("This should never happen", xpee);
		}
	}

	private final String xml;
	private Element element = null;

	public MapQuestLocation(Element result) {
		StringWriter swriter = new StringWriter();

		try {
			Transformer transformer = TRANSFORMER_FACTORY.newTransformer();
			transformer.transform(new DOMSource(result), new StreamResult(swriter));
		} catch (TransformerException te) {
			throw new RuntimeException("This should never happen");
		}

		this.xml = swriter.toString();
	}

	public String getQuality() {
		return this.getString(xpathQuality);
	}

	@Override
	public Double getLatitudeInDegrees() {
		return this.getDouble(xpathLatitude);
	}

	@Override
	public Double getLongitudeInDegrees() {
		return this.getDouble(xpathLongitude);
	}

	@Override
	public Double getAltitudeInMeters() {
		throw new UnsupportedOperationException();
	}

	@Override
	public String getCountryCode() {
		return this.getString(xpathCountryCode);
	}

	@Override
	public String getStateCode() {
		return this.getString(xpathStateCode);
	}

	@Override
	public String getProvinceCode() {
		return this.getString(xpathStateCode);
	}

	@Override
	public String getCounty() {
		return this.getString(xpathCounty);
	}

	@Override
	public String getParish() {
		return this.getString(xpathCounty);
	}

	@Override
	public String getTownship() {
		throw new UnsupportedOperationException();
	}

	@Override
	public String getBorough() {
		throw new UnsupportedOperationException();
	}

	@Override
	public String getLocality() {
		return this.getString(xpathCity);
	}

	@Override
	public String getThoroughfare() {
		return this.getString(xpathAddress);
	}

	@Override
	public String getPostalCodeFull() {
		return this.getString(xpathZip);
	}

	@Override
	public String getPostalCode() {
		String zip = this.getPostalCodeFull();
		if (zip != null) zip = zip.substring(0, 5);
		return zip;
	}

	private Double getDouble(XPathExpression xexpression) {
		String str = this.getString(xexpression);
		return str == null ? null : new Double(str);
	}

	private String getString(XPathExpression xexpression) {
		try {
			String str = (String) xexpression.evaluate(this.getElement(), XPathConstants.STRING);
			return str == null || str.length() == 0 ? null : str;
		} catch (XPathExpressionException xpee) {
			throw new RuntimeException("This should never happen", xpee);
		}
	}

	private Element getElement() {
		if (this.element != null) return this.element;

		DOMResult dresult = new DOMResult();

		try {
			Transformer transformer = TRANSFORMER_FACTORY.newTransformer();
			transformer.transform(new StreamSource(new StringReader(this.xml)), dresult);
		} catch (TransformerException te) {
			throw new RuntimeException("This should never happen");
		}

		Document document = (Document) dresult.getNode();
		this.element = document.getDocumentElement();
		return this.element;
	}

}
