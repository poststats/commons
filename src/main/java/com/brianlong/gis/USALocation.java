package com.brianlong.gis;

public interface USALocation extends Location {

	String getStateCode();

	String getCounty();

	String getParish();

	String getTownship();

	String getBorough();

}
