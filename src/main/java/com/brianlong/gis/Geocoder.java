package com.brianlong.gis;

import com.brianlong.cache.CacheException;
import java.io.IOException;
import java.util.List;

public interface Geocoder<T extends Location> {

	T find(String query) throws GeocodingException, CacheException, IOException;

	List<T> search(String query) throws GeocodingException, CacheException, IOException;

}
