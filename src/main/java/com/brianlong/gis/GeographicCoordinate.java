package com.brianlong.gis;

class GeographicCoordinate {

	/**
	 * @param latitudeDegrees A latitude in degrees
	 * @return A distance in meters
	 *         {@link http://www.newton.dep.anl.gov/askasci/gen99/gen99915.htm}
	 */
	public static double computeRadiusInMeters(double latitudeDegrees) {
		double latitude = Math.toRadians(latitudeDegrees);
		double re = GeographicDistance.EARTH_RADIUS_EQUATORIAL_METERS;
		double rp = GeographicDistance.EARTH_RADIUS_POLAR_METERS;
		double re2 = Math.pow(re, 2.0);
		double rp2 = Math.pow(rp, 2.0);
		/*
		 * if (false) { // guy named Vince Calder // re*{1 +[(rp^2 - re^2)/re^2]
		 * *(sin(d))^2} ^ -1/2 return Math.sqrt(re2 + (rp2 - re2) *
		 * Math.pow(Math.sin(latitude), 2.0)); }
		 */
		// Wikipedia
		// ((cost*re^2)^2 + (sint*rp^2)^2) / ((cost*re)^2 + (sint*rp)^2)
		double numerator = Math.pow(Math.cos(latitude) * re2, 2.0) + Math.pow(Math.sin(latitude) * rp2, 2.0);
		double denominator = Math.pow(Math.cos(latitude) * re, 2.0) + Math.pow(Math.sin(latitude) * rp, 2.0);
		return Math.sqrt(numerator / denominator);

		// ((cost*re^2)^2 + (sint*rp^2)^2) / ((cost*re)^2 + (sint*rp)^2)
		// (cost^2*re^4 + sint^2*rp^4) / (cost^2*re^2 + sint^2*rp^2)
		// ((1-sint^2)*re^4 + sint^2*rp^4) / ((1-sint^2)*re^2 + sint^2*rp^2)
		// (re^4 - sint^2*re^4 + sint^2*rp^4) / (re^2 - sint^2*re^2 + sint^2*rp^2)
		// (re^4 - sint^2*(re^4 + rp^4)) / (re^2 - sint^2*(re^2 + rp^2))
		// (1 - sint^2*(1 + rp^4/re^4)) * re^4 / (1 - sint^2*(1 + rp^2/re^2)) * re^2
		// (1 - sint^2*(1 + rp^4/re^4)) * re^2 / (1 - sint^2*(1 + rp^2/re^2))
	}

	private double latitude;
	private double longitude;
	private double altitude;

	public GeographicCoordinate(double latitudeInDegrees, double longitudeInDegrees, double altitudeInMeters) {
		this.latitude = latitudeInDegrees;
		this.longitude = longitudeInDegrees;
		this.altitude = altitudeInMeters;
	}

	public GeographicCoordinate(Location location) {
		this.latitude = location.getLatitudeInDegrees();
		this.longitude = location.getLongitudeInDegrees();
		this.altitude = location.getAltitudeInMeters();
	}

	public double getLatitudeInDegrees() {
		return this.latitude;
	}

	public double getLongitudeInDegrees() {
		return this.longitude;
	}

	public double getAltitudeInMeters() {
		return this.altitude;
	}

	public CartesianCoordinate toCartesianCoordinate() {
		// convert geographic to spheric
		double phi = Math.toRadians(90.0 - this.latitude);
		double theta = Math.toRadians(this.longitude);
		double radius = this.altitude + GeographicCoordinate.computeRadiusInMeters(this.latitude);

		// convert spheric to cartesian
		double x = radius * Math.sin(phi) * Math.cos(theta);
		double y = radius * Math.sin(phi) * Math.sin(theta);
		double z = radius * Math.cos(phi);

		return new CartesianCoordinate(x, y, z);
	}

	public String toString() {
		return "{ " + this.latitude + " , " + this.longitude + " , " + this.altitude + " }";
	}

}
