package com.brianlong.gis;

public interface CanadaLocation extends Location {

	String getProvinceCode();

}
