package com.brianlong.gis;

import com.brianlong.DeveloperException;
import com.brianlong.util.StringUtil;
import com.brianlong.xml.XmlFactory;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class GoogleMapsGeocodeResponse {

	private final Element rootElement;

	public GoogleMapsGeocodeResponse(InputStream istream) throws IOException, SAXException {
		try {
			DocumentBuilder docbuilder = XmlFactory.newDocumentBuilder();
			Document document = docbuilder.parse(istream);
			this.rootElement = document.getDocumentElement();
		} catch (ParserConfigurationException pce) {
			throw new DeveloperException(pce);
		}
	}

	public String getStatusCode() {
		XPath xpathStatusCode = XmlFactory.newXPath();
		try {
			String statusCode = (String) xpathStatusCode.evaluate("status/text()", rootElement, XPathConstants.STRING);
			return StringUtil.getInstance()
					.trim(statusCode);
		} catch (XPathExpressionException xpee) {
			throw new DeveloperException(xpee);
		}
	}

	public String getErrorMessage() {
		XPath xpathErrorMessage = XmlFactory.newXPath();
		try {
			String errorMessage = (String) xpathErrorMessage.evaluate("error_message/text()", rootElement, XPathConstants.STRING);
			return StringUtil.getInstance()
					.trim(errorMessage);
		} catch (XPathExpressionException xpee) {
			throw new DeveloperException(xpee);
		}
	}

	public GoogleMapsLocation getFirstLocation() {
		XPath xpathResult = XmlFactory.newXPath();
		try {
			Element result = (Element) xpathResult.evaluate("result", rootElement, XPathConstants.NODE);
			return result == null ? null : new GoogleMapsLocation(result);
		} catch (XPathExpressionException xpee) {
			throw new DeveloperException(xpee);
		}
	}

	public List<GoogleMapsLocation> getLocations() {
		XPath xpathResult = XmlFactory.newXPath();
		try {
			NodeList results = (NodeList) xpathResult.evaluate("result", rootElement, XPathConstants.NODESET);

			ArrayList<GoogleMapsLocation> locations = new ArrayList<GoogleMapsLocation>(results.getLength());
			for (int i = 0; i < results.getLength(); i++)
				locations.add(new GoogleMapsLocation((Element) results.item(i)));
			return locations;
		} catch (XPathExpressionException xpee) {
			throw new DeveloperException(xpee);
		}
	}

}
