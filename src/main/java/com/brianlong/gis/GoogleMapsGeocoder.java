package com.brianlong.gis;

import com.brianlong.cache.CacheException;
import com.brianlong.cache.MemoryCacher;
import com.brianlong.cache.ReaperFactory;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.xml.sax.SAXException;

public class GoogleMapsGeocoder implements Geocoder<GoogleMapsLocation> {

	private static final long DAY_MS = 24L * 60L * 60L * 1000L;

	private final MemoryCacher<String, GoogleMapsLocation> cache = new MemoryCacher<String, GoogleMapsLocation>(DAY_MS * 7L, ReaperFactory.getInstance(DAY_MS));
	private final URI uri = URI.create("https://maps.googleapis.com/maps/api/geocode/xml");
	private final String key;

	public GoogleMapsGeocoder(String key) {
		this.key = key;
	}

	public GoogleMapsLocation find(String query) throws IOException, CacheException, GeocodingException {
		if (this.cache.isCached(query)) return this.cache.get(query);

		GoogleMapsGeocodeResponse response = this.executeAndValidateQuery(query);
		GoogleMapsLocation location = response.getFirstLocation();
		this.cache.add(query, location, true);
		return location;
	}

	public List<GoogleMapsLocation> search(String query) throws IOException, CacheException, GeocodingException {
		GoogleMapsGeocodeResponse response = this.executeAndValidateQuery(query);
		List<GoogleMapsLocation> locations = response.getLocations();
		this.cache.add(query, locations.iterator()
				.next(), true);
		return locations;
	}

	private GoogleMapsGeocodeResponse executeAndValidateQuery(String query) throws IOException, GeocodingException {
		try {
			URI uri = new URIBuilder(this.uri).addParameter("address", query)
					.addParameter("sensor", "false")
					.addParameter("key", this.key)
					.build();

			HttpGet method = new HttpGet(uri);

			CloseableHttpClient client = HttpClientBuilder.create()
					.build();
			try {
				CloseableHttpResponse response = client.execute(method);
				if (response.getStatusLine()
						.getStatusCode() % 100 != 2)
					throw new IOException("A status code of 20x was expected");

				InputStream istream = response.getEntity()
						.getContent();
				try {
					GoogleMapsGeocodeResponse xmlResponse = new GoogleMapsGeocodeResponse(istream);
					String statusCode = xmlResponse.getStatusCode();
					if (!"OK".equals(statusCode))
						throw new GeocodingException("Google Maps returned status code " + statusCode + ": " + xmlResponse.getErrorMessage());
					return xmlResponse;
				} catch (SAXException se) {
					throw new RuntimeException(se);
				} finally {
					istream.close();
				}
			} finally {
				client.close();
			}
		} catch (URISyntaxException use) {
			throw new RuntimeException("This should never happen", use);
		}
	}

}
