package com.brianlong.gis;

public interface ItalyLocation extends Location {

	String getRegion();

}
