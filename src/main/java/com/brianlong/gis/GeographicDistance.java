package com.brianlong.gis;

public interface GeographicDistance {

	static final double EARTH_RADIUS_EQUATORIAL_METERS = 6378137.0;
	static final double EARTH_RADIUS_POLAR_METERS = 6356752.0;
	static final double EARTH_RADIUS_MEAN_METERS = 6371009.0;

	/**
	 * @param location1 A starting location
	 * @param location2 A finishing location
	 * @return A distance of 0 or more in meters
	 */
	double computeInMeters(Location location1, Location location2);

}
