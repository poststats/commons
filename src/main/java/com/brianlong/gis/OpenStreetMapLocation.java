package com.brianlong.gis;

import com.brianlong.data.StateMap;
import com.brianlong.util.StringUtil;
import com.brianlong.xml.FlexElement;
import com.brianlong.xml.XmlFactory;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class OpenStreetMapLocation implements Location, USALocation, CanadaLocation {

	private static final long serialVersionUID = 1L;

	public enum Classification {
		Place, Highway, Boundary
	}

	public enum Type {
		House, Residential, Village, City, Administrative, Postcode
	}

	private static final TransformerFactory TRANSFORMER_FACTORY = TransformerFactory.newInstance();
	private static XPathExpression xpathClass;
	private static XPathExpression xpathType;
	private static XPathExpression xpathRank;
	private static XPathExpression xpathLatitude;
	private static XPathExpression xpathLongitude;
	private static XPathExpression xpathCountryCode;
	private static XPathExpression xpathState;
	private static XPathExpression xpathCounty;
	private static XPathExpression xpathHamlet;
	private static XPathExpression xpathSuburb;
	private static XPathExpression xpathCity;
	private static XPathExpression xpathRoad;
	private static XPathExpression xpathHouseNumber;
	private static XPathExpression xpathZip;
	private static Map<String, String> stateMap;

	static {
		XPath xpath = XmlFactory.newXPath();
		try {
			xpathClass = xpath.compile("@class");
			xpathType = xpath.compile("@type");
			xpathRank = xpath.compile("@place_rank");
			xpathLatitude = xpath.compile("@lat");
			xpathLongitude = xpath.compile("@lon");
			xpathCountryCode = xpath.compile("country_code/text()");
			xpathState = xpath.compile("state/text()");
			xpathCounty = xpath.compile("county/text()");
			xpathHamlet = xpath.compile("hamlet/text()");
			xpathSuburb = xpath.compile("suburb/text()");
			xpathCity = xpath.compile("city/text()");
			xpathRoad = xpath.compile("road/text()");
			xpathHouseNumber = xpath.compile("house_number/text()");
			xpathZip = xpath.compile("postcode/text()");
		} catch (XPathExpressionException xpee) {
			throw new RuntimeException("This should never happen", xpee);
		}

		stateMap = new HashMap<String, String>(StateMap.getInstance()
				.size());
		for (String stateCode : StateMap.getInstance()
				.keySet()) {
			stateMap.put(StateMap.getInstance()
					.get(stateCode), stateCode);
		}
	}

	private final String xml;
	private Element element = null;

	public OpenStreetMapLocation(Element result) {
		StringWriter swriter = new StringWriter();

		try {
			Transformer transformer = TRANSFORMER_FACTORY.newTransformer();
			transformer.transform(new DOMSource(result), new StreamResult(swriter));
		} catch (TransformerException te) {
			throw new RuntimeException("This should never happen");
		}

		this.xml = swriter.toString();
	}

	public Classification getClassification() {
		String classification = this.getString(xpathClass);
		classification = StringUtil.getInstance()
				.toUppercaseWords(classification);
		try {
			return Classification.valueOf(classification);
		} catch (IllegalArgumentException iae) {
			return null;
		}
	}

	public Type getType() {
		String type = this.getString(xpathType);
		type = StringUtil.getInstance()
				.toUppercaseWords(type);
		try {
			return Type.valueOf(type);
		} catch (IllegalArgumentException iae) {
			return null;
		}
	}

	public Integer getRank() {
		return this.getInteger(xpathRank);
	}

	@Override
	public Double getLatitudeInDegrees() {
		return this.getDouble(xpathLatitude);
	}

	@Override
	public Double getLongitudeInDegrees() {
		return this.getDouble(xpathLongitude);
	}

	@Override
	public Double getAltitudeInMeters() {
		throw new UnsupportedOperationException();
	}

	@Override
	public String getCountryCode() {
		String code = this.getString(xpathCountryCode);
		return code == null ? null : code.toUpperCase();
	}

	public String getState() {
		return this.getString(xpathState);
	}

	@Override
	public String getStateCode() {
		return stateMap.get(this.getString(xpathState));
	}

	@Override
	public String getProvinceCode() {
		// String state = this.getState();
		throw new UnsupportedOperationException();
	}

	@Override
	public String getCounty() {
		return this.getString(xpathCounty);
	}

	@Override
	public String getParish() {
		return this.getString(xpathCounty);
	}

	@Override
	public String getTownship() {
		return this.getString(xpathHamlet, xpathSuburb);
	}

	@Override
	public String getBorough() {
		return this.getString(xpathCity);
	}

	@Override
	public String getLocality() {
		return this.getString(xpathCity, xpathSuburb, xpathHamlet);
	}

	@Override
	public String getThoroughfare() {
		String road = this.getString(xpathRoad);
		if (StringUtil.getInstance()
				.isEmpty(road))
			return null;

		String houseNumber = this.getString(xpathHouseNumber);
		return StringUtil.getInstance()
				.isEmpty(houseNumber) ? road : (houseNumber + " " + road);
	}

	@Override
	public String getPostalCodeFull() {
		return this.getString(xpathZip);
	}

	@Override
	public String getPostalCode() {
		String zip = this.getPostalCodeFull();
		if (zip != null) zip = zip.substring(0, 5);
		return zip;
	}

	private Integer getInteger(XPathExpression xexpression) {
		String str = this.getString(xexpression);
		return str == null ? null : new Integer(str);
	}

	private Double getDouble(XPathExpression xexpression) {
		String str = this.getString(xexpression);
		return str == null ? null : new Double(str);
	}

	private String getString(XPathExpression... xexpressions) {
		try {
			for (XPathExpression xexpression : xexpressions) {
				try {
					String str = (String) xexpression.evaluate(this.getElement(), XPathConstants.STRING);
					if (str != null && str.length() > 0) return str;
				} catch (NullPointerException npe) {
				}
			}

			return null;
		} catch (XPathExpressionException xpee) {
			throw new RuntimeException("This should never happen", xpee);
		}
	}

	private Element getElement() {
		if (this.element != null) return this.element;

		DOMResult dresult = new DOMResult();

		try {
			Transformer transformer = TRANSFORMER_FACTORY.newTransformer();
			transformer.transform(new StreamSource(new StringReader(this.xml)), dresult);
		} catch (TransformerException te) {
			throw new RuntimeException("This should never happen");
		}

		Document document = (Document) dresult.getNode();
		this.element = document.getDocumentElement();
		return this.element;
	}

	private Element getFirstPlace() {
		return new FlexElement(this.getElement()).getFirstChild()
				.getElement();
	}

}
