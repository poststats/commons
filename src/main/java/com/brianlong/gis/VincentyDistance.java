package com.brianlong.gis;

import static java.lang.Math.*;

/**
 * This is the Java implementation of the pseudo-code documented Wikipedia's
 * entry for Vincenty's formulae.
 *
 * {@link http://en.wikipedia.org/wiki/Vincenty%27s_formulae}
 */
public class VincentyDistance implements GeographicDistance {

	private static final VincentyDistance INSTANCE = new VincentyDistance();

	public static VincentyDistance getInstance() {
		return VincentyDistance.INSTANCE;
	}

	private VincentyDistance() {
	}

	public double computeInMeters(Location location1, Location location2) {
		double a = EARTH_RADIUS_EQUATORIAL_METERS;
		double b = EARTH_RADIUS_POLAR_METERS;
		double lat1 = toRadians(location1.getLatitudeInDegrees());
		double lat2 = toRadians(location2.getLatitudeInDegrees());
		double dlon = toRadians(location1.getLongitudeInDegrees() - location2.getLongitudeInDegrees());
		double f = (a - b) / a;
		double U1 = atan((1.0 - f) * tan(lat1));
		double U2 = atan((1.0 - f) * tan(lat2));
		double v1 = dlon;
		double v2 = 2 * PI;

		double sino = 0.0, coso = 0.0, o = 0.0, cos2a = 0.0, cos2om = 0.0;
		while (abs(v1 - v2) > 10E-12) {
			sino = sqrt(pow(cos(U2) * sin(v1), 2.0) + pow(cos(U1) * sin(U2) - sin(U1) * cos(U2) * cos(v1), 2.0));
			coso = sin(U1) * sin(U2) + cos(U1) * cos(U2) * cos(v1);
			o = atan2(sino, coso);
			double sina = cos(U1) * cos(U2) * sin(v1) / sino;
			cos2a = 1.0 - pow(sina, 2.0);
			cos2om = coso - 2.0 * sin(U1) * sin(U2) / cos2a;
			double C = f * cos2a * (4.0 + f * (4.0 - 3.0 * cos2a)) / 16.0;
			v2 = v1;
			v1 = dlon + (1.0 - C) * f * sina * (o + C * sino * (cos2om + C * coso * (-1.0 + 2.0 * pow(cos2om, 2.0))));
		}

		double a2 = pow(a, 2.0);
		double b2 = pow(b, 2.0);
		double u2 = cos2a * (a2 - b2) / b2;
		double A = 1.0 + u2 * (4096.0 + u2 * (-768.0 + u2 * (320.0 - 175.0 * u2))) / 16384.0;
		double B = u2 * (256.0 + u2 * (-128.0 + u2 * (74.0 - 47.0 * u2))) / 1024.0;
		double Do = B * sino * (cos2om
				+ B * (coso * (-1.0 + 2.0 * pow(cos2om, 2.0)) - B * cos2om * (-3.0 + 4.0 * pow(sino, 2.0)) * (-3.0 + 4.0 * pow(cos2om, 2.0)) / 6.0) / 4.0);
		double s = b * A * (o - Do);

		return s;
	}

}
