package com.brianlong.gis;

import static java.lang.Math.*;

/**
 * This is the Java implementation of the pseudo-code documented Wikipedia's
 * entry for Vincenty's formulae.
 *
 * {@link http://en.wikipedia.org/wiki/Vincenty%27s_formulae}
 */
public class FlatEarthDistance implements GeographicDistance {

	private static final FlatEarthDistance INSTANCE = new FlatEarthDistance();

	public static FlatEarthDistance getInstance() {
		return FlatEarthDistance.INSTANCE;
	}

	private FlatEarthDistance() {
	}

	public double computeInMeters(Location location1, Location location2) {
		double minlat = Math.min(location1.getLatitudeInDegrees(), location2.getLatitudeInDegrees());
		double maxlat = Math.max(location1.getLatitudeInDegrees(), location2.getLatitudeInDegrees());
		double minlon = Math.min(location1.getLongitudeInDegrees(), location2.getLongitudeInDegrees());
		double maxlon = Math.max(location1.getLongitudeInDegrees(), location2.getLongitudeInDegrees());
		double dlat = Math.toRadians(maxlat - minlat);
		double dlon = Math.toRadians(maxlon - minlon);

		double maxlatRadius = GeographicCoordinate.computeRadiusInMeters(maxlat);
		double minlatRadius = GeographicCoordinate.computeRadiusInMeters(minlat);
		double avglatRadius = (maxlatRadius + minlatRadius) / 2.0;
		double maxlatEastWest = maxlatRadius * dlon;
		double minlatEastWest = minlatRadius * dlon;
		double northSouth = avglatRadius * dlat;

		double northSouth2 = pow(northSouth, 2.0);
		double maxlatDistance = sqrt(pow(maxlatEastWest, 2.0) + northSouth2);
		double minlatDistance = sqrt(pow(minlatEastWest, 2.0) + northSouth2);
		double avglatDistance = (maxlatDistance + minlatDistance) / 2.0;
		return avglatDistance;
	}

}
