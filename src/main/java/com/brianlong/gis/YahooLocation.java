package com.brianlong.gis;

import com.brianlong.xml.XmlFactory;
import java.io.StringReader;
import java.io.StringWriter;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class YahooLocation implements Location, USALocation, CanadaLocation {

	private static final long serialVersionUID = 1L;

	private static final TransformerFactory TRANSFORMER_FACTORY = TransformerFactory.newInstance();
	private static XPathExpression xpathQuality;
	private static XPathExpression xpathRadius;
	private static XPathExpression xpathLatitude;
	private static XPathExpression xpathLongitude;
	private static XPathExpression xpathCountry;
	private static XPathExpression xpathCountryCode;
	private static XPathExpression xpathState;
	private static XPathExpression xpathStateCode;
	private static XPathExpression xpathCounty;
	private static XPathExpression xpathCity;
	private static XPathExpression xpathName;
	private static XPathExpression xpathHouse;
	private static XPathExpression xpathStreet;
	private static XPathExpression xpathUzip;

	static {
		XPath xpath = XmlFactory.newXPath();
		try {
			xpathQuality = xpath.compile("quality/text()");
			xpathRadius = xpath.compile("radius/text()");
			xpathLatitude = xpath.compile("latitude/text()");
			xpathLongitude = xpath.compile("longitude/text()");
			xpathCountry = xpath.compile("country/text()");
			xpathCountryCode = xpath.compile("countrycode/text()");
			xpathState = xpath.compile("state/text()");
			xpathStateCode = xpath.compile("statecode/text()");
			xpathCounty = xpath.compile("county/text()");
			xpathCity = xpath.compile("city/text()");
			xpathName = xpath.compile("name/text()");
			xpathHouse = xpath.compile("house/text()");
			xpathStreet = xpath.compile("street/text()");
			xpathUzip = xpath.compile("uzip/text()");
		} catch (XPathExpressionException xpee) {
			throw new RuntimeException("This should never happen", xpee);
		}
	}

	private final String xml;
	private Element element = null;

	public YahooLocation(Element result) {
		StringWriter swriter = new StringWriter();

		try {
			Transformer transformer = TRANSFORMER_FACTORY.newTransformer();
			transformer.transform(new DOMSource(result), new StreamResult(swriter));
		} catch (TransformerException te) {
			throw new RuntimeException("This should never happen");
		}

		this.xml = swriter.toString();
	}

	public Integer getQuality() {
		return this.getInteger(xpathQuality);
	}

	public Integer getRadiusInMeters() {
		return this.getInteger(xpathRadius);
	}

	@Override
	public Double getLatitudeInDegrees() {
		return this.getDouble(xpathLatitude);
	}

	@Override
	public Double getLongitudeInDegrees() {
		return this.getDouble(xpathLongitude);
	}

	@Override
	public Double getAltitudeInMeters() {
		throw new UnsupportedOperationException();
	}

	public String getCountry() {
		return this.getString(xpathCountry);
	}

	@Override
	public String getCountryCode() {
		return this.getString(xpathCountryCode);
	}

	public String getState() {
		return this.getString(xpathState);
	}

	@Override
	public String getStateCode() {
		return this.getString(xpathStateCode);
	}

	@Override
	public String getProvinceCode() {
		return this.getString(xpathStateCode);
	}

	@Override
	public String getCounty() {
		return this.getString(xpathCounty);
	}

	@Override
	public String getParish() {
		return this.getString(xpathCounty);
	}

	@Override
	public String getTownship() {
		throw new UnsupportedOperationException();
	}

	@Override
	public String getBorough() {
		throw new UnsupportedOperationException();
	}

	@Override
	public String getLocality() {
		String city = this.getString(xpathCity);
		if (city != null) return city;
		return this.getString(xpathName);
	}

	@Override
	public String getThoroughfare() {
		String house = this.getString(xpathHouse);
		String street = this.getString(xpathStreet);
		if (street == null) return null;
		else if (house == null) return street;
		else return house + " " + street;
	}

	@Override
	public String getPostalCodeFull() {
		return this.getString(xpathUzip);
	}

	@Override
	public String getPostalCode() {
		String zip = this.getPostalCodeFull();
		if (zip != null) zip = zip.substring(0, 5);
		return zip;
	}

	private Integer getInteger(XPathExpression xexpression) {
		String str = this.getString(xexpression);
		return str == null ? null : new Integer(str);
	}

	private Double getDouble(XPathExpression xexpression) {
		String str = this.getString(xexpression);
		return str == null ? null : new Double(str);
	}

	private String getString(XPathExpression xexpression) {
		try {
			String str = (String) xexpression.evaluate(this.getElement(), XPathConstants.STRING);
			return str == null || str.length() == 0 ? null : str;
		} catch (XPathExpressionException xpee) {
			throw new RuntimeException("This should never happen", xpee);
		}
	}

	private Element getElement() {
		if (this.element != null) return this.element;

		DOMResult dresult = new DOMResult();

		try {
			Transformer transformer = TRANSFORMER_FACTORY.newTransformer();
			transformer.transform(new StreamSource(new StringReader(this.xml)), dresult);
		} catch (TransformerException te) {
			throw new RuntimeException("This should never happen");
		}

		Document document = (Document) dresult.getNode();
		this.element = document.getDocumentElement();
		return this.element;
	}

}
