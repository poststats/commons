package com.brianlong.gis;

import com.brianlong.DeveloperException;
import com.brianlong.util.StringUtil;
import com.brianlong.xml.XmlFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class GoogleMapsLocation implements Location, USALocation, CanadaLocation {

	private static final long serialVersionUID = 1L;

	private String type = null;
	private Map<String, String> addressComponents = null;
	private Double latitude = null;
	private Double longitude = null;

	public GoogleMapsLocation(Element element) {
		this.type = this.findElementString(element, "type");
		this.findAddressComponents(element);
		this.findGeometry(element);
	}

	private void findAddressComponents(Element element) {
		XPath xpath = XmlFactory.newXPath();

		try {
			XPathExpression xpathType = xpath.compile("type");
			XPathExpression xpathShortName = xpath.compile("short_name");

			this.addressComponents = new HashMap<String, String>();
			List<Element> addressComponents = this.findElements(element, "address_component");
			for (Element addressComponent : addressComponents) {
				String shortName = (String) xpathShortName.evaluate(addressComponent, XPathConstants.STRING);
				NodeList types = (NodeList) xpathType.evaluate(addressComponent, XPathConstants.NODESET);
				for (int n = 0; n < types.getLength(); n++) {
					this.addressComponents.put(types.item(n)
							.getTextContent(), shortName);
				}
			}

			System.out.println(this.addressComponents);
		} catch (XPathExpressionException xpee) {
			throw new DeveloperException(xpee);
		}
	}

	private void findGeometry(Element element) {
		Element locationElement = this.findElement(element, "geometry/location");
		String latitude = this.findElementString(locationElement, "lat");
		String longitude = this.findElementString(locationElement, "lng");

		this.latitude = Double.parseDouble(latitude);
		this.longitude = Double.parseDouble(longitude);
	}

	public String getType() {
		return type;
	}

	@Override
	public Double getLatitudeInDegrees() {
		return this.latitude;
	}

	@Override
	public Double getLongitudeInDegrees() {
		return this.longitude;
	}

	@Override
	public Double getAltitudeInMeters() {
		throw new UnsupportedOperationException();
	}

	public String getCountry() {
		return this.getOne("country", "political");
	}

	@Override
	public String getCountryCode() {
		return this.getOne("country", "political");
	}

	public String getState() {
		return this.getOne("administrative_area_level_1");
	}

	@Override
	public String getStateCode() {
		return this.getOne("administrative_area_level_1");
	}

	@Override
	public String getProvinceCode() {
		return this.getOne("administrative_area_level_1");
	}

	@Override
	public String getCounty() {
		return this.getOne("administrative_area_level_2");
	}

	@Override
	public String getParish() {
		return this.getOne("administrative_area_level_2");
	}

	@Override
	public String getTownship() {
		return this.getOne("administrative_area_level_3");
	}

	@Override
	public String getBorough() {
		return this.getOne("locality", "neighborhood", "administrative_area_level_4", "postal_town");
	}

	@Override
	public String getLocality() {
		return this.getOne("locality", "neighborhood", "administrative_area_level_4", "postal_town", "administrative_area_level_3",
				"administrative_area_level_2");
	}

	@Override
	public String getThoroughfare() {
		StringBuilder street = new StringBuilder();
		if (this.addressComponents.containsKey("street_number")) street.append(this.addressComponents.get("street_number"));
		street.append(' ');
		if (this.addressComponents.containsKey("route")) street.append(this.addressComponents.get("route"));
		return StringUtil.getInstance()
				.trim(street.toString());
	}

	@Override
	public String getPostalCode() {
		return this.getOne("postal_code");
	}

	@Override
	public String getPostalCodeFull() {
		throw new UnsupportedOperationException();
	}

	private String findElementString(Element element, String query) {
		XPath xpath = XmlFactory.newXPath();
		try {
			element = (Element) xpath.evaluate(query, element, XPathConstants.NODE);
			return element == null ? null : element.getTextContent();
		} catch (XPathExpressionException xpee) {
			throw new DeveloperException(xpee);
		}
	}

	private Element findElement(Element element, String query) {
		XPath xpath = XmlFactory.newXPath();
		try {
			return (Element) xpath.evaluate(query, element, XPathConstants.NODE);
		} catch (XPathExpressionException xpee) {
			throw new DeveloperException(xpee);
		}
	}

	private List<Element> findElements(Element element, String query) {
		XPath xpath = XmlFactory.newXPath();
		try {
			NodeList elements = (NodeList) xpath.evaluate(query, element, XPathConstants.NODESET);

			ArrayList<Element> elementsList = new ArrayList<Element>(elements.getLength());
			for (int n = 0; n < elements.getLength(); n++)
				elementsList.add((Element) elements.item(n));
			return elementsList;
		} catch (XPathExpressionException xpee) {
			throw new DeveloperException(xpee);
		}
	}

	private String getOne(String... fields) {
		for (String field : fields) {
			String value = this.addressComponents.get(field);
			if (value != null) return value;
		}

		return null;
	}

}
