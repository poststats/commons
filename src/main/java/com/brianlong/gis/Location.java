package com.brianlong.gis;

import java.io.Serializable;

public interface Location extends Serializable {

	Double getLatitudeInDegrees();

	Double getLongitudeInDegrees();

	Double getAltitudeInMeters();

	String getCountryCode();

	String getLocality();

	String getThoroughfare();

	String getPostalCode();

	String getPostalCodeFull();

}
