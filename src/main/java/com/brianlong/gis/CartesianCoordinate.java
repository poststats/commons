package com.brianlong.gis;

import com.brianlong.math.Vector;

class CartesianCoordinate {

	private double x;
	private double y;
	private double z;

	public CartesianCoordinate(double xInMeters, double yInMeters, double zInMeters) {
		this.x = xInMeters;
		this.y = yInMeters;
		this.z = zInMeters;
	}

	public double getXInMeters() {
		return this.x;
	}

	public double getYInMeters() {
		return this.y;
	}

	public double getZInMeters() {
		return this.z;
	}

	public double computeDistance(CartesianCoordinate cart) {
		double dx = cart.x - this.x;
		double dy = cart.y - this.y;
		double dz = cart.z - this.z;
		return Math.sqrt(Math.pow(dx, 2.0) + Math.pow(dy, 2.0) + Math.pow(dz, 2.0));
	}

	public GeographicCoordinate toGeographicCoordinate() {
		// convert cartesian to spheric
		double x2 = Math.pow(this.x, 2.0);
		double y2 = Math.pow(this.y, 2.0);
		double radius = Math.sqrt(x2 + y2 + Math.pow(this.z, 2.0));
		double theta = Math.atan2(this.y, this.x);
		double phi = Math.atan2(Math.sqrt(x2 + y2), this.z);

		// convert spheric to geographic
		double latitude = 90.0 - Math.toDegrees(phi);
		double longitude = Math.toDegrees(theta);
		double altitude = radius - GeographicCoordinate.computeRadiusInMeters(latitude);

		return new GeographicCoordinate(latitude, longitude, altitude);
	}

	public Vector toVector() {
		return new Vector(this.x, this.y, this.z);
	}

	public String toString() {
		return "{ " + this.x + " , " + this.y + " , " + this.z + " }";
	}

}
