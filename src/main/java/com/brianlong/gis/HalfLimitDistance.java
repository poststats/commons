package com.brianlong.gis;

public class HalfLimitDistance implements GeographicDistance {

	private static final double PRECISION_DEGREES = 0.001;
	private static final HalfLimitDistance INSTANCE = new HalfLimitDistance();

	public static HalfLimitDistance getInstance() {
		return HalfLimitDistance.INSTANCE;
	}

	private HalfLimitDistance() {
	}

	public double computeInMeters(Location location1, Location location2) {
		GeographicCoordinate geo1 = new GeographicCoordinate(location1);
		GeographicCoordinate geo2 = new GeographicCoordinate(location2);
		return this.computeUsingLimitsInMeters(geo1, geo2, PRECISION_DEGREES);
	}

	private double computeUsingLimitsInMeters(GeographicCoordinate geo1, GeographicCoordinate geo2, double deltaDegrees) {
		CartesianCoordinate cart1 = geo1.toCartesianCoordinate();
		CartesianCoordinate cart2 = geo2.toCartesianCoordinate();

		if (Math.abs(geo1.getLatitudeInDegrees() - geo2.getLatitudeInDegrees()) < deltaDegrees
				&& Math.abs(geo1.getLongitudeInDegrees() - geo2.getLongitudeInDegrees()) < deltaDegrees)
			return cart1.computeDistance(cart2);

		CartesianCoordinate avgcart = new CartesianCoordinate((cart1.getXInMeters() + cart2.getXInMeters()) / 2.0,
				(cart1.getYInMeters() + cart2.getYInMeters()) / 2.0, (cart1.getZInMeters() + cart2.getZInMeters()) / 2.0);

		GeographicCoordinate geohalfway = avgcart.toGeographicCoordinate();
		geohalfway = new GeographicCoordinate(geohalfway.getLatitudeInDegrees(), geohalfway.getLongitudeInDegrees(),
				(geo1.getAltitudeInMeters() + geo2.getAltitudeInMeters()) / 2.0);

		return this.computeUsingLimitsInMeters(geo1, geohalfway, deltaDegrees) + this.computeUsingLimitsInMeters(geohalfway, geo2, deltaDegrees);
	}

}
