package com.brianlong.gis;

import com.brianlong.xml.XmlFactory;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class OpenStreetMapGeocoder implements Geocoder<OpenStreetMapLocation> {

	private static DocumentBuilder dbuilder;
	private static XPathExpression xpathResults;

	static {
		try {
			dbuilder = XmlFactory.newDocumentBuilder();
		} catch (ParserConfigurationException pce) {
			throw new RuntimeException("This should never happen", pce);
		}

		XPath xpath = XmlFactory.newXPath();
		try {
			xpathResults = xpath.compile("/searchresults/place");
		} catch (XPathExpressionException xpee) {
			throw new RuntimeException("This should never happen", xpee);
		}
	}

	private final URI baseUrl = URI.create("http://open.mapquestapi.com/nominatim/v1/search");
	private final String key;

	public OpenStreetMapGeocoder(String key) {
		this.key = key;
	}

	@Override
	public OpenStreetMapLocation find(String query) throws GeocodingException, IOException {
		List<OpenStreetMapLocation> locations = this.search(query, 1);
		return locations.isEmpty() ? null
				: locations.iterator()
						.next();
	}

	@Override
	public List<OpenStreetMapLocation> search(String query) throws GeocodingException, IOException {
		return this.search(query, Integer.MAX_VALUE);
	}

	public List<OpenStreetMapLocation> search(String query, int maxResults) throws GeocodingException, IOException {
		try {
			URI uri = new URIBuilder(this.baseUrl).addParameter("key", this.key)
					.addParameter("format", "xml")
					.addParameter("addressdetails", "1")
					.addParameter("limit", String.valueOf(maxResults))
					.addParameter("q", query)
					.build();

			HttpGet method = new HttpGet(uri);

			CloseableHttpClient client = HttpClientBuilder.create()
					.build();
			try {
				CloseableHttpResponse response = client.execute(method);
				if (response.getStatusLine()
						.getStatusCode() % 100 != 2)
					throw new GeocodingException("The OpenStreetMap Geocoder Web Service returned an error with code: " + response.getStatusLine()
							.getStatusCode());

				InputStream istream = response.getEntity()
						.getContent();
				try {
					Document xmldoc = dbuilder.parse(istream);
					NodeList results = (NodeList) xpathResults.evaluate(xmldoc, XPathConstants.NODESET);

					List<OpenStreetMapLocation> locations = new LinkedList<OpenStreetMapLocation>();
					for (int n = 0; n < results.getLength(); n++) {
						Element result = (Element) results.item(n);
						locations.add(new OpenStreetMapLocation(result));
						if (maxResults < locations.size()) break;
					}

					return locations;
				} finally {
					istream.close();
				}
			} finally {
				client.close();
			}
		} catch (URISyntaxException use) {
			throw new RuntimeException("This should never happen", use);
		} catch (XPathExpressionException xpee) {
			throw new RuntimeException("This should never happen", xpee);
		} catch (SAXException se) {
			throw new GeocodingException("The response from Yahoo! could not be parsed", se);
		}
	}

}
