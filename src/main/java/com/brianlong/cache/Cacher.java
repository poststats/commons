package com.brianlong.cache;

import com.brianlong.util.DirtyListener;
import com.brianlong.util.Listener;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Brian M Long
 */
public abstract class Cacher<ID, T> implements DirtyListener<ID> {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final UUID id = UUID.randomUUID();
	private final long expirationMilliseconds;

	public Cacher(long expirationMilliseconds, Reaper reaper) {
		this.expirationMilliseconds = expirationMilliseconds;
		reaper.addCache(this);
	}

	public UUID getId() {
		return this.id;
	}

	public abstract void add(ID id, T obj, boolean dirty) throws CacheException;

	public abstract boolean isCached(ID id);

	public abstract T get(ID id) throws CacheRetrievalException;

	public Map<ID, T> get(Collection<ID> ids) throws CacheRetrievalException {
		Map<ID, T> map = new LinkedHashMap<ID, T>(ids.size());
		for (ID id : ids) {
			T value = this.get(id);
			if (value != null) map.put(id, value);
		}

		return map;
	}

	public abstract T remove(ID id, boolean dirty) throws CacheException;

	public abstract void clear() throws CacheException;

	public abstract int size();

	public abstract void reap() throws CacheException;

	public abstract void addListener(Listener<ID> listener);

	protected long getExpirationMilliseconds() {
		return this.expirationMilliseconds;
	}

	@Override
	public void modified(ID id) {
		try {
			this.remove(id, false);
		} catch (CacheException ce) {
			this.logger.warn("A cache issue prevented a dirty object from being removed: " + ce.getMessage());
		}
	}

	@Override
	public void removed(ID id) {
		try {
			this.remove(id, false);
		} catch (CacheException ce) {
			this.logger.warn("A cache issue prevented a dirty object from being removed: " + ce.getMessage());
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Cacher)) return false;
		else return this.id.equals(((Cacher<?, ?>) obj).id);
	}

	@Override
	public int hashCode() {
		return this.id.hashCode();
	}

}
