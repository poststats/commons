package com.brianlong.cache;

import com.brianlong.util.Listener;
import com.brianlong.util.ModifyListener;
import com.brianlong.util.RemoveListener;
import com.hazelcast.config.ListenerConfig;
import com.hazelcast.config.TopicConfig;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.topic.ITopic;
import com.hazelcast.topic.MessageListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HazelcastCacheAdapter<T> implements ModifyListener<T>, RemoveListener<T> {

	private final Logger logger = LoggerFactory.getLogger(HazelcastCacheAdapter.class);
	private final HazelcastInstance hzInst;
	private final String modifyTopicId;
	private final String removeTopicId;
	private final HazelcastCacheListener<T> listener = new HazelcastCacheListener<T>();

	public HazelcastCacheAdapter(HazelcastInstance hzInst, String id) {
		this.hzInst = hzInst;
		this.modifyTopicId = "poststats.cache.invalidation.modify." + id;
		this.removeTopicId = "poststats.cache.invalidation.remove." + id;

		this.configureTopic(this.modifyTopicId, this.listener.createModifiedListener());
		this.configureTopic(this.removeTopicId, this.listener.createRemovedListener());
	}

	public void addListener(Listener<T> listener) {
		this.listener.addListener(listener);
	}

	@Override
	public void modified(T value) {
		if (HazelcastCacheAdapter.this.logger.isTraceEnabled()) HazelcastCacheAdapter.this.logger.trace("modified(" + value + ")");
		ITopic<T> topic = this.getTopic(this.modifyTopicId);
		topic.publish(value);
	}

	@Override
	public void removed(T value) {
		if (HazelcastCacheAdapter.this.logger.isTraceEnabled()) HazelcastCacheAdapter.this.logger.trace("removed(" + value + ")");
		ITopic<T> topic = this.getTopic(this.removeTopicId);
		topic.publish(value);
	}

	private void configureTopic(String topicId, MessageListener<T> listener) {
		TopicConfig topicConfig = this.hzInst.getConfig()
				.getTopicConfig(topicId);
		topicConfig.setGlobalOrderingEnabled(false);
		topicConfig.addMessageListenerConfig(new ListenerConfig(listener));
	}

	private ITopic<T> getTopic(String topicId) {
		return this.hzInst.getTopic(topicId);
	}

}
