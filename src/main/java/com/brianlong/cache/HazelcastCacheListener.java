package com.brianlong.cache;

import com.brianlong.util.Listener;
import com.brianlong.util.ModifyListener;
import com.brianlong.util.RemoveListener;
import com.hazelcast.topic.MessageListener;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HazelcastCacheListener<T> implements Serializable, ModifyListener<T>, RemoveListener<T> {

	private final Logger logger = LoggerFactory.getLogger(HazelcastCacheListener.class);

	private final List<ModifyListener<T>> modifyListeners = new LinkedList<ModifyListener<T>>();
	private final List<RemoveListener<T>> removeListeners = new LinkedList<RemoveListener<T>>();

	public void addListener(Listener<T> listener) {
		this.logger.trace("addListener({})", listener.getClass()
				.getName());
		if (listener instanceof RemoveListener) this.removeListeners.add((RemoveListener<T>) listener);
		if (listener instanceof ModifyListener) this.modifyListeners.add((ModifyListener<T>) listener);
	}

	MessageListener<T> createModifiedListener() {
		return new HazelcastModifiedMessageListener<T>(this);
	}

	MessageListener<T> createRemovedListener() {
		return new HazelcastRemovedMessageListener<T>(this);
	}

	public void modified(T value) {
		this.logger.trace("modifiedElsewhere({})", value);
		for (ModifyListener<T> listener : this.modifyListeners)
			listener.modified(value);
	}

	public void removed(T value) {
		this.logger.trace("removedElsewhere({})", value);
		for (RemoveListener<T> listener : this.removeListeners)
			listener.removed(value);
	}

}
