package com.brianlong.cache;

import com.brianlong.thread.PeriodicExecutor;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Reaper extends PeriodicExecutor implements Runnable {

	private final Logger logger = LoggerFactory.getLogger(Reaper.class);
	private final Set<Cacher<?, ?>> caches = new HashSet<Cacher<?, ?>>(3);

	public Reaper(long reaperMilliseconds) {
		super("reaper", reaperMilliseconds, reaperMilliseconds, TimeUnit.MILLISECONDS);
		this.execute(this);
	}

	public <ID, T> void addCache(Cacher<ID, T> cache) {
		if (this.logger.isTraceEnabled()) this.logger.trace("addCache(" + cache.getId() + ")");
		this.caches.add(cache);
	}

	public boolean hasCaches() {
		return !this.caches.isEmpty();
	}

	public <ID, T> void removeCache(Cacher<ID, T> cache) {
		if (this.logger.isTraceEnabled()) this.logger.trace("removeCache(" + cache.getId() + ")");
		this.caches.remove(cache);
	}

	public void run() {
		if (this.logger.isTraceEnabled()) this.logger.trace("run()");
		try {
			for (Cacher<?, ?> cache : this.caches)
				cache.reap();
		} catch (RuntimeException re) {
			throw re;
		} catch (Exception e) {
			this.logger.error("Caching reaper experienced an unexpected issue: " + e.getMessage(), e);
		}
	}

}
