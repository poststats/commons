package com.brianlong.cache;

import com.brianlong.io.ChunkedInputStream;
import com.brianlong.util.Listener;
import com.brianlong.util.ModifyListener;
import com.brianlong.util.RemoveListener;
import com.brianlong.util.SoftHashMap;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Brian M Long
 */
public class FileCacher<ID> extends Cacher<ID, byte[]> {

	private SoftHashMap<ID, CacheObject> cache = new SoftHashMap<ID, CacheObject>();
	private final List<ModifyListener<ID>> modifyListeners = new LinkedList<ModifyListener<ID>>();
	private final List<RemoveListener<ID>> removeListeners = new LinkedList<RemoveListener<ID>>();
	private File cacheDirectory = null;

	public FileCacher(long expirationMilliseconds, Reaper reaper) {
		super(expirationMilliseconds, reaper);
	}

	public FileCacher(File directory, long expirationMilliseconds, Reaper reaper) {
		super(expirationMilliseconds, reaper);

		this.cacheDirectory = directory;
	}

	@Override
	public void addListener(Listener<ID> listener) {
		if (listener instanceof ModifyListener) this.modifyListeners.add((ModifyListener<ID>) listener);
		if (listener instanceof RemoveListener) this.removeListeners.add((RemoveListener<ID>) listener);
	}

	@Override
	public void add(ID id, byte[] bytes, boolean dirty) throws CacheException {
		try {
			CacheObject co = new CacheObject(bytes, this.cacheDirectory, System.currentTimeMillis() + this.getExpirationMilliseconds());

			synchronized (this.cache) {
				this.cache.put(id, co);
			}

			if (dirty) for (ModifyListener<ID> listener : this.modifyListeners)
				listener.modified(id);
		} catch (IOException ie) {
			throw new CacheException(ie.getMessage(), ie);
		}
	}

	public void add(ID id, InputStream istream, boolean dirty) throws CacheException {
		try {
			CacheObject co = new CacheObject(istream, this.cacheDirectory, System.currentTimeMillis() + this.getExpirationMilliseconds());

			synchronized (this.cache) {
				this.cache.put(id, co);
			}

			if (dirty) for (ModifyListener<ID> listener : this.modifyListeners)
				listener.modified(id);
		} catch (IOException ie) {
			throw new CacheException(ie.getMessage(), ie);
		}
	}

	@Override
	public boolean isCached(ID id) {
		synchronized (this.cache) {
			return this.cache.containsKey(id);
		}
	}

	@Override
	public byte[] get(ID id) throws CacheRetrievalException {
		ByteArrayOutputStream baostream = new ByteArrayOutputStream();
		this.get(id, baostream);
		return baostream.toByteArray();
	}

	@SuppressWarnings("resource")
	public long get(ID id, OutputStream ostream) throws CacheRetrievalException {
		CacheObject co = null;

		synchronized (this.cache) {
			co = this.cache.get(id);
			if (co != null) co.expirationMilliseconds = System.currentTimeMillis() + this.getExpirationMilliseconds();
		}

		if (co == null) return 0;
		try {
			return new ChunkedInputStream(new FileInputStream(co.file)).readFully(4096, ostream);
		} catch (IOException ie) {
			throw new CacheRetrievalException(ie.getMessage(), ie);
		}
	}

	@Override
	public byte[] remove(ID id, boolean dirty) throws CacheException {
		ByteArrayOutputStream baostream = new ByteArrayOutputStream();
		this.remove(id, baostream, dirty);
		return baostream.toByteArray();
	}

	@SuppressWarnings("resource")
	public long remove(ID id, OutputStream ostream, boolean dirty) throws CacheException {
		CacheObject co = null;

		synchronized (this.cache) {
			co = this.cache.remove(id);
		}

		if (dirty) for (RemoveListener<ID> listener : this.removeListeners)
			listener.removed(id);

		if (co == null) return 0;
		try {
			return new ChunkedInputStream(new FileInputStream(co.file)).readFully(4096, ostream);
		} catch (IOException ie) {
			throw new CacheRetrievalException(ie.getMessage(), ie);
		}
	}

	@Override
	public void clear() throws CacheException {
		synchronized (this.cache) {
			this.cache.clear();
		}
	}

	@Override
	public int size() {
		synchronized (this.cache) {
			return this.cache.size();
		}
	}

	@Override
	public void reap() throws CacheException {
		long nowMilliseconds = System.currentTimeMillis();

		if (this.cache == null) // fixes threading issue
			return;
		synchronized (this.cache) {
			for (Iterator<ID> ids = this.cache.keySet()
					.iterator(); ids.hasNext();) {
				ID id = ids.next();
				CacheObject co = (CacheObject) this.cache.get(id);
				if (nowMilliseconds > co.expirationMilliseconds) {
					ids.remove(); // hopefully this removes the mapping and not just the key
				}
			}
		}
	}

	private class CacheObject {

		public File file;
		public long expirationMilliseconds;

		public CacheObject(byte[] bytes, File cacheDirectory, long expirationMilliseconds) throws IOException {
			this.file = File.createTempFile("fc-", "-cache", cacheDirectory);
			this.expirationMilliseconds = expirationMilliseconds;

			FileOutputStream fostream = new FileOutputStream(file);
			try {
				fostream.write(bytes);
			} finally {
				fostream.close();
			}
		}

		public CacheObject(InputStream istream, File cacheDirectory, long expirationMilliseconds) throws IOException {
			this.file = File.createTempFile("fc-", "-cache", cacheDirectory);
			this.expirationMilliseconds = expirationMilliseconds;

			FileOutputStream fostream = new FileOutputStream(file);
			try {
				ChunkedInputStream cistream = new ChunkedInputStream(istream);
				try {
					cistream.readFully(4096, fostream);
				} finally {
					cistream.close();
				}
			} finally {
				fostream.close();
			}
		}

		@Override
		protected void finalize() throws Throwable {
			try {
				super.finalize();
			} finally {
				this.file.delete();
			}
		}

		@Override
		public boolean equals(Object obj) {
			return this.file.equals(obj);
		}

		@Override
		public int hashCode() {
			return this.file.hashCode();
		}

	}

}
