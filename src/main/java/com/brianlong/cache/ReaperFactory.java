package com.brianlong.cache;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class ReaperFactory {

	private static Map<Long, Reaper> reapers = new HashMap<Long, Reaper>(50);

	public static Reaper getDefaultInstance() {
		return getInstance(5000L);
	}

	public static Reaper getInstance(long reaperIntervalMilliseconds) {
		synchronized (reapers) {
			if (!reapers.containsKey(reaperIntervalMilliseconds)) {
				Reaper reaper = new Reaper(reaperIntervalMilliseconds);
				reapers.put(reaperIntervalMilliseconds, reaper);
			}
		}

		return reapers.get(reaperIntervalMilliseconds);
	}

	public static void shutdownInstances() {
		synchronized (reapers) {
			for (Entry<Long, Reaper> reaper : reapers.entrySet())
				reaper.getValue()
						.shutdownNow();
			reapers.clear();
		}
	}

}
