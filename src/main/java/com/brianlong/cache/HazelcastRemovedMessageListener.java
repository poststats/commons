package com.brianlong.cache;

import com.brianlong.util.RemoveListener;
import com.hazelcast.topic.Message;
import com.hazelcast.topic.MessageListener;
import java.io.Serializable;

public class HazelcastRemovedMessageListener<T> implements MessageListener<T>, Serializable {

	private final RemoveListener<T> listener;

	public HazelcastRemovedMessageListener(RemoveListener<T> listener) {
		this.listener = listener;
	}

	@Override
	public void onMessage(Message<T> message) {
		if (!message.getPublishingMember()
				.localMember())
			this.listener.removed(message.getMessageObject());
	}

}
