package com.brianlong.cache;

import com.hazelcast.core.HazelcastInstance;

public abstract class ClusterAwareMemoryCacher<ID, T> extends MemoryCacher<ID, T> {

	public ClusterAwareMemoryCacher(long expirationMilliseconds, HazelcastInstance hzInstance) {
		this(expirationMilliseconds, ReaperFactory.getDefaultInstance(), hzInstance);
	}

	public ClusterAwareMemoryCacher(long expirationMilliseconds, Reaper reaper, HazelcastInstance hzInstance) {
		super(expirationMilliseconds, reaper);

		if (hzInstance != null) {
			HazelcastCacheAdapter<ID> adapter = new HazelcastCacheAdapter<ID>(hzInstance, this.getClass()
					.getName());
			this.addListener(adapter);
			adapter.addListener(this);
		}
	}

}
