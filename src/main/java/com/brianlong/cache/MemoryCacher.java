package com.brianlong.cache;

import com.brianlong.util.Listener;
import com.brianlong.util.ModifyListener;
import com.brianlong.util.RemoveListener;
import com.brianlong.util.SoftHashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class implements a simple yet powerful caching mechanism.
 *
 * The cacher is simply a map of ids to cached values. The cached values remain
 * in the cache until they are reaped. Cached values will only be reaped after
 * they have expired or JVM memory constraints are reached.
 *
 * The time in the cache starts from the time the value was last accessed. From
 * then, it will remain in the cache anywhere from the expiration time to the
 * expiration time + reap time.
 *
 * A separate reaper thread is used to clean up expired cache objects. When this
 * class is finalized, the reaper thread is automatically stopped.
 *
 * This class is thread-safe.
 *
 * @author Brian M Long
 */
public class MemoryCacher<ID, T> extends Cacher<ID, T> {

	private final Logger logger = LoggerFactory.getLogger(MemoryCacher.class);
	private final SoftHashMap<ID, CacheObject> cache = new SoftHashMap<ID, CacheObject>();
	private final List<ModifyListener<ID>> modifyListeners = new LinkedList<ModifyListener<ID>>();
	private final List<RemoveListener<ID>> removeListeners = new LinkedList<RemoveListener<ID>>();

	public MemoryCacher(long expirationMilliseconds, Reaper reaper) {
		super(expirationMilliseconds, reaper);
	}

	@Override
	public void addListener(Listener<ID> listener) {
		if (listener instanceof ModifyListener) this.modifyListeners.add((ModifyListener<ID>) listener);
		if (listener instanceof RemoveListener) this.removeListeners.add((RemoveListener<ID>) listener);
	}

	@Override
	public void add(ID id, T obj, boolean dirty) throws CacheException {
		if (this.logger.isTraceEnabled()) this.logger.trace("add(" + id + ", " + obj + ", " + dirty + ")");
		CacheObject co = new CacheObject(obj, System.currentTimeMillis() + this.getExpirationMilliseconds());

		synchronized (this.cache) {
			this.cache.put(id, co);
		}

		if (dirty) for (ModifyListener<ID> listener : this.modifyListeners)
			listener.modified(id);
	}

	@Override
	public boolean isCached(ID id) {
		synchronized (this.cache) {
			return this.cache.containsKey(id);
		}
	}

	@Override
	public T get(ID id) throws CacheRetrievalException {
		CacheObject co = null;

		synchronized (this.cache) {
			co = this.cache.get(id);
			if (co != null) co.expirationMilliseconds = System.currentTimeMillis() + this.getExpirationMilliseconds();
		}

		return co == null ? null : co.obj;
	}

	@Override
	public T remove(ID id, boolean dirty) throws CacheException {
		if (this.logger.isTraceEnabled()) this.logger.trace("remove(" + id + ", " + dirty + ")");
		CacheObject co = null;

		synchronized (this.cache) {
			co = this.cache.remove(id);
		}

		if (dirty) for (RemoveListener<ID> listener : this.removeListeners)
			listener.removed(id);

		return co == null ? null : co.obj;
	}

	@Override
	public void clear() throws CacheException {
		synchronized (this.cache) {
			this.cache.clear();
		}
	}

	@Override
	public int size() {
		synchronized (this.cache) {
			return this.cache.size();
		}
	}

	@Override
	public void reap() throws CacheException {
		if (this.logger.isTraceEnabled()) this.logger.trace("reap()");
		long nowMilliseconds = System.currentTimeMillis();

		if (this.cache == null) // threading issue
			return;
		synchronized (this.cache) {
			for (Iterator<ID> ids = this.cache.keySet()
					.iterator(); ids.hasNext();) {
				ID id = ids.next();
				CacheObject co = (CacheObject) this.cache.get(id);
				if (nowMilliseconds > co.expirationMilliseconds) {
					if (this.logger.isDebugEnabled()) this.logger.debug("reap(): expired " + id);
					ids.remove(); // hopefully this removes the mapping and not just the key
				}
			}
		}
	}

	private class CacheObject {

		public T obj;
		public long expirationMilliseconds;

		public CacheObject(T obj, long expirationMilliseconds) {
			this.obj = obj;
			this.expirationMilliseconds = expirationMilliseconds;
		}

		@Override
		public boolean equals(Object obj) {
			return this.obj.equals(obj);
		}

		@Override
		public int hashCode() {
			return this.obj.hashCode();
		}

	}

}
