package com.brianlong.cache;

import com.brianlong.util.ModifyListener;
import com.hazelcast.topic.Message;
import com.hazelcast.topic.MessageListener;
import java.io.Serializable;

public class HazelcastModifiedMessageListener<T> implements MessageListener<T>, Serializable {

	private final ModifyListener<T> listener;

	public HazelcastModifiedMessageListener(ModifyListener<T> listener) {
		this.listener = listener;
	}

	@Override
	public void onMessage(Message<T> message) {
		if (!message.getPublishingMember()
				.localMember())
			this.listener.modified(message.getMessageObject());
	}

}
