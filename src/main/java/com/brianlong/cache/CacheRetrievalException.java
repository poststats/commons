package com.brianlong.cache;

public class CacheRetrievalException extends CacheException {

	private static final long serialVersionUID = 1L;

	public CacheRetrievalException(String message) {
		super(message);
	}

	public CacheRetrievalException(String message, Throwable t) {
		super(message, t);
	}

}
