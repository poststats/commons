package com.brianlong.thread;

public class TimeoutEnforcer implements Runnable {

	private final Timeoutable timeoutable;
	private final long timeout;

	public TimeoutEnforcer(Timeoutable timeoutable, long timeout) {
		this.timeoutable = timeoutable;
		this.timeout = timeout;
	}

	public void run() {
		try {
			Thread.sleep(this.timeout);
			this.timeoutable.timeout();
		} catch (InterruptedException ie) {
			Thread.currentThread()
					.interrupt();
		}
	}

	public interface Timeoutable {
		void timeout();
	}

}
