package com.brianlong.thread;

/**
 * This class implements a semaphore that makes "write" executions mutually
 * exclusive while making "read" executions fully threaded.
 *
 * In short, a semaphore is an object that controls thread throughput. A
 * "synchronized" block in Java is a semaphore. The block only allows 1 thread
 * to execute over the block at one time.
 *
 * A read/white semaphore will either give full threaded access to a resource,
 * except when a "write" is required. The acquisition of the semaphore for
 * writing will block all other threads.
 *
 * It is essential that all calls to "acquireX" are followed up by a call to
 * "releaseX" UNDER ALL CIRCUMSTANCES. The developer can guarantee this by using
 * "try..finally" after every "acquire" with "release" being the first executed
 * line in the "finally" block.
 *
 * @author Brian M Long
 */
public class ReadWriteSemaphore {

	private int readyReadThreads = 0;
	private int executingReadThreads = 0;
	private int readyWriteThreads = 0;
	private int executingWriteThreads = 0;
	private Object writeBlock = new Long(System.currentTimeMillis());

	/**
	 * This method acquires a read unit of this semaphore.
	 *
	 * This method counts up the number of threads simultaneously executing with
	 * read authority under the governance of this semaphore. There can be an
	 * unlimited number of reads at any one time. If there is already a thread
	 * executing with write authority, then the calling thread will block/wait until
	 * the write has been released.
	 *
	 * If there are 1 or more threads waiting to take hold of the semaphore, the
	 * read thread will block/wait until the write thread is acquired and later
	 * released. This implements more of a FIFO algorithm to force heavy-read
	 * systems to perform the writes.
	 *
	 * @return true if acquired; false if not acquired (interrupted)
	 */
	public boolean acquireRead() {
		try {
			synchronized (this) {
				while (this.readyWriteThreads > 0 || this.executingWriteThreads > 0) {
					this.readyReadThreads = this.increment(this.readyReadThreads);
					try {
						this.wait();
					} finally {
						this.readyReadThreads = this.decrement(this.readyReadThreads);
					}
				}
				this.executingReadThreads = this.increment(this.executingReadThreads);
			}

			return true;
		} catch (InterruptedException ie) {
			return false;
		}
	}

	/**
	 * This method acquires a read unit of this semaphore, but will only wait the
	 * specified time for that unit.
	 *
	 * This method counts up the number of threads simultaneously executing with
	 * read authority under the governance of this semaphore. There can be an
	 * unlimited number of reads at any one time. If there is already a thread
	 * executing with write authority, then the calling thread will block/wait until
	 * the write has been released or the specified time expires.
	 *
	 * If there are 1 or more threads waiting to take hold of the semaphore, the
	 * read thread will block/wait until the write thread is acquired and later
	 * released. This implements more of a FIFO algorithm to force heavy-read
	 * systems to perform the writes.
	 *
	 * @param millis The maximum number of milliseconds for the calling thread to
	 *               block/wait for a release.
	 * @return true if acquired; false if not acquired (timeout or interrupted)
	 */
	public boolean acquireRead(long millis) {
		try {
			synchronized (this) {
				if (this.readyWriteThreads > 0 || this.executingWriteThreads > 0) {
					this.readyReadThreads = this.increment(this.readyReadThreads);
					try {
						this.wait(millis);
					} finally {
						this.readyReadThreads = this.decrement(this.readyReadThreads);
					}

					if (this.readyWriteThreads > 0 || this.executingWriteThreads > 0) return false;
				}
				this.executingReadThreads++;
			}

			return true;
		} catch (InterruptedException ie) {
			return false;
		}
	}

	/**
	 * This method acquires a read unit of this semaphore, but will only wait the
	 * specified time for that unit.
	 *
	 * This method counts up the number of threads simultaneously executing with
	 * read authority under the governance of this semaphore. There can be an
	 * unlimited number of reads at any one time. If there is already a thread
	 * executing with write authority, then the calling thread will block/wait until
	 * the write has been released or the specified time expires.
	 *
	 * If there are 1 or more threads waiting to take hold of the semaphore, the
	 * read thread will block/wait until the write thread is acquired and later
	 * released. This implements more of a FIFO algorithm to force heavy-read
	 * systems to perform the writes.
	 *
	 * @param millis The maximum number of milliseconds for the calling thread to
	 *               block/wait for a release.
	 * @param nanos  A number of seconds measured in nanoseconds (1000 nanos per
	 *               millisecond).
	 * @return true if acquired; false if not acquired (timeout or interrupted)
	 */
	public boolean acquireRead(long millis, int nanos) {
		try {
			synchronized (this) {
				if (this.readyWriteThreads > 0 || this.executingWriteThreads > 0) {
					this.readyReadThreads = this.increment(this.readyReadThreads);
					try {
						this.wait(millis, nanos);
					} finally {
						this.readyReadThreads = this.decrement(this.readyReadThreads);
					}

					if (this.readyWriteThreads > 0 || this.executingWriteThreads > 0) return false;
				}
				this.executingReadThreads++;
			}

			return true;
		} catch (InterruptedException ie) {
			return false;
		}
	}

	public boolean acquireWrite() {
		try {
			synchronized (this) {
				while (this.executingReadThreads > 0 || this.readyWriteThreads > 0 || this.executingWriteThreads > 0) {
					this.readyWriteThreads = this.increment(this.readyWriteThreads);
					try {
						synchronized (this.writeBlock) {
							this.wait();
						}
					} finally {
						this.readyWriteThreads = this.decrement(this.readyWriteThreads);
					}
				}
				this.executingWriteThreads++;
			}

			return true;
		} catch (InterruptedException ie) {
			return false;
		}
	}

	public boolean acquireWrite(long millis) {
		try {
			synchronized (this) {
				if (this.executingReadThreads > 0 || this.readyWriteThreads > 0 || this.executingWriteThreads > 0) {
					this.readyWriteThreads = this.increment(this.readyWriteThreads);
					try {
						synchronized (this.writeBlock) {
							this.wait(millis);
						}
					} finally {
						this.readyWriteThreads = this.decrement(this.readyWriteThreads);
					}

					if (this.executingReadThreads > 0 || this.readyWriteThreads > 0 || this.executingWriteThreads > 0) return false;
				}
				this.executingWriteThreads++;
			}

			return true;
		} catch (InterruptedException ie) {
			return false;
		}
	}

	public boolean acquireWrite(long millis, int nanos) {
		try {
			synchronized (this) {
				if (this.executingReadThreads > 0 || this.readyWriteThreads > 0 || this.executingWriteThreads > 0) {
					this.readyWriteThreads = this.increment(this.readyWriteThreads);
					try {
						synchronized (this.writeBlock) {
							this.wait(millis, nanos);
						}
					} finally {
						this.readyWriteThreads = this.decrement(this.readyWriteThreads);
					}

					if (this.executingReadThreads > 0 || this.readyWriteThreads > 0 || this.executingWriteThreads > 0) return false;
				}
				this.executingWriteThreads++;
			}

			return true;
		} catch (InterruptedException ie) {
			return false;
		}
	}

	public void releaseRead() {
		synchronized (this) {
			this.executingReadThreads = this.decrement(this.executingReadThreads);

			if (this.readyWriteThreads > 0 && this.executingReadThreads == 0) {
				synchronized (this.writeBlock) {
					this.notify();
				}
			}
		}
	}

	public void releaseWrite() {
		synchronized (this) {
			this.executingWriteThreads = this.decrement(this.executingWriteThreads);

			if (this.readyWriteThreads > 0) {
				synchronized (this.writeBlock) {
					this.notify();
				}
			} else if (this.readyReadThreads > 0) {
				this.notifyAll();
			}
		}
	}

	/**
	 * This method is for UNIT TESTING ONLY.
	 */
	int getAcquiredReadThreads() {
		synchronized (this) {
			return this.executingReadThreads;
		}
	}

	/**
	 * This method is for UNIT TESTING ONLY.
	 */
	int getAcquiredWriteThreads() {
		synchronized (this) {
			synchronized (this.writeBlock) {
				return this.executingWriteThreads;
			}
		}
	}

	private int decrement(int num) {
		return Math.max(0, num - 1);
	}

	private int increment(int num) {
		return num + 1;
	}

}
