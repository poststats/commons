package com.brianlong.thread;

import java.lang.reflect.Method;

/**
 * This class makes it easier to invoke any method using it's own thread.
 *
 * For instance: Thread thread = new MethodForker(string, "indexOf", new Class[]
 * {String.class}, new Object[] {"hello"}).fork(); thread.join();
 *
 * @author Brian M Long
 */
public class MethodForker implements Runnable {

	private Object instance;
	private Method method;
	private Object[] parameters;
	private Thread thread = null;
	private Object returnValue = null;
	private ReturnCallback callback = null;

	public MethodForker(Object instance, String methodName) throws NoSuchMethodException {
		this(instance, methodName, new Class[0], new Object[0]);
	}

	public MethodForker(Object instance, String methodName, Class<?>[] parameterTypes, Object[] parameters) throws NoSuchMethodException {
		this(instance, instance.getClass()
				.getDeclaredMethod(methodName, parameterTypes), parameters);
	}

	public MethodForker(Object instance, Method method) {
		this(instance, method, new Object[0]);
	}

	public MethodForker(Object instance, Method method, Object[] parameters) {
		this.instance = instance;
		this.method = method;
		this.parameters = parameters;
	}

	public Thread fork() {
		this.thread = new Thread(this);
		this.thread.start();

		if (this.callback != null) {
			try {
				new MethodForker(this, "doCallback").fork();
			} catch (NoSuchMethodException nsme) { // this should never happen
				throw new RuntimeException(nsme.getMessage());
			}
		}

		return this.thread;
	}

	public void interrupt() {
		if (this.thread == null) throw new IllegalStateException("You must first fork a thread before interrupting it");
		this.thread.interrupt();
	}

	public void waitForCompletion() throws InterruptedException {
		if (this.thread == null) throw new IllegalStateException("You must first fork a thread before waiting for it to return");
		this.thread.join();
		this.thread = null;
	}

	public Object waitForReturn() throws InterruptedException {
		this.waitForCompletion();
		return this.returnValue;
	}

	public void registerCallback(ReturnCallback callback) {
		this.callback = callback;
		if (this.thread != null) {
			try {
				new MethodForker(this, "doCallback").fork();
			} catch (NoSuchMethodException nsme) { // this should never happen
				throw new RuntimeException(nsme.getMessage());
			}
		}
	}

	@SuppressWarnings("unused")
	private void doCallback() throws InterruptedException {
		if (this.callback == null) throw new IllegalStateException("You must first register a callback before performing a callback");
		this.callback.methodReturned(this.waitForReturn());
	}

	public void run() {
		try {
			this.returnValue = this.method.invoke(this.instance, this.parameters);
		} catch (Exception e) {
			Thread thisThread = Thread.currentThread();
			thisThread.getUncaughtExceptionHandler()
					.uncaughtException(thisThread, e);
		}
	}

	public interface ReturnCallback {
		void methodReturned(Object returnValue);
	}

}
