package com.brianlong.thread;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Brian M Long
 */
public class MappedSemaphore<T> {

	private Set<T> locks = new HashSet<T>();

	/**
	 * @return true if acquired; false if not acquired
	 */
	public boolean acquireNow(T key) {
		synchronized (this) {
			if (this.locks.contains(key)) return false;

			this.locks.add(key);
			return true;
		}
	}

	/**
	 * @return true if acquired; false if not acquired (interrupted)
	 */
	public boolean acquire(T key) {
		try {
			synchronized (this) {
				while (this.locks.contains(key))
					this.wait();
				this.locks.add(key);
			}

			return true;
		} catch (InterruptedException ie) {
			return false;
		}
	}

	/**
	 * @param millis The maximum number of milliseconds for the calling thread to
	 *               block/wait for a release.
	 * @return true if acquired; false if not acquired (timeout or interrupted)
	 */
	public boolean acquire(T key, long millis) {
		try {
			synchronized (this) {
				while (this.locks.contains(key)) {
					long timeoutMillis = System.currentTimeMillis() + millis;
					this.wait(millis);
					if (System.currentTimeMillis() >= timeoutMillis) return false;
				}

				this.locks.add(key);
			}

			return true;
		} catch (InterruptedException ie) {
			return false;
		}
	}

	public void release(String key) {
		synchronized (this) {
			this.locks.remove(key);
			this.notifyAll();
		}
	}
}
