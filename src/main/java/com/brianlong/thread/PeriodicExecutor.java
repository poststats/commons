package com.brianlong.thread;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PeriodicExecutor {

	private final Logger logger = LoggerFactory.getLogger(PeriodicExecutor.class);
	private final String id;
	private final long delayBeforeFirstRun;
	private final long delayInterval;
	private Thread thread = null;
	private boolean running = false;

	public PeriodicExecutor(String id, long delayBeforeFirstRun, long delayInterval, TimeUnit unit) {
		this.id = id;
		this.delayBeforeFirstRun = unit.toMillis(delayBeforeFirstRun);
		this.delayInterval = unit.toMillis(delayInterval);
	}

	public String getId() {
		return this.id;
	}

	public boolean isTerminated() {
		return this.thread == null || !this.thread.isAlive();
	}

	public boolean isShutdown() {
		return !this.running;
	}

	public void shutdown() {
		if (this.thread != null && this.thread.isAlive()) this.running = false;
	}

	public List<Runnable> shutdownNow() {
		if (this.thread != null && this.thread.isAlive()) {
			this.running = false;
			this.thread.interrupt();
		}
		return Collections.emptyList();
	}

	public boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException {
		if (this.thread != null && this.thread.isAlive()) {
			long expireTime = System.currentTimeMillis() + unit.toMillis(timeout);
			this.thread.join(unit.toMillis(timeout));
			if (System.currentTimeMillis() >= expireTime) return false;
		}

		return true;
	}

	public void execute(final Runnable command) {
		if (this.logger.isTraceEnabled()) this.logger.trace("execute(): " + this.id);

		this.running = true;

		this.thread = new Thread(new Runnable() {
			@Override
			public void run() {
				PeriodicExecutor.this.executeThreaded(command);
			}
		}, this.id + "-periodic");

		this.thread.setDaemon(true);
		this.thread.start();

	}

	private void executeThreaded(Runnable command) {
		if (this.logger.isTraceEnabled()) this.logger.trace("executeThreaded(): " + this.id);

		try {
			if (this.delayBeforeFirstRun > 0L) Thread.sleep(this.delayBeforeFirstRun);

			while (!this.isShutdown()) {
				if (this.logger.isDebugEnabled()) this.logger.debug("executeThreaded(): periodic run '" + this.id + "' starting ...");
				try {
					command.run();
					if (this.logger.isDebugEnabled()) this.logger.debug("executeThreaded(): periodic run '" + this.id + "' completed");
				} catch (RuntimeException re) {
					this.logger.warn("Periodic exeuction '" + this.id + "' ran into an unexpected issue: " + re.getMessage(), re);
				} catch (Error e) {
					this.logger.warn("Periodic execution '" + this.id + "' ran into an unexpected issue: " + e.getMessage(), e);
				}
				Thread.sleep(this.delayInterval);
			}

			if (this.logger.isDebugEnabled()) this.logger.debug("executeThreaded(): periodic run '" + this.id + "' stopped gracefully");
		} catch (InterruptedException ie) {
			this.logger.warn("Periodic run '" + PeriodicExecutor.this.id + "' terminated");
			Thread.currentThread()
					.interrupt();
		}
	}

}
