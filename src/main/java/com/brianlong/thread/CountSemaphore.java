package com.brianlong.thread;

/**
 * @author Brian M Long
 */
public class CountSemaphore {

	private final long maxCount;
	private final Object lock;
	private long count = 0;

	public CountSemaphore(long maxCount) {
		if (maxCount < 1) throw new IllegalArgumentException("The 'maxCount' parameter must be greater than 0");
		this.maxCount = maxCount;
		this.lock = this;
	}

	public CountSemaphore(long maxCount, Object lock) {
		if (maxCount < 1) throw new IllegalArgumentException("The 'maxCount' parameter must be greater than 0");
		this.maxCount = maxCount;
		this.lock = lock;
	}

	public boolean acquireNow(long count) {
		synchronized (this.lock) {
			if (this.count + count >= this.maxCount) return false;

			this.count += count;
			return true;
		}
	}

	public boolean acquire(long count) {
		try {
			synchronized (this.lock) {
				while (this.count + count >= this.maxCount)
					this.lock.wait();
				this.count += count;
			}

			return true;
		} catch (InterruptedException ie) {
			return false;
		}
	}

	public void release(long count) {
		synchronized (this.lock) {
			this.count = Math.max(0, this.count - count);
			this.lock.notifyAll();
		}
	}

	/**
	 * This method is for UNIT TESTING ONLY.
	 */
	long getAcquiredCount() {
		return this.count;
	}
}
