package com.brianlong.thread;

/**
 * This class implements a semaphore that restricts the load on an arbitrary
 * resource.
 *
 * In short, a semaphore is an object that controls thread throughput. A
 * "synchronized" block in Java is a semaphore. The block only allows 1 thread
 * to execute over the block at one time. This can be considered a resource
 * semaphore of size "1"; "1" for how many threads can execute at the same time.
 *
 * A resource semaphore allows the developer to create "synchronized" blocks
 * that allow any number of threads to execute at the same time. This is great
 * when you want to minimize the load going through a resource contrained point
 * in the system.
 *
 * It is essential that all calls to "acquire" are followed up by a call to
 * "release" UNDER ALL CIRCUMSTANCES. The developer can guarantee this by using
 * "try..finally" after every "acquire" with "release" being the first executed
 * line in the "finally" block.
 *
 * @author Brian M Long
 */
public class ResourceSemaphore {

	private final int maxThreads;
	private final Object lock;
	private int threads = 0;

	/**
	 * This constructor instantiates a resource semaphore of size "maxThreads".
	 *
	 * @param maxThreads The maximum number of threads successfully returned from
	 *                   "acquire", but not yet returned from "release".
	 */
	public ResourceSemaphore(int maxThreads) {
		if (maxThreads < 1) throw new IllegalArgumentException("The 'maxThreads' parameter must be greater than 0");
		this.maxThreads = maxThreads;
		this.lock = this;
	}

	/**
	 * This constructor instantiates a resource semaphore of size "maxThreads" and
	 * synchronized over the specified object.
	 *
	 * @param maxThreads The maximum number of threads successfully returned from
	 *                   "acquire", but not yet returned from "release".
	 */
	public ResourceSemaphore(int maxThreads, Object lock) {
		if (maxThreads < 1) throw new IllegalArgumentException("The 'maxThreads' parameter must be greater than 0");
		this.maxThreads = maxThreads;
		this.lock = lock;
	}

	/**
	 * This method acquires a unit of this semaphore.
	 *
	 * This method counts up the number of threads simultaneously executing under
	 * the governance of this semaphore. If there are already too many threads
	 * executing under the governance, then the method will immediately return
	 * false.
	 *
	 * @return true if acquired; false if not acquired
	 */
	public boolean acquireNow() {
		synchronized (this.lock) {
			if (this.threads >= this.maxThreads) return false;

			this.threads++;
			return true;
		}
	}

	/**
	 * This method acquires a unit of this semaphore.
	 *
	 * This method counts up the number of threads simultaneously executing under
	 * the governance of this semaphore. If there are already too many threads
	 * executing under the governance, then the calling thread will block/wait until
	 * one has been released.
	 *
	 * @return true if acquired; false if not acquired (interrupted)
	 */
	public boolean acquire() {
		try {
			synchronized (this.lock) {
				while (this.threads >= this.maxThreads)
					this.lock.wait();
				this.threads++;
			}

			return true;
		} catch (InterruptedException ie) {
			return false;
		}
	}

	/**
	 * This method acquires a unit of this semaphore, but will only wait the
	 * specified time for that unit.
	 *
	 * This method counts up the number of threads simultaneously executing under
	 * the governance of this semaphore. If there are already too many threads
	 * executing under the governance, then the calling thread will block/wait until
	 * one has been released or the specified time expires.
	 *
	 * @param millis The maximum number of milliseconds for the calling thread to
	 *               block/wait for a release.
	 * @return true if acquired; false if not acquired (timeout or interrupted)
	 */
	public boolean acquire(long millis) {
		try {
			synchronized (this.lock) {
				if (this.threads >= this.maxThreads) {
					this.lock.wait(millis);
					if (this.threads >= this.maxThreads) return false;
				}
				this.threads++;
			}

			return true;
		} catch (InterruptedException ie) {
			return false;
		}
	}

	/**
	 * This method acquires a unit of this semaphore, but will only wait the
	 * specified time for that unit.
	 *
	 * This method counts up the number of threads simultaneously executing under
	 * the governance of this semaphore. If there are already too many threads
	 * executing under the governance, then the calling thread will block/wait until
	 * one has been released or the specified time expires.
	 *
	 * @param millis The maximum number of milliseconds for the calling thread to
	 *               block/wait for a release.
	 * @param nanos  A number of seconds measured in nanoseconds (1000 nanos per
	 *               millisecond).
	 * @return true if acquired; false if not acquired (timeout or interrupted)
	 */
	public boolean acquire(long millis, int nanos) {
		try {
			synchronized (this.lock) {
				if (this.threads >= this.maxThreads) {
					this.lock.wait(millis, nanos);
					if (this.threads >= this.maxThreads) return false;
				}
				this.threads++;
			}

			return true;
		} catch (InterruptedException ie) {
			return false;
		}
	}

	/**
	 * This method releases a thread allocated to this semaphore.
	 *
	 * This method counts down the number of threads simultaneously executing under
	 * the governance of this semaphore. If there are threads blocking/waiting to
	 * execute, a random one is allowed to return from its call to "acquire". Some
	 * JVMs implement "wait" with a FIFO algorithm, but it isn't guaranteed.
	 */
	public void release() {
		synchronized (this.lock) {
			this.threads = Math.max(0, this.threads - 1);
			this.lock.notify();
		}
	}

	public int getAcquiredThreads() {
		return this.threads;
	}
}
