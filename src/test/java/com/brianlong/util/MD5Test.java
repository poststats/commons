package com.brianlong.util;

import org.junit.Assert;
import org.junit.Test;

public class MD5Test {

	@Test
	public void testRandom() {
		Assert.assertNotNull(MD5.compute());
		Assert.assertNotEquals(MD5.compute(), MD5.compute());
	}

	@Test
	public void testEmptyString() {
		Assert.assertNotNull(MD5.compute(""));
		Assert.assertEquals(MD5.compute(""), MD5.compute(""));
		Assert.assertNotEquals(MD5.compute(""), MD5.compute("."));
	}

	@Test
	public void testShortString() {
		Assert.assertNotNull(MD5.compute("Here is a test"));
		Assert.assertEquals(MD5.compute("Here is a test"), MD5.compute("Here is a test"));
		Assert.assertNotEquals(MD5.compute("Here is a test"), MD5.compute("Here is a test."));
	}

	@Test
	public void testLongString() {
		Assert.assertNotNull(MD5.compute("Here is a test that is a bit longer"));
		Assert.assertEquals(MD5.compute("Here is a test that is a bit longer"), MD5.compute("Here is a test that is a bit longer"));
		Assert.assertNotEquals(MD5.compute("Here is a test that is a bit longer"), MD5.compute("Here is a test that is a bit longer."));
	}

	@Test
	public void testEmptyBytes() {
		Assert.assertNotNull(MD5.compute(new byte[0]));
		Assert.assertEquals(MD5.compute(new byte[0]), MD5.compute(new byte[0]));
		Assert.assertNotEquals(MD5.compute(new byte[0]), MD5.compute(new byte[1]));
	}

	@Test
	public void testBytes() {
		Assert.assertNotNull(MD5.compute(new byte[10]));
		Assert.assertEquals(MD5.compute(new byte[10]), MD5.compute(new byte[10]));
		Assert.assertNotEquals(MD5.compute(new byte[10]), MD5.compute(new byte[11]));
	}

}
