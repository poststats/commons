package com.brianlong.gis;

import com.brianlong.validation.GoogleMapsLocationConstraint;
import com.brianlong.validation.GoogleMapsLocationConstraint.Quality;
import com.brianlong.validation.LocationConstraint;

public class GoogleMapsGeocoderTest extends AbstractGeocoderTest {

	private static final String KEY = "AIzaSyAZecTw3LIliPnQUerep5o3rx42mzXzvw4";

	@SuppressWarnings("unchecked")
	@Override
	public Geocoder<GoogleMapsLocation> getGeocoder() {
		return new GoogleMapsGeocoder(KEY);
	}

	@SuppressWarnings("unchecked")
	@Override
	public LocationConstraint<GoogleMapsLocation> getLocationConstraint(Object quality) {
		return new GoogleMapsLocationConstraint(KEY, (Quality) quality);
	}

}
