package com.brianlong.gis;

import com.brianlong.cache.CacheException;
import com.brianlong.validation.GoogleMapsLocationConstraint.Quality;
import com.brianlong.validation.LocationConstraint;
import java.io.IOException;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public abstract class AbstractGeocoderTest {

	private Geocoder<? extends Location> geocoder = null;

	public abstract <T extends Location> Geocoder<T> getGeocoder();

	public abstract <T extends Location> LocationConstraint<T> getLocationConstraint(Object quality);

	@Before
	public void initGeocoder() {
		this.geocoder = this.getGeocoder();
	}

	@Test
	public void testHomeByPhysicalAddress() throws GeocodingException, CacheException, IOException {
		List<? extends Location> locations = this.geocoder.search("445 Legacy Ct, Westerville, OH");
		Assert.assertNotNull(locations);
		Assert.assertEquals(1, locations.size());

		Location location = locations.iterator()
				.next();
		Assert.assertNotNull(location);
		Assert.assertNotNull(location.getLatitudeInDegrees());
		Assert.assertEquals(40.138, location.getLatitudeInDegrees()
				.doubleValue(), 0.0005);
		Assert.assertNotNull(location.getLongitudeInDegrees());
		Assert.assertEquals(-82.915, location.getLongitudeInDegrees()
				.doubleValue(), 0.0005);
	}

	@Test
	public void testHomeByCity() throws GeocodingException, CacheException, IOException {
		List<? extends Location> locations = this.geocoder.search("Jersey City, NJ");
		Assert.assertNotNull(locations);
		Assert.assertEquals(1, locations.size());

		Location location = locations.iterator()
				.next();
		Assert.assertNotNull(location);
		Assert.assertNotNull(location.getLatitudeInDegrees());
		Assert.assertEquals(40.728, location.getLatitudeInDegrees()
				.doubleValue(), 0.05);
		Assert.assertNotNull(location.getLongitudeInDegrees());
		Assert.assertEquals(-74.078, location.getLongitudeInDegrees()
				.doubleValue(), 0.05);
	}

	@Test
	public void testGolfapaloozaByCity() throws GeocodingException, CacheException, IOException {
		List<? extends Location> locations = this.geocoder.search("700 West Alpine Dr, Terra Alta, WV");
		Assert.assertNotNull(locations);
		Assert.assertFalse(locations.isEmpty());

		Location location = locations.iterator()
				.next();
		Assert.assertNotNull(location);
		Assert.assertNotNull(location.getLatitudeInDegrees());
		Assert.assertEquals(39.46, location.getLatitudeInDegrees()
				.doubleValue(), 0.01);
		Assert.assertNotNull(location.getLongitudeInDegrees());
		Assert.assertEquals(-79.50, location.getLongitudeInDegrees()
				.doubleValue(), 0.01);
	}

	@Test
	public void testHomeByZip() throws GeocodingException, CacheException, IOException {
		List<? extends Location> locations = this.geocoder.search("43081");
		Assert.assertNotNull(locations);
		// Assert.assertEquals(1, locations.size());

		Location location = locations.iterator()
				.next();
		Assert.assertNotNull(location);
		Assert.assertEquals("Westerville", location.getLocality());
		Assert.assertTrue(location instanceof USALocation);
		Assert.assertEquals("OH", ((USALocation) location).getStateCode());
		Assert.assertEquals("US", location.getCountryCode());
	}

	@Test
	public void testJamaica() throws GeocodingException, CacheException, IOException {
		List<? extends Location> locations = this.geocoder.search("81 Knutsford Blvd, Kingston 5, Jamaica");
		Assert.assertNotNull(locations);
		Assert.assertFalse(locations.isEmpty());

		Location location = locations.iterator()
				.next();
		Assert.assertNotNull(location);
		Assert.assertNotNull(location.getThoroughfare());
		Assert.assertTrue(location.getThoroughfare()
				.matches("Knutsford B.+d.*"));
		Assert.assertEquals("Kingston", location.getLocality());
		Assert.assertTrue("JM".equalsIgnoreCase(location.getCountryCode()));
	}

	@Test
	public void testCounty() throws GeocodingException, CacheException, IOException {
		List<? extends Location> locations = this.geocoder.search("Annandale, NJ");
		Assert.assertNotNull(locations);

		for (Location location : locations) {
			if (location instanceof GoogleMapsLocation) {
				GoogleMapsLocation glocation = (GoogleMapsLocation) location;
				Assert.assertTrue(Quality.getQuality(glocation.getType())
						.compareTo(Quality.Locality) <= 0);
			}
		}
	}

}
