package com.brianlong.gis;

import com.brianlong.validation.LocationConstraint;
import com.brianlong.validation.MapQuestLocationConstraint;
import com.brianlong.validation.MapQuestLocationConstraint.Quality;

public class MapQuestGeocoderTest extends AbstractGeocoderTest {

	private static final String KEY = "Fmjtd|luua20u7nh,7x=o5-967554";

	@SuppressWarnings("unchecked")
	@Override
	public Geocoder<MapQuestLocation> getGeocoder() {
		return new MapQuestGeocoder(KEY);
	}

	@SuppressWarnings("unchecked")
	@Override
	public LocationConstraint<MapQuestLocation> getLocationConstraint(Object quality) {
		return new MapQuestLocationConstraint(KEY, (Quality) quality);
	}

}
