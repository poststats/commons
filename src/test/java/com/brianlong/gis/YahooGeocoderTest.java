package com.brianlong.gis;

import com.brianlong.validation.LocationConstraint;
import com.brianlong.validation.YahooLocationConstraint;
import com.brianlong.validation.YahooLocationConstraint.Quality;

public class YahooGeocoderTest extends AbstractGeocoderTest {

	@SuppressWarnings("unchecked")
	@Override
	public Geocoder<YahooLocation> getGeocoder() {
		return new YahooGeocoder();
	}

	@SuppressWarnings("unchecked")
	@Override
	public LocationConstraint<YahooLocation> getLocationConstraint(Object quality) {
		return new YahooLocationConstraint((Quality) quality);
	}

}
