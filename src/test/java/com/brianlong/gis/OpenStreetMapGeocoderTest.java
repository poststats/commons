package com.brianlong.gis;

import com.brianlong.validation.LocationConstraint;
import com.brianlong.validation.OpenStreetMapLocationConstraint;
import com.brianlong.validation.OpenStreetMapLocationConstraint.Quality;

public class OpenStreetMapGeocoderTest extends AbstractGeocoderTest {

	private static final String KEY = "qTdaGqAQKCEGbKRUxRTXLAJQi8uc2AL7";

	@SuppressWarnings("unchecked")
	@Override
	public Geocoder<OpenStreetMapLocation> getGeocoder() {
		return new OpenStreetMapGeocoder(KEY);
	}

	@SuppressWarnings("unchecked")
	@Override
	public LocationConstraint<OpenStreetMapLocation> getLocationConstraint(Object quality) {
		return new OpenStreetMapLocationConstraint(KEY, (Quality) quality);
	}

}
