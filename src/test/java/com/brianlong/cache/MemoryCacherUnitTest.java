package com.brianlong.cache;

import org.junit.Assert;
import org.junit.Test;

public class MemoryCacherUnitTest {

	@Test
	public void basicAddRetrieve() throws CacheException {
		MemoryCacher<String, String> cacher = new MemoryCacher<String, String>(10000L, ReaperFactory.getDefaultInstance());
		for (int i = 0; i < 100; i++)
			cacher.add(String.valueOf(i), "#" + i, true);
		Assert.assertEquals(100, cacher.size());
		for (int i = 0; i < 100; i++) {
			Assert.assertTrue(cacher.isCached(String.valueOf(i)));
			Assert.assertEquals("#" + i, cacher.get(String.valueOf(i)));
		}

		Assert.assertFalse(cacher.isCached("-1"));
		Assert.assertFalse(cacher.isCached("100"));
	}

	@Test
	public void basicExpiration() throws CacheException, InterruptedException {
		MemoryCacher<String, String> cacher = new MemoryCacher<String, String>(1000L, ReaperFactory.getInstance(400L));
		for (int i = 0; i < 100; i++)
			cacher.add(String.valueOf(i), "#" + i, true);
		Assert.assertEquals(100, cacher.size());
		Thread.sleep(2000L);
		Assert.assertEquals(0, cacher.size());
	}

	@Test(expected = java.lang.NullPointerException.class)
	public void isCachedNull() throws CacheException {
		MemoryCacher<String, String> cacher = new MemoryCacher<String, String>(10000L, ReaperFactory.getDefaultInstance());
		cacher.isCached(null);
	}

	@Test(expected = java.lang.NullPointerException.class)
	public void getNull() throws CacheException {
		MemoryCacher<String, String> cacher = new MemoryCacher<String, String>(10000L, ReaperFactory.getDefaultInstance());
		cacher.get((String) null);
	}

	@Test(expected = java.lang.NullPointerException.class)
	public void addNullId() throws CacheException {
		MemoryCacher<String, String> cacher = new MemoryCacher<String, String>(10000L, ReaperFactory.getDefaultInstance());
		cacher.add(null, "value", true);
	}

	@Test
	public void addNullValue() throws CacheException {
		MemoryCacher<String, String> cacher = new MemoryCacher<String, String>(10000L, ReaperFactory.getDefaultInstance());
		cacher.add("id", null, true);
		Assert.assertEquals(1, cacher.size());
		Assert.assertTrue(cacher.isCached("id"));
		Assert.assertNull(cacher.get("id"));
	}

	@Test
	public void empty() throws CacheException {
		MemoryCacher<String, String> cacher = new MemoryCacher<String, String>(10000L, ReaperFactory.getDefaultInstance());
		Assert.assertEquals(0, cacher.size());
		Assert.assertFalse(cacher.isCached(""));
		Assert.assertNull(cacher.get(""));
	}

}
