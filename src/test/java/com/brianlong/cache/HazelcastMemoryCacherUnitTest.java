package com.brianlong.cache;

import com.hazelcast.config.Config;
import com.hazelcast.config.NetworkConfig;
import com.hazelcast.config.TcpIpConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class HazelcastMemoryCacherUnitTest {

	private MemoryCacher<String, String>[] cachers;

	@SuppressWarnings("unchecked")
	@Before
	public void init2() throws CacheException {
		MemoryCacher<String, String> cacher1 = new MemoryCacher<String, String>(10000L, ReaperFactory.getDefaultInstance());
		HazelcastInstance hzInst1 = this.createHazelcastInstance(5701, "localhost:5801");
		HazelcastCacheAdapter<String> hzAdapter1 = new HazelcastCacheAdapter<String>(hzInst1, "cache");

		MemoryCacher<String, String> cacher2 = new MemoryCacher<String, String>(10000L, ReaperFactory.getDefaultInstance());
		HazelcastInstance hzInst2 = this.createHazelcastInstance(5801, "localhost:5701");
		HazelcastCacheAdapter<String> hzAdapter2 = new HazelcastCacheAdapter<String>(hzInst2, "cache");

		cacher1.addListener(hzAdapter1);
		cacher2.addListener(hzAdapter2);
		hzAdapter1.addListener(cacher1);
		hzAdapter2.addListener(cacher2);

		this.cachers = new MemoryCacher[] { cacher1, cacher2 };
	}

	@After
	public void uninit() {
		for (HazelcastInstance hzInst : Hazelcast.getAllHazelcastInstances())
			hzInst.shutdown();
	}

	private HazelcastInstance createHazelcastInstance(int port, String... members) {
		Config config = new Config();
		NetworkConfig networkConfig = config.getNetworkConfig();
		networkConfig.setPort(port);
		TcpIpConfig tcpipConfig = networkConfig.getJoin()
				.getTcpIpConfig();
		for (String member : members)
			tcpipConfig.addMember(member);

		return Hazelcast.newHazelcastInstance(config);
	}

	@Test
	public void empty() {
		Assert.assertEquals(0, this.cachers[0].size());
		Assert.assertEquals(0, this.cachers[1].size());
	}

	@Test
	public void disparate() throws CacheException, InterruptedException {
		this.cachers[0].add("1", "#1", true);
		this.cachers[1].add("2", "#2", true);
		Thread.sleep(500L);

		Assert.assertEquals(1, this.cachers[0].size());
		Assert.assertTrue(this.cachers[0].isCached("1"));
		Assert.assertFalse(this.cachers[0].isCached("2"));
		Assert.assertEquals(1, this.cachers[1].size());
		Assert.assertFalse(this.cachers[1].isCached("1"));
		Assert.assertTrue(this.cachers[1].isCached("2"));
	}

	@Test
	public void duplicate() throws CacheException, InterruptedException {
		this.cachers[0].add("1", "#1", true);
		Thread.sleep(500L);
		this.cachers[1].add("1", "#2", true);
		Thread.sleep(500L);

		Assert.assertEquals(0, this.cachers[0].size());
		Assert.assertFalse(this.cachers[0].isCached("1"));
		Assert.assertEquals(1, this.cachers[1].size());
		Assert.assertTrue(this.cachers[1].isCached("1"));

		this.cachers[0].add("1", "#1", true);
		Thread.sleep(500L);

		Assert.assertEquals(1, this.cachers[0].size());
		Assert.assertTrue(this.cachers[0].isCached("1"));
		Assert.assertEquals(0, this.cachers[1].size());
		Assert.assertFalse(this.cachers[1].isCached("1"));
	}

}
